'use strict';

import { graphiqlExpress, graphqlExpress } from 'apollo-server-express';
import * as AWS from 'aws-sdk';
import { json, urlencoded } from 'body-parser';
import * as compression from 'compression';
import * as config from 'config';
import * as cors from 'cors';
import * as express from 'express';
import * as files from 'express-fileupload';
import { execute, GraphQLError } from 'graphql';
import { subscribe } from 'graphql/subscription';
import * as helmet from 'helmet';
import * as http from 'http';
import * as _ from 'lodash';
import * as mongoose from 'mongoose';
import * as process from 'process';
import { mw as requestIpMw } from 'request-ip';
import { SubscriptionServer } from 'subscriptions-transport-ws';
import * as uuid from 'uuid';

import { documents } from './_shared/documents';
import AppGraphQLSchema from './_shared/graphql';
import './mv/express-promise';

import { ApplicationError, HttpError, NotFoundHttpError } from './errors';

import { aclMv, authMv, authMvRequired, userMv } from './mv/auth';

import { router as ItemRouter } from './routes/item';
import { router as TokenRouter } from './routes/token';

import schedulerRunner from './scheduler';

(mongoose as any).Promise = global.Promise;
AWS.config.setPromisesDependency(global.Promise);

AWS.config.update({
    region: config.get('aws.region') as string,
    apiVersions: config.get('aws.apiVersions') as any,
    credentials: {
        accessKeyId: config.get('aws.accessKeyId') as string,
        secretAccessKey: config.get('aws.secretAccessKey') as string,
    },
});

class AppServer {
    /**
     * Entry point (FrontController)
     *
     * @returns {AppServer}
     */
    public static bootstrap(): AppServer {
        return new this();
    }

    public app: express.Application;
    public server: http.Server;
    public subscription;

    private constructor() {
        this.app = express();

        this.config();
        this.routes();

        this.server = http.createServer(this.app);

        this.subscribe();
        this.graphQL();
        this.serverListen();
        this.scheduler();
        this.files();

        this.notFound();
        this.error();
    }

    private config(): void {
        if (config.get('is_prod')) {
            this.app.enable('trust proxy');
        }

        this.app.use(helmet());
        this.app.use(urlencoded({ extended: false }));
        this.app.use(json());
        this.app.use(compression());
    }

    private routes(): void {
        this.app.get('/', cors({ methods: ['GET', 'OPTIONS'] }), (req: express.Request, res: express.Response) =>
            res.json({ version: require('./../package.json').version }),
        );
        this.app.use('/token', cors({ methods: ['DELETE', 'PUT', 'POST', 'OPTIONS'] }), TokenRouter);
    }

    private graphQL(): void {
        this.app.use(
            config.get('graphql_path') as string,
            cors({
                methods: ['GET', 'POST', 'OPTIONS'],
            }),
            requestIpMw(),
            authMv,
            userMv,
            aclMv,
            graphqlExpress(req => ({
                debug: !config.get('is_prod'),
                formatError: err => this.processError(err).errRes,
                rootValue: { acl: req.acl, user: req.user },
                context: { clientIp: req.clientIp, user: req.user },
                schema: AppGraphQLSchema as any,
            })),
        );

        // debug mode and UI for GraphQL
        if (!config.get('is_prod')) {
            this.app.use(
                '/graphiql',
                graphiqlExpress({
                    endpointURL: config.get('graphql_path') as string,
                }),
            );
        }
    }

    private files(): void {
        this.app.use(
            '/items',
            cors({
                methods: ['GET', 'DELETE', 'PUT', 'POST', 'OPTIONS'],
                origin: new RegExp(String(config.get('frontend_host')).replace(/[\-\[\]\/\{\}\(\)\*\+\?\.\\\^\$\|]/g, '\\$&')),
                optionsSuccessStatus: 200,
                credentials: true,
            }),
            files(),
            authMvRequired,
            userMv,
            aclMv,
            ItemRouter,
        );

        this.app.use('/preview', express.static(_.trimEnd(config.get('temp_upload_path') as string, '/')));
    }

    private notFound(): void {
        this.app.use((req: express.Request, res: express.Response, next: express.NextFunction) => {
            throw new NotFoundHttpError('Not found', 'not_found');
        });
    }

    private error(): void {
        this.app.use((err: any | Error, req: express.Request, res: express.Response, next: express.NextFunction) => {
            const { errStatus, errRes } = this.processError(err);
            if (!res.headersSent) {
                res.status(errStatus).json(errRes);
            } else {
                res.end();
            }
        });
    }

    private serverListen(): void {
        this.server.on('error', (error: any) => {
            if (error.syscall !== 'listen') {
                throw error;
            }

            const addr = this.server.address();
            const bind = typeof addr === 'string' ? 'Pipe ' + addr : 'Port ' + addr.port;

            switch (error.code) {
                case 'EACCES':
                    console.error(bind + ' requires elevated privileges');
                    process.exit(1);
                    break;

                case 'EADDRINUSE':
                    console.error(bind + ' is already in use');
                    process.exit(1);
                    break;

                default:
                    throw error;
            }
        });

        this.server.on('listening', () => {
            const addr = this.server.address();
            console.info('Listening on', typeof addr === 'string' ? 'pipe ' + addr : 'port ' + addr.port);
        });
        this.server.listen(config.get('port'));
    }

    private subscribe() {
        this.subscription = SubscriptionServer.create(
            {
                schema: AppGraphQLSchema as any,
                execute,
                subscribe,
                async onConnect({ token }, webSocket) {
                    webSocket.id = uuid.v4();
                    if (token) {
                        await documents.UserRoom.addUserByJwtToken(webSocket.id, token);
                    }
                },
                async onDisconnect(webSocket) {
                    await documents.UserRoom.removeUserByWsId(webSocket.id);
                },
                // async onOperation(message, params, webSocket) {
                //     if (!webSocket.id) {
                //         webSocket.id = uuid.v4();
                //     }
                //     const user = await documents.UserRoom.addUserByJwtToken(webSocket.id, message.payload.token);
                //     return {
                //         ...params,
                //         context: user,
                //     };
                // },
            },
            {
                path: config.get('subscription_path'),
                server: this.server,
            },
        );
    }

    private scheduler() {
        schedulerRunner();
    }

    private processError(err: Error | any): { errStatus: any; errRes: any } {
        const isDebug = !config.get('is_prod');
        if (err instanceof GraphQLError && err.originalError instanceof ApplicationError) {
            err = err.originalError;
        }
        try {
            let errStatus = 500;
            const errRes: any = {
                code: 'undefined_error',
                message: 'Undefined error.',
            };

            if (err instanceof Error) {
                errRes.code = err.name;
                // todo: optimize it
                if (err.name === 'UnauthorizedError') {
                    errStatus = 401;
                }
                if (err instanceof HttpError) {
                    errStatus = err.status;
                    if (typeof err.code !== 'undefined') {
                        errRes.code = err.code;
                    }
                }

                if (isDebug) {
                    errRes.message = err.toString();
                    errRes.stack = err.stack;
                } else {
                    errRes.message = err.message;
                }
            } else {
                if (isDebug) {
                    errRes.message = err;
                }
            }

            return { errStatus, errRes };
        } catch (err2) {
            console.error('Error sending 500!', err2.stack);
        }
    }
}

const appServer = AppServer.bootstrap();

export const server = appServer.server;
export const app = appServer.app;
export const subscription = appServer.subscription;
