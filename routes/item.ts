'use strict';

import { promisify } from 'util';
import * as config from 'config';
import * as express from 'express';
import * as mime from 'mime';
import * as guid from 'randomstring';

export const router = express.Router();

interface IExpressUploadedFile {
    name: string;
    data: Buffer;
    encoding: string;
    mimetype: string;

    mv(path: string): Promise<any>;
}

interface IFilesUploadExpressRequest<fileName extends string> extends express.Request {
    files: {
        [fileName: string]: IExpressUploadedFile;
    };
}

router.post('/upload', async (req: IFilesUploadExpressRequest<'fileName'>, res: express.Response) => {
    if (!req.files) {
        return res.status(400).send('No files were uploaded.');
    }

    const itemImage = req.files.itemImage;
    try {
        const filename = `${guid.generate()}.${mime.getExtension(itemImage.mimetype)}`;
        await promisify(itemImage.mv)(`${config.get('temp_upload_path')}/${filename}`);
        return res.json({
            code: 0,
            message: 'Item image uploaded',
            src: filename,
        });
    } catch (e) {
        return res.status(500).json({
            code: e.code,
            message: `Item image upload error: ${e.message}`,
        });
    }
});
