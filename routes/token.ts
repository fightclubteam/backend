'use strict';

import * as express from 'express';
import { documents } from '../_shared/documents';
import { authMvRequired, userMv } from '../mv/auth';
import { BadRequestHttpError, NotFoundHttpError } from '../errors';

export const router = express.Router();

router.post('/', async (req: express.Request, res: express.Response) => {
    const user = await documents.User.findByUsername(req.body.username);

    if (!user) {
        throw new NotFoundHttpError('User not found.', 'user_not_found');
    }

    if (!user.canLogin()) {
        throw new BadRequestHttpError('The user has already used all of login attempts.', 'no_user_login_attempts');
    }

    const isPasswordVerified = await user.verifyPassword(req.body.password);
    if (!isPasswordVerified) {
        await user.increaseLoginAttempts();
        throw new BadRequestHttpError('Invalid credentials.', 'user_invalid_credentials');
    }

    await user.resetLoginAttempts();

    const token = user.generateAuthToken();
    return res
        .header('Authorization', `Bearer ${token}`)
        .status(201)
        .json({ token });
});

router.put('/', authMvRequired, userMv, async (req: express.Request, res: express.Response) => {
    try {
        await documents.RevokedAuthToken.update(
            {
                user: req.user,
                jti: req.jwt.jti,
                iat: new Date(req.jwt.iat * 1000),
            },
            { upsert: true },
        );
    } catch (e) {
        console.error(e);
    }

    return res.json({ token: req.user.generateAuthToken() });
});

router.delete('/', authMvRequired, userMv, async (req: express.Request, res: express.Response) => {
    try {
        await documents.RevokedAuthToken.create({
            user: req.user,
            jti: req.jwt.jti,
            iat: new Date(req.jwt.iat * 1000),
        });
    } catch (e) {
        console.error(e);
    } finally {
        res.sendStatus(204);
    }
});
