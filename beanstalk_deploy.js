#!/usr/bin/env node
/**
 * @description A Bitbucket Builds template for deploying an application to AWS Elastic Beanstalk via NodeJS and AWS SDK
 * @author Ivan Zubok <chi_no@ukr.net>
 * @version v1.0.0
 * @example node beanstalk_deploy.js /some/path/of/compressed-file.zip
 */
'use strict';

const AWS = require('aws-sdk');
const fs = require('fs');

AWS.config.update({
    accessKeyId: process.env.AWS_ACCESS_KEY_ID,
    secretAccessKey: process.env.AWS_SECRET_ACCESS_KEY,
    region: process.env.AWS_DEFAULT_REGION
});

const VERSION_LABEL = (() => {
    const date = new Date();
    return [date.getFullYear(), fixZero(date.getMonth() + 1), fixZero(date.getDate())].join('-')
    + '_' +
    [fixZero(date.getHours()), fixZero(date.getMinutes()), fixZero(date.getSeconds())].join(':');

    function fixZero(datePart) {
        return datePart < 10 ? '0' + datePart : datePart;
    }
})();

const BUCKET_KEY = process.env.APPLICATION_NAME + '/' + VERSION_LABEL + '-bitbucket_builds.zip';

/**
 * Uploads build to S3
 *
 * @param {string} artifact path of *.zip file
 * @param {function} cb callback
 */
function uploadToS3(artifact, cb) {
    const uploadParams = {Bucket: process.env.S3_BUCKET, Key: BUCKET_KEY, Body: fs.createReadStream(artifact)};
    uploadParams.Body.on('error', err => console.error('Read stream: ', err));

    (new AWS.S3()).upload(uploadParams, cb);
}

/**
 * Creates new ElasticBeanstalk version for application
 *
 * @param {function} cb callback
 */
function createNewVersion(cb) {
    (new AWS.ElasticBeanstalk()).createApplicationVersion({
        ApplicationName: process.env.APPLICATION_NAME,
        VersionLabel: VERSION_LABEL,
        Description: 'New build from Bitbucket Pipeline',
        SourceBundle: {
            S3Bucket: process.env.S3_BUCKET,
            S3Key: BUCKET_KEY,
        },
        Process: true
    }, cb);
}

/**
 * Deploys ElasticBeanstalk application
 *
 * @param {function} cb callback
 */
function deployNewVersion(cb) {
    (new AWS.ElasticBeanstalk()).updateEnvironment({
        ApplicationName: process.env.APPLICATION_NAME,
        EnvironmentName: process.env.APPLICATION_ENVIRONMENT,
        VersionLabel: VERSION_LABEL
    }, cb);
}

uploadToS3(process.argv[2] || '/tmp/artifact.zip', err => {
    if (err) {
        console.error('\x1b[31mAWS S3\x1b[0m: failed to deploy new build...', err);
        process.exit(1);
    }

    console.info('\x1b[36mAWS S3\x1b[0m: build successfully uploaded to S3...');

    createNewVersion((err, data) => {
        if (err) {
            console.error('\x1b[31mAWS ElasticBeanstalk\x1b[0m: failed to create new version...', err);
            process.exit(1);
        }

        if (!data || !data.ResponseMetadata || !data.ResponseMetadata.RequestId) {
            console.error('\x1b[31mAWS ElasticBeanstalk\x1b[0m: failed to parse response...', data);
            process.exit(1);
        }

        console.info('\x1b[36mAWS ElasticBeanstalk\x1b[0m: new version successfully created...');

        // Wait(4s) for the new version to be consistent before deploying
        setTimeout(() => deployNewVersion(err => {
            if (err) {
                console.error('\x1b[31mAWS ElasticBeanstalk\x1b[0m: failed to update environment...', err);
                process.exit(1);
            }
            console.info('\x1b[36mAWS ElasticBeanstalk\x1b[0m: new version successfully deployed.');
            process.exit(0); // just to be make sure that everything is fine and kill current process immediately
        }), 4000);
    });
});
