## Branch naming convention
- If you want to create a feature branch please create new branch from
`master` with `feature/branch_name` name.

## How to install project
    npm i
    npm run seed-mongo
    npm run server

## Installation
    git clone git@bitbucket.org:fightclubteam/backend.git 
    cp config/local.ts.dist config/local.ts # and replace/change to local variables
    docker-compose up

browse to playing with GraphQL
    
    http://localhost:8081/graphiql
    
authorize GraphiQL

    https://chrome.google.com/webstore/detail/modheader/idgpnmonknjnojddfkpgkljpfnnfcklj?utm_source=chrome-app-launcher-info-dialog
    
extended GraphiQL

    https://chrome.google.com/webstore/detail/graphiql-feen/mcbfdonlkfpbfdpimkjilhdneikhfklp/related
    
## Docker and Docker compose

Docker images are located at:

* `docker/node` - main backend image
* `docker/node-dev` - temporary image to build bcrypt dependency

Build project for unix-systems:

    docker build -t fightclub/node-dev:latest docker/node-dev
    docker build -t fightclub/node:latest docker/node
    docker run -it --rm -v $(pwd):/var/www/backend -w /var/www/backend fightclub/node-dev:latest npm i bcrypt
    docker run -it --rm -v $(pwd):/var/www/backend -w /var/www/backend fightclub/node:latest npm i
    docker-compose -f docker-compose-dev.yml run backend npm run seed-mongo
    docker-compose -f docker-compose-dev.yml up -d
    
Build project for Windows/VirtualBox systems:

    docker build -t fightclub/node-dev:latest docker/node-dev
    docker build -t fightclub/node:latest docker/node
    docker run -it --rm -v $(pwd):/var/www/backend -w /var/www/backend fightclub/node-dev:latest npm i bcrypt --no-bin-links --unsafe-perm
    docker run -it --rm -v $(pwd):/var/www/backend -w /var/www/backend fightclub/node:latest npm i --no-bin-links
    docker-compose -f docker-compose-dev.yml run backend npm run seed-mongo
    docker-compose -f docker-compose-dev.yml up -d
    
## Useful knowledge
   
Rebuilding the images
   
After adding new npm or typings package, you will have to rebuild your images as follows:
   
    docker-compose stop
    docker-compose rm
    docker-compose build
    docker-compose up
    
Add new npm package:

    docker-compose run app npm i -S <wathever>
    
Add new typings:
   
    docker-compose run app npm i -D @types/<wathever>

## Can I deploy ?

I'v not yet automated the deployment. For now you can do whatever you want with the /dist folder, that contains the transpiled js code.

## Run in dev env

    npm run server:run
    
## GraphQL sandbox
    
    http://localhost:8081/graphiql

##Windows users only 
How to get rid of annoying popups in Windows 10?
[https://github.com/Unitech/pm2/issues/2182](https://github.com/Unitech/pm2/issues/2182)
