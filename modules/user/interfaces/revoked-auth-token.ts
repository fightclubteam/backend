'use strict';

import * as mongoose from 'mongoose';

export interface IRevokedAuthToken extends mongoose.Document {
    user: string;
    jti: string;
    iat: Date;
}

export interface IRevokedAuthTokenDocument extends mongoose.Model<IRevokedAuthToken> {}
