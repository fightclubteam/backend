'use strict';

import * as mongoose from 'mongoose';

import { IFindableByIdOrFail } from '../../../_shared/common.interfaces';

import { ItemInterface } from '../../item/interfaces/item';
import { IUser } from './user';

export interface IUserItem extends mongoose.Document {
    item: ItemInterface;
    user: IUser;
    is_dressed_on: boolean;
}

export interface IUserItemDocument extends mongoose.Model<IUserItem>, IFindableByIdOrFail<IUserItem> {}
