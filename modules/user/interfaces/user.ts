'use strict';

import * as mongoose from 'mongoose';
import { IAclResource, IAclRole } from '../../../_shared/common.interfaces';
import { IFight } from '../../fight/interfaces/fight';
import { IItemsCollectionAware, ItemInterface } from '../../item/interfaces/item';
import { ILocation } from '../../location/interfaces/location';
import { IStore } from '../../store/interfaces/store';
import { IUserItem } from './user_item';

export type UserRoleType = 'ADMIN' | 'DEALER' | 'CLAN_HEAD' | 'MODERATOR' | 'USER';
export type UserGenderType = 'MALE' | 'FEMALE';

export interface IEmailSearch<TInstance> {
    /**
     * Search for a single instance by its email. This applies LIMIT 1, so the listener will
     * always be called with a single instance.
     */
    findByEmail(email: string): Promise<TInstance | null>;
}

export interface IUsernameSearch<TInstance> {
    /**
     * Search for a single instance by its email. This applies LIMIT 1, so the listener will
     * always be called with a single instance.
     */
    findByUsername(username: string): Promise<TInstance | null>;
}

export interface IUserRank extends mongoose.Document {
    slug: string;
    position: number;
    title: string;
}

export interface IUserAbout {
    slogan: string;
    hobby: string;
    city: string;
}

export interface IUserRoom extends mongoose.Document {
    ws_id: string;
    login: string;
    level: number;
    location: string;
}

export interface IUserStatus {
    silent_till?: Date;
    ban_till?: Date;
}

export interface IUserAbility {
    strength?: number;
    agility?: number;
    intuition?: number;
    endurance?: number;
    intelligence?: number;
    spirituality?: number;
    sincerity?: number;
    hp?: number;
    hp_max?: number;
    hp_current?: number;
    energy?: number;
    mall?: number;
    mall_type?: number;
    backpack_weight_max?: number;
    backpack_weight_current?: number;
    damage_min?: number;
    damage_max?: number;
}

export interface IUserModifier {
    dodge: number;
    anti_dodge: number;
    critical: number;
    anti_critical: number;
}

export interface IUserMastery {
    club: number;
    axe: number;
    knife: number;
    sword: number;
}

export interface IUserCurrentFightInfo {
    hp?: number;
    updated_at?: Date;
}

export interface IUserLastLocation {
    fight?: IFight;
    finished_at?: Date;
}

export interface IUserArmor {
    head: number;
    chest: number;
    abdomen: number;
    girdle: number;
    legs: number;
}

export interface IUserSets {
    name: string;
    items: string[];
    is_dressed_on: boolean;
}

export interface IUser extends mongoose.Document, IAclRole, IItemsCollectionAware<IUser, IUserItem> {
    email?: string;
    login?: string;
    password?: string;
    register_ip?: string;
    name?: string;
    birthday?: Date;
    location?: string | ILocation;
    sex?: UserGenderType;
    role?: UserRoleType;
    free_ability?: number;
    free_mastery?: number;
    exp?: number;
    level?: number;
    rank?: string;
    valor?: number;
    wins?: number;
    defeats?: number;
    draws?: number;
    dinars?: number;
    timezone?: string;
    login_attempts?: number;
    reset_password_hash?: string;
    sets?: IUserSets[];
    sent_reset_password_at?: Date;
    created_at?: Date;
    updated_at?: Date;
    about?: IUserAbout;
    abilities?: IUserAbility;
    armor?: IUserArmor;
    modifiers?: IUserModifier;
    mastery?: IUserMastery;
    statuses?: IUserStatus;
    current_fight_info?: IUserCurrentFightInfo;
    last_fight?: ILocation | string;
    last_location_info?: IUserLastLocation;

    getAttackCount(): Promise<1 | 2>;
    getDefendCount(): Promise<2 | 3>;

    canMove(location: ILocation): boolean;
    canLogin(): boolean;
    verifyPassword(password: string): Promise<boolean>;
    getAuthToken(): string;
    generateAuthToken(): string;
    forgotPassword(): Promise<IUser>;
    increaseLoginAttempts(): Promise<IUser>;
    updateStats(params: { abilities: IUserAbility; mastery: IUserMastery }): Promise<IUser>;
    resetLoginAttempts(): Promise<IUser>;
    recalculateStatsUserForItem(itemInfo: IUserItem, multiplier: number): IUser;

    isOnline(): Promise<boolean>;
    getDamageBetweenMinMax(): number;
    isDead(): boolean;
    hasLowHP(): boolean;
    getItem(itemId: string): any; // todo: ItemInterface

    takeClothesOff(): Promise<IUser>;
    takeClothOn(item: IUserItem): Promise<IUser>;
    takeClothOff(item: IUserItem): Promise<IUser>;
    takeSetOn(setId: string): Promise<IUser>;
    createSet(name: string, items: string[], setId?: string): Promise<IUser>;
    deleteSet(setId: string): Promise<IUser>;
    buyItemFromStore(store: IStore, item: ItemInterface): Promise<IUserItem>;
    sellItemToStore(store: IStore, item: ItemInterface): Promise<IUserItem>;
}

export interface IUserDocument
    extends mongoose.Model<IUser>,
        IEmailSearch<IUser>,
        IUsernameSearch<IUser>,
        IAclResource,
        IItemsCollectionAware<IUser, IUserItem> {
    findByLogin(login: string): mongoose.DocumentQuery<IUser | null, IUser>;
    findUserForFight(fight: IFight, login: string): mongoose.DocumentQuery<IUser | null, IUser>;
    updateLastFight(fight): mongoose.Query<any>;
    setUsersCurrentFight(fight: IFight): Promise<any>;
    getCommonMallType(mallType1: number, mallType2: number): number;
    takeClothesOff(user: IUser): Promise<null>;
}

export interface IUserRankDocument extends mongoose.Model<IUserRank> {}

export interface IUserRoomDocument extends mongoose.Model<IUserRoom> {
    addUserByJwtToken(wsId: string, token: string): Promise<IUser>;
    removeUserByWsId(wsId: string): Promise<any>;
}
