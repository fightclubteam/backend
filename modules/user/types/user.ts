'use strict';

export default `
    enum UserGender {
        MALE
        FEMALE
    }
    
    enum UserRole {
        ADMIN
        DEALER
        CLAN_HEAD
        MODERATOR
        USER
    }
    
    type UserRankType {
        slug: String!
        position: Int!
        title: String!
    }

    type UserMastery {
        club: Int
        axe: Int
        knife: Int
        sword: Int
    }
    
    type UserStatus {
        silent_till: String
        ban_till: String
    }
    
    type UserAbility {
        strength: Int
        agility: Int
        intuition: Int
        endurance: Int
        armor: Int
        hp_max: Int
        hp_current: Int
        damage_min: Int
        damage_max: Int
        power: Int
        intelligence: Int
        spirituality: Int
        sincerity: Int
        mall: Int
        mall_type: Float
        basic_hp: Int
        backpack_weight_current: Int
        backpack_weight_max: Int
        basic_damage_min: Int
        basic_damage_max: Int
        energy: Int
    }
    
    type UserArmor {
        head: Int
        chest: Int
        abdomen: Int
        girdle: Int
        legs: Int
    }
    
    type UserModifiers {
        dodge: Int
        anti_dodge: Int
        critical: Int
        anti_critical: Int
        basic_critical: Int
        basic_anti_critical: Int
        basic_dodge: Int
        basic_anti_dodge: Int
    }
    
    type User {
        id: String!
        email: String!
        login: String!
        name: String!
        birthday: String!
        location: LocationType
        sex: UserGender!
        role: UserRole!
        free_ability: Int!
        exp: Int!
        valor: Int!
        level: Int!
        wins: Int!
        defeats: Int!
        draws: Int!
        dinars: Int!
        login_attempts: Int
        free_mastery: Int
        abilities: UserAbility
        mastery: UserMastery
        armor: UserArmor
        modifiers: UserModifiers
        statuses: UserStatus
        items(limit: Int = 10, page: Int = 1): PaginatedUserItemType!
        rank: String
        sets: [ItemsSetType]
        hp_max: Int
        hp_current: Int
    }
    
    type UserPublicAbility {
        strength: Int
        agility: Int
        intuition: Int
        endurance: Int
        intelligence: Int
        hp_max: Int
        hp_current: Int
        spirituality: Int
        sincerity: Int
    }
    
    type UserItemType implements Node {
        _id: ID!
        item: ItemType!
        user: User!
        is_dressed_on: Boolean!
    }
    
    type UserPublic {
        login: String!
        name: String!
        birthday: String!
        location: String!
        sex: UserGender!
        level: Int!
        wins: Int!
        defeats: Int!
        draws: Int!
        rank: String!
        hp_max: Int
        hp_current: Int
        abilities: UserPublicAbility!
        is_online: Boolean!
        items: [UserItemType]!
    }
    
    input UserInput {
        email: String!
        login: String!
        name: String!
        password: String!
        birthday: String!
        sex: UserGender!
    }
    
    input UserAbilityInput {
        strength: Int!
        agility: Int!
        intuition: Int!
        endurance: Int!
        intelligence: Int!
        spirituality: Int!
        sincerity: Int!
    }
    
    input UserModifierInput {
        dodge: Int!
        anti_dodge: Int!
        critical: Int!
        anti_critical: Int!
    }
    
    input UserMasteryInput {
        club: Int!
        axe: Int!
        knife: Int!
        sword: Int!
    }
    
    type ForgotUserPasswordType {
        sent_at: String!
        expires_at: String!
    }
    
    type Query {
        user(login: String!): User!
        currentUser: User!
        userPublic(login: String!): UserPublic!
        userRanks: [UserRankType]!
    }
   
    type Mutation {
        registerUser(signUpUser: UserInput!): User!
        forgotUserPassword(email: String!): ForgotUserPasswordType!
        takeClothesOff: User!
        takeClothOff(item_id: String!): User!
        takeClothOn(item_id: String!): User!
        takeSetOn(set_id: String!): User!
        createSet(name: String!, items: [String!]!, set_id: String): User!
        deleteSet(set_id: String!): User!
        updateUserStats(abilities: UserAbilityInput!, mastery: UserMasteryInput!): Int!
        buyItemFromStore(store_id: String!, store_item_id: String!): UserItemType!
        sellItemToStore(store_id: String!, user_item_id: String!): UserItemType!
    }
`;
