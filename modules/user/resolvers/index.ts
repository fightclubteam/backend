'use strict';

import { mergeResolvers } from 'merge-graphql-schemas';

import userMutationsResolvers from './user-mutations';
import userQueriesResolvers from './user-queries';
import userSubscriptionsResolvers from './user-subscriptions';

export default mergeResolvers([userQueriesResolvers, userMutationsResolvers, userSubscriptionsResolvers]);
