'use strict';

import { documents } from '../../../_shared/documents';
import { sendEmailTemplateToUser } from '../../../_shared/helpers/email/email';

import { GUEST_ROLE } from '../enums/user';

import UserExistsError from '../errors/UserExistsError';
import UserNotAllowedError from '../errors/UserNotAllowedError';
import UserNotFoundError from '../errors/UserNotFoundError';
import StoreItemNotFoundError from '../../store/errors/StoreItemNotFoundError';
import UserItemNotFoundError from '../errors/UserItemNotFoundError';

export default {
    Mutation: {
        async registerUser(root, { signUpUser }, { clientIp }) {
            let user = await documents.User.findByEmail(signUpUser.email);
            if (user) {
                throw new UserExistsError(`User with email ${signUpUser.email} already exists`);
            }
            user = await documents.User.findByUsername(signUpUser.login);
            if (user) {
                throw new UserExistsError(`User with login ${signUpUser.login} already exists`);
            }

            const [location, rank] = await Promise.all([
                documents.Location.findDefaultLocation(),
                documents.UserRank.findOne(),
            ]);

            user = new documents.User({
                ...signUpUser,
                location,
                rank: rank.slug,
                register_ip: clientIp,
            });

            await user.save();

            // todo use CQRS pattern
            await sendEmailTemplateToUser(user, 'register-user');

            return user;
        },
        async forgotUserPassword(root, { email }) {
            // todo send password hash
            let user = await documents.User.findByEmail(email);

            if (!user) {
                throw new UserNotFoundError(`User with email ${email} does not exist`);
            }

            user = await user.forgotPassword();

            return {
                sent_at: user.sent_reset_password_at,
                expires_at: user.sent_reset_password_at,
            };
        },
        takeClothesOff({ user }) {
            return user.takeClothesOff();
        },
        async takeClothOn({ user }, { item_id }) {
            const item = await documents.UserItem.findById(item_id);
            return user.takeClothOn(item);
        },
        async takeClothOff({ user }, { item_id }) {
            const item = await documents.UserItem.findById(item_id);
            return user.takeClothOff(item);
        },
        takeSetOn({ user }, { set_id }) {
            return user.takeSetOn(set_id);
        },
        createSet({ user }, { name, items, set_id }) {
            return user.createSet(name, items, set_id);
        },
        deleteSet({ user }, { set_id }) {
            return user.deleteSet(set_id);
        },
        async updateUserStats({ user }, stats) {
            await user.updateStats(stats);
            return user.free_ability;
        },
        async buyItemFromStore({ user }, { store_id, store_item_id }) {
            if (!user) {
                throw new UserNotFoundError('Only real user can buy item from store');
            }
            if (user.role === GUEST_ROLE) {
                throw new UserNotAllowedError('Guest users cannot buy items');
            }
            const storeItem = await documents.StoreItem.findByIdOrFail(store_item_id);
            if (!storeItem || storeItem.store._id.toString() !== store_id) {
                throw new StoreItemNotFoundError('Item not found inside this store');
            }
            return user.buyItemFromStore(storeItem.store, storeItem.item);
        },
        async sellItemToStore({ user }, { store_id, user_item_id }) {
            if (!user) {
                throw new UserNotFoundError('Only real user can sell item back to store');
            }
            if (user.role === GUEST_ROLE) {
                throw new UserNotAllowedError('Guest users cannot sell items');
            }
            const [userItem, store] = await Promise.all([
                documents.UserItem.findByIdOrFail(user_item_id),
                documents.Store.findByIdOrFail(store_id),
            ]);
            if (!userItem || userItem.user._id.toString() !== user._id.toString()) {
                throw new UserItemNotFoundError('This item does not belong to this user');
            }
            return await user.sellItemToStore(store, userItem);
        },
    },
};
