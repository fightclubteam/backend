'use strict';

import { documents } from '../../../_shared/documents';
import { paginateModel } from '../../../_shared/helpers/pagination/pagination';

import UserNotFoundError from '../errors/UserNotFoundError';

const defaultItemsFilter = {
    types: [],
};

export default {
    Query: {
        user(_, { login }) {
            return documents.User.findByLogin(login);
        },
        currentUser({ user }) {
            return user;
        },
        async userPublic(_, { login }) {
            const user = await documents.User
                .findByLogin(login)
                .populate('location')
                .populate('items.item');

            if (!user) {
                throw new UserNotFoundError(`User ${login} not found`);
            }
            return {
                ...user.toJSON(),
                location: (user.location as any).label,
                is_online: await user.isOnline(),
                hp_current: user.abilities.hp_current,
                hp_max: user.abilities.hp_max,
            };
        },
        userRanks() {
            return documents.UserRank.find();
        },
    },
    User: {
        async items(user, { limit, page, filters = defaultItemsFilter }) {
            const { types = [] } = filters;
            const criteria = {};

            if (types.length > 0) {
                Object.assign(criteria, {
                    'item.item_type': { $in: types },
                });
            }

            // Search current store only
            Object.assign(criteria, {
                'user._id': user._id,
            });

            return paginateModel(documents.UserItem, page, limit, {
                populate: 'user',
                ...criteria,
            });
        },
        location(user) {
            return user
                .populate('location')
                .execPopulate()
                .then(u => u.location);
        },
        hp_max(user) {
            return user.abilities.hp_max;
        },
        hp_current(user) {
            return user.abilities.hp_current;
        },
    },
};
