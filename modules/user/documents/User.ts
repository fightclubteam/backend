'use strict';

import * as bcrypt from 'bcrypt';
import * as config from 'config';
import * as jwt from 'jsonwebtoken';
import * as _ from 'lodash';
import * as moment from 'moment';
import * as mongoose from 'mongoose';
import * as uuid from 'uuid';

import { documents } from '../../../_shared/documents';
import { escapeRegExp } from '../../../_shared/helper';
import { ItemInterface } from '../../item/interfaces/item';

import { ILocation } from '../../location/interfaces/location';

import { IFight } from '../../fight/interfaces/fight';

import StoreItemBuyError from '../../store/errors/StoreItemBuyError';
import { IStore } from '../../store/interfaces/store';

import { GUEST_ROLE, MAX_LOGIN_ATTEMPTS_COUNT, USER_GENDER, USER_ROLE } from '../enums/user';
import UserHasNotEnoughMoneyError from '../errors/UserHasNotEnoughMoneyError';
import UserItemNotFoundError from '../errors/UserItemNotFoundError';
import UserSetNotFoundError from '../errors/UserSetNotFoundError';
import { IUserAbility, IUser, IUserMastery } from '../interfaces/user';
import { IUserItem } from '../interfaces/user_item';

const SALT_ROUNDS = 10;
const HP_PER_ENDURANCE = 6;
const BASIC_BACKPACK_WEIGHT = 15;
const BACKPACK_WEIGHT_PER_STRENGTH = 5;
const MOD_DODGE_PER_AGILITY = 5;
const MOD_ANTI_DODGE_PER_INTUITION = 2;
const MOD_CRITICAL_PER_INTUITION = 5;
const MOD_ANTI_CRITICAL_PER_AGILITY = 2;
const MOD_ANTI_CRITICAL_PER_INTUITION = 3;
const MOD_ANTI_DODGE_PER_AGILITY = 3;
const DAMAGE_PER_STRENGTH = 3;

const itemsSetSchema = new mongoose.Schema({
    name: {
        type: String,
        required: true,
    },
    is_dressed_on: {
        type: Boolean,
        default: false,
        required: true,
    },
    items: [
        {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'Item',
            required: true,
        },
    ],
});

const userSchema = new mongoose.Schema(
    {
        email: {
            type: String,
            required: true,
            unique: true,
        },
        login: {
            type: String,
            required: true,
            unique: true,
            min: 3,
            max: 20,
        },
        password: {
            type: String,
            required: true,
        },
        register_ip: {
            type: String,
            required: true,
        },
        name: {
            type: String,
            required: true,
        },
        birthday: {
            type: Date,
        },
        sex: {
            type: String,
            enum: _.values(USER_GENDER),
            required: true,
        },
        role: {
            type: String,
            enum: _.values(USER_ROLE),
            default: USER_ROLE.USER,
        },
        free_ability: {
            type: Number,
            required: true,
            default: 99,
        },
        free_mastery: {
            type: Number,
            required: true,
            default: 99,
        },
        exp: {
            type: Number,
            required: true,
            default: 0,
        },
        level: {
            type: Number,
            required: true,
            default: 0,
        },
        rank: {
            type: String,
            required: true,
        },
        wins: {
            type: Number,
            required: true,
            default: 0,
        },
        defeats: {
            type: Number,
            required: true,
            default: 0,
        },
        draws: {
            type: Number,
            required: true,
            default: 0,
        },
        valor: {
            type: Number,
            required: true,
            default: 0,
        },
        dinars: {
            type: Number,
            required: true,
            default: 0,
        },
        timezone: {
            type: String,
        },
        login_attempts: {
            type: Number,
            default: 0,
            min: 0,
            required: true,
        },
        reset_password_hash: {
            type: String,
        },
        sent_reset_password_at: {
            type: Date,
        },
        location: {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'Location',
            required: true,
        },
        about: {
            slogan: {
                type: String,
                trim: true,
            },
            hobby: {
                type: String,
                trim: true,
            },
            city: {
                type: String,
            },
        },
        modifiers: {
            dodge: {
                type: Number,
                required: true,
                default: 0,
                get(value) {
                    return value + this.modifiers.basic_dodge;
                },
            },
            anti_dodge: {
                type: Number,
                required: true,
                default: 0,
                get(value) {
                    return value + this.modifiers.basic_anti_dodge;
                },
            },
            critical: {
                type: Number,
                required: true,
                default: 0,
                get(value) {
                    return value + this.modifiers.basic_critical;
                },
            },
            anti_critical: {
                type: Number,
                required: true,
                default: 0,
                get(value) {
                    return value + this.modifiers.basic_anti_critical;
                },
            },
        },
        mastery: {
            club: {
                type: Number,
                required: true,
                min: 0,
                default: 0,
            },
            axe: {
                type: Number,
                required: true,
                min: 0,
                default: 0,
            },
            knife: {
                type: Number,
                required: true,
                min: 0,
                default: 0,
            },
            sword: {
                type: Number,
                required: true,
                min: 0,
                default: 0,
            },
        },
        armor: {
            head: {
                type: Number,
                required: true,
                default: 0,
            },
            chest: {
                type: Number,
                required: true,
                default: 0,
            },
            abdomen: {
                type: Number,
                required: true,
                default: 0,
            },
            girdle: {
                type: Number,
                required: true,
                default: 0,
            },
            legs: {
                type: Number,
                required: true,
                default: 0,
            },
        },
        abilities: {
            strength: {
                type: Number,
                required: true,
                default: 3,
            },
            agility: {
                type: Number,
                required: true,
                default: 3,
            },
            intuition: {
                type: Number,
                required: true,
                default: 3,
            },
            endurance: {
                type: Number,
                required: true,
                default: 3,
            },
            intelligence: {
                type: Number,
                required: true,
                default: 0,
            },
            sincerity: {
                type: Number,
                required: true,
                default: 0,
            },
            spirituality: {
                type: Number,
                required: true,
                default: 0,
            },
            damage_min: {
                type: Number,
                required: true,
                default: 0,
                get(value) {
                    return value + this.abilities.basic_damage_min;
                },
            },
            damage_max: {
                type: Number,
                required: true,
                default: 0,
                get(value) {
                    return value + this.abilities.basic_damage_max;
                },
            },
            hp_max: {
                type: Number,
                required: true,
                default: 0,
                get(value) {
                    return value + this.abilities.basic_hp;
                },
            },
            backpack_weight_current: {
                type: Number,
                required: true,
                default: 0,
            },
        },
        sets: [
            {
                type: itemsSetSchema,
            },
        ],
        status: {
            silent_till: {
                type: Date,
            },
            ban_till: {
                type: Date,
            },
        },
        current_fight_info: {
            hp: {
                type: Number,
                min: 0,
                set(value) {
                    return value < 0 ? 0 : value;
                },
            },
            fight: {
                type: mongoose.Schema.Types.ObjectId,
                ref: 'Fight',
            },
            updated_at: {
                type: Date,
            },
        },
        last_fight: {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'Fight',
        },
        last_location_info: {
            location: {
                type: mongoose.Schema.Types.ObjectId,
                ref: 'Location',
            },
            moving_at: {
                type: Date,
            },
        },
    },
    {
        timestamps: { createdAt: 'created_at', updatedAt: 'updated_at' },
        toObject: {
            virtuals: true,
            getters: true,
            transform(doc, ret, options) {
                delete ret.password;
                return ret;
            },
        },
        toJSON: {
            virtuals: true,
            getters: true,
            transform(doc, ret, options) {
                delete ret.password;
                return ret;
            },
        },
    },
)
    .index({ login: 1 }, { unique: true })
    .index({ location: 1 })
    .index({ last_fight: 1 })
    .index({ 'current_fight_info.fight': 1, 'current_fight_info.hp': 1 })
    .index({ email: 1 }, { unique: true });

userSchema.pre('update', beforeSave);
userSchema.pre('save', beforeSave);

userSchema.virtual('abilities.basic_hp').get(function() {
    return this.abilities.endurance * HP_PER_ENDURANCE;
});

userSchema.virtual('abilities.basic_damage_min').get(function() {
    return Math.floor(this.abilities.strength / DAMAGE_PER_STRENGTH);
});

userSchema.virtual('abilities.basic_damage_max').get(function() {
    return this.abilities.basic_damage_min * 2;
});

userSchema.virtual('modifiers.basic_critical').get(function() {
    return MOD_CRITICAL_PER_INTUITION * this.abilities.intuition;
});

userSchema.virtual('modifiers.basic_anti_critical').get(function() {
    return (
        MOD_ANTI_CRITICAL_PER_INTUITION * this.abilities.intuition +
        MOD_ANTI_CRITICAL_PER_AGILITY * this.abilities.agility
    );
});

userSchema.virtual('modifiers.basic_dodge').get(function() {
    return MOD_DODGE_PER_AGILITY * this.abilities.agility;
});

userSchema.virtual('modifiers.basic_anti_dodge').get(function() {
    return (
        MOD_ANTI_DODGE_PER_AGILITY * this.abilities.agility + MOD_ANTI_DODGE_PER_INTUITION * this.abilities.intuition
    );
});

userSchema.virtual('abilities.mall').get(function() {
    return this.modifiers.dodge + this.modifiers.anti_dodge + this.modifiers.critical + this.modifiers.anti_critical;
});

userSchema.virtual('abilities.backpack_weight_max').get(function() {
    return BACKPACK_WEIGHT_PER_STRENGTH * this.abilities.strength + BASIC_BACKPACK_WEIGHT;
});

userSchema.virtual('abilities.energy').get(function() {
    return 0; // todo: make after tech task
});

userSchema.virtual('abilities.mall_type').get(function() {
    return ((100 / this.abilities.mall).toFixed(4) as any) * 1.0;
});

userSchema.virtual('abilities.hp_current').get(function() {
    if (
        !this.current_fight_info ||
        !(this.current_fight_info.updated_at instanceof Date) ||
        this.current_fight_info.hp >= this.abilities.hp_max
    ) {
        return this.abilities.hp_max;
    }

    // Do not regenerate HP while fighting
    if (this.current_fight_info.fight) {
        return this.current_fight_info.hp;
    }

    // todo: move it to somewhere in admin console
    const REGENERATION_TIME_MAX = 10; // 10 min
    const regeneratedHP = Math.floor(
        moment.utc().diff(moment(this.current_fight_info.updated_at), 'seconds') *
            this.abilities.hp_max /
            (REGENERATION_TIME_MAX * 60),
    );
    const currentHP = this.current_fight_info.hp + regeneratedHP;

    return currentHP >= this.abilities.hp_max ? this.abilities.hp_max : currentHP;
});

userSchema.method('forgotPassword', function() {
    // this.password = null;
    this.sent_reset_password_at = new Date();
    this.reset_password_hash = uuid.v4();

    return this.save();
});

userSchema.method('verifyPassword', function(password) {
    return bcrypt.compare(password, this.password);
});

userSchema.method('generateAuthToken', function() {
    return jwt.sign({ sub: this.id, jti: uuid.v4() } as any, this.getAuthToken(), {
        expiresIn: config.get('security.expires_in') as string,
        algorithm: 'HS512',
    });
});

userSchema.method('getAuthToken', function() {
    return (
        JSON.stringify({
            id: this.id,
            password: this.password,
            login: this.login,
        }) + config.get('security.token')
    );
});

userSchema.method('increaseLoginAttempts', function() {
    this.login_attempts++;
    return this.save();
});

userSchema.method('resetLoginAttempts', function() {
    this.login_attempts = 0;
    return this.save();
});

userSchema.method('canLogin', function() {
    return this.login_attempts < MAX_LOGIN_ATTEMPTS_COUNT;
});

userSchema.method('hasLowHP', function() {
    return this.abilities.hp_current <= this.abilities.hp_max / 3;
});

userSchema.method('canMove', async function(location: ILocation) {
    // location instance
    // todo: move to some admin console configuration
    if (!this.last_location_info || !(this.last_location_info.moving_at instanceof Date)) {
        return false;
    }

    if (Date.now() - this.last_location_info.moving_at.valueOf() < location.travel_time) {
        return false;
    }

    await this.populate('last_location_info.location');

    const isNearbyLocation = await location.isNearbyLocation(this.last_location_info.location);
    if (!isNearbyLocation) {
        return false;
    }

    // todo: add other logic: injury, death and etc...

    return true;
});

userSchema.method('getRoles', function() {
    return [].concat(this.role);
});

userSchema.method('isGuest', function() {
    return this.role === GUEST_ROLE;
});

userSchema.method('updateStats', function(params: { abilities: IUserAbility; mastery: IUserMastery }) {
    const { abilities, mastery } = params;
    const abilitiesValues = _.values(abilities);
    const masteryValues = _.values(mastery);
    if (
        _.filter(abilitiesValues, value => value < 0).length > 0 ||
        _.filter(masteryValues, value => value < 0).length > 0
    ) {
        throw new Error('User stats should be only positive value.');
    }

    const countAbilitiesToUpdate = _.sum(abilitiesValues);
    const countMasteryToUpdate = _.sum(masteryValues);
    if (
        (countAbilitiesToUpdate > this.free_ability || countAbilitiesToUpdate < 1) &&
        (countMasteryToUpdate > this.free_mastery || countMasteryToUpdate < 1)
    ) {
        throw new Error('Error while updating user stats.');
    }

    this.abilities.agility += abilities.agility;
    this.abilities.strength += abilities.strength;
    this.abilities.intuition += abilities.intuition;
    this.abilities.endurance += abilities.endurance;
    this.abilities.intelligence += abilities.intelligence;
    this.abilities.spirituality += abilities.spirituality;
    this.abilities.sincerity += abilities.sincerity;

    this.mastery.club += mastery.club;
    this.mastery.axe += mastery.axe;
    this.mastery.knife += mastery.knife;
    this.mastery.sword += mastery.sword;

    this.free_ability -= countAbilitiesToUpdate;
    this.free_mastery -= countMasteryToUpdate;
    return this.save();
});

userSchema.method('getDamageBetweenMinMax', function() {
    return _.random(this.abilities.damage_min, this.abilities.damage_max, false);
});

userSchema.method('isDead', function() {
    return (
        this.current_fight_info &&
        this.current_fight_info.updated_at instanceof Date &&
        this.current_fight_info.hp === 0
    );
});

userSchema.static('getCommonMallType', function(mallType1: number, mallType2: number) {
    return Math.min(mallType1, mallType2);
});

userSchema.static('findUserForFight', function(fight: IFight, login: string) {
    return UserModel.findOne({ login, 'current_fight_info.fight': fight });
});

userSchema.method('addItem', function(item: ItemInterface) {
    this.abilities.backpack_weight_current += item.weight;
    return documents.UserItem.create({
        item,
        user: this,
        is_dressed_on: false,
    });
});

userSchema.method('removeItem', function(item: IUserItem) {
    this.abilities.backpack_weight_current -= item.item.weight;
    if (item.user._id.toString() !== this._id.toString()) {
        throw new UserItemNotFoundError('User "' + this.login + '" does not have item named "' + item.item.title);
    }
    return item.remove();
});

userSchema.method('getItems', function(filter = {}) {
    return documents.UserItem.find({
        'user._id': this._id,
        ...filter,
    });
});

userSchema.method('getItemsCount', function(filter = {}) {
    return documents.UserItem.count({
        'user._id': this._id,
        ...filter,
    });
});

userSchema.method('buyItemFromStore', async function(store: IStore, item: ItemInterface) {
    let isBought = false;
    if (isFinite(item.price)) {
        if (item.price > this.dinars) {
            throw new UserHasNotEnoughMoneyError(
                'You do not have enough credits to buy this amount of "' + item.title + '"',
            );
        }
        this.dinars -= item.price;
        isBought = true;
    }
    if (isBought) {
        // Save money
        const newItem = await this.addItem(item);
        await this.save();
        return newItem;
    }
    throw new StoreItemBuyError('Item cannot be bought');
});

userSchema.method('sellItemToStore', async function(store: IStore, item: IUserItem) {
    let isSold = false;
    if (isFinite(item.item.price)) {
        this.dinars += Math.floor(item.item.price * (1 - store.tax || 0));
        isSold = true;
    }
    if (isSold) {
        // Save money
        const newItem = await this.removeItem(item);
        await this.save();
        return newItem;
    }
    return item;
});

userSchema.method('getAttackCount', async function() {
    const itemsCount = await this.getItemsCount({
        is_dressed_on: true,
        'item.item_type': {
            $in: ['KNIFE_LEFT', 'AXE', 'SWORD', 'MACE', 'KNIFE'],
        },
    });
    return itemsCount >= 2 ? 2 : 1;
});

userSchema.method('getDefendCount', async function() {
    const itemsCount = await this.getItemsCount({
        is_dressed_on: true,
        'item.item_type': 'SHIELD',
    });
    return itemsCount >= 1 ? 3 : 2;
});

userSchema.method('isOnline', async function() {
    // todo: move out to UserRoom `documents.UserRoom.userExists`
    const userRoomCount = await documents.UserRoom.count({ 'users.login': this.login });
    return userRoomCount > 0;
});

userSchema.method('recalculateStatsUserForItem', function(itemInfo: any, multiplier: number) {
    let basicHp = this.abilities.basic_hp;

    for (const ability in itemInfo.item.abilities) {
        if (this.abilities.hasOwnProperty(ability)) {
            if (_.includes(['damage_min', 'damage_max'], ability)) {
                // multiply by 2, because getter always return basic value
                this.abilities[ability] -= 2 * this.abilities[`basic_${ability}`];
            }
            this.abilities[ability] += itemInfo.item.abilities[ability] * multiplier;
        }
    }

    for (const mastery in itemInfo.item.mastery) {
        if (this.mastery.hasOwnProperty(mastery)) {
            this.mastery[mastery] += itemInfo.item.mastery[mastery] * multiplier;
        }
    }

    for (const modifier in itemInfo.item.modifiers) {
        if (itemInfo.item.modifiers.hasOwnProperty(modifier) && this.modifiers.hasOwnProperty(modifier)) {
            this.modifiers[modifier] +=
                itemInfo.item.modifiers[modifier] * multiplier - this.modifiers[`basic_${modifier}`];
        }
    }

    for (const armor in itemInfo.item.armor) {
        if (this.armor.hasOwnProperty(armor)) {
            this.armor[armor] += itemInfo.item.armor[armor] * multiplier;
        }
    }

    this.abilities.backpack_weight_current -= itemInfo.item.weight * multiplier;
    basicHp -= itemInfo.item.abilities.hp * multiplier;

    this.abilities.hp_max -= basicHp;

    return this;
});

userSchema.method('takeClothesOff', async function() {
    await documents.User.takeClothesOff(this);
    return this;
});

userSchema.method('takeClothOn', async function(item: IUserItem) {
    if (item.user._id.toString() !== this._id.toString()) {
        throw new UserItemNotFoundError(`User has no item ${item.item._id}.`);
    }

    if (_.get(this, 'current_fight_info.fight', null) !== null) {
        throw new Error(); // todo: make custom error
    }

    if (item.is_dressed_on) {
        // Nothing to do, but we should not throw exception
        return this;
    }

    const dressedItems = await this.getItems({ is_dressed_on: true });
    const itemsToUndress = _.filter(dressedItems, (itemInfo: IUserItem) =>
        documents.Item.isItemsConflict(
            itemInfo.item,
            item.item,
            _.filter(
                dressedItems,
                (userItem: IUserItem) =>
                    userItem.is_dressed_on === true && userItem.item.item_type === itemInfo.item.item_type,
            ).length,
        ),
    );
    await Promise.all(_.map(itemsToUndress, itemInfo => this.takeClothOff(itemInfo)));

    item.is_dressed_on = true;
    await item.save();
    this.recalculateStatsUserForItem(item, 1);
    return this.save();
});

userSchema.method('takeClothOff', async function(item: IUserItem) {
    if (item.user._id.toString() !== this._id.toString()) {
        throw new UserItemNotFoundError(`User has no item ${item.item._id}.`);
    }

    if (_.get(this, 'current_fight_info.fight', null) !== null) {
        throw new Error(); // todo: make custom error
    }

    if (item.is_dressed_on) {
        item.is_dressed_on = false;
        await item.save();
        this.recalculateStatsUserForItem(item, -1);
        return this.save();
    }
    return this;
});

userSchema.method('takeSetOff', async function(setId: string) {
    // Find set by ID among all user sets
    const userSet = _.find(this.sets, (set: any) => set._id.toString() === setId);
    if (!userSet) {
        throw new UserSetNotFoundError('Set not found.');
    }
    // Find all items which are in the set and already dressed on user
    const dressedItems = await this.getItems({
        is_dressed_on: true,
        _id: {
            $in: userSet.items,
        },
    });
    // Undress all dressed on items
    await Promise.all(_.map(dressedItems, (item: IUserItem) => this.takeClothOff(item)));
    // Undress set itself
    userSet.is_dressed_on = false;
    return this.save();
});

userSchema.method('takeSetOn', async function(setId: string) {
    // Find set by ID among all user sets
    const userSet = _.find(this.sets, (set: any) => set._id.toString() === setId);
    if (!userSet) {
        throw new UserSetNotFoundError('Set not found.');
    }
    // Undress all dressed on sets
    await Promise.all(
        _.map(_.filter(this.sets, (set: any) => set.is_dressed_on === true), (set: any) =>
            this.takeSetOff(set._id.toString()),
        ),
    );
    // Find all items which are in the set and still not dressed
    const setItems = await this.getItems({
        is_dressed_on: false,
        _id: {
            $in: userSet.items,
        },
    });
    // Dress all items found on previous step
    await Promise.all(_.map(setItems, (item: IUserItem) => this.takeClothOn(item)));
    // Dress set itself
    userSet.is_dressed_on = true;
    return this.save();
});

userSchema.method('createSet', async function(name: string, items: string[], setId?: string) {
    // Find all items already dressed on user
    const userItems = await this.getItems({ is_dressed_on: true });
    _.each(userItems, (userItem: IUserItem) => {
        if (!_.includes(items, userItem._id.toString())) {
            throw new Error('Items are not on user.');
        }
    });
    const userSet = {
        name,
        items: _.map(items, (itemId: any) => new mongoose.Types.ObjectId(itemId._id || itemId)),
        is_dressed_on: true,
    };
    if (setId) {
        const filteredSet = _.find(this.sets, (set: any) => set._id.toString() === setId);
        if (!filteredSet) {
            throw new UserSetNotFoundError('Set not found.');
        }
        Object.assign(filteredSet, userSet);
    } else {
        this.sets.push(userSet);
    }
    return this.save();
});

userSchema.method('deleteSet', function(setId: string) {
    this.sets = _.reject(this.sets, (set: any) => set._id.toString() === setId);
    return this.save();
});

userSchema.static({
    /**
     * @param {string} email User email
     */
    findByEmail(email: string) {
        return UserModel.findOne({ email: new RegExp(`^${escapeRegExp(email)}$`, 'i') });
    },

    /**
     * @param {string} login User login
     */
    findByLogin(login: string) {
        return UserModel.findOne({ login: new RegExp(`^${escapeRegExp(login)}$`, 'i') });
    },

    /**
     * @param {string} username User email or login
     */
    findByUsername(username: string) {
        const escapedUsername = new RegExp(`^${escapeRegExp(username)}$`, 'i');
        return UserModel.findOne({
            $or: [{ login: escapedUsername }, { email: escapedUsername }],
        });
    },

    updateLastFight(fight: IFight) {
        const bulk = UserModel.collection.initializeUnorderedBulkOp();
        _.each(fight.getAllTeamLoginList(), login => {
            const field = fight.isDraw() ? 'draws' : fight.isUserWinner(login) ? 'wins' : 'defeats';
            bulk.find({ login }).updateOne({
                $set: {
                    last_fight: fight._id,
                    'current_fight_info.fight': null,
                    updated_at: fight.finished_at,
                },
                $inc: {
                    [field]: 1,
                    exp: fight.isUserWinner(login) ? 20 : 0,
                },
            });
        });

        return bulk.execute();
    },

    async setUsersCurrentFight(fight: IFight) {
        const now = new Date();
        const userInfo = _.map(fight.team.right.concat(fight.team.left), user => ({
            hp: user.hp_current,
            login: user.login,
        }));

        if (fight.isFistful()) {
            const users = await UserModel.find({ login: { $in: _.map(userInfo, 'login') } });
            const promises = [];
            for (let i = 0, l = users.length; i < l; i++) {
                const user = users[i];
                await documents.User.takeClothesOff(user);
                user.current_fight_info = {
                    fight: fight._id,
                    updated_at: now,
                    hp: user.abilities.hp_current,
                } as any;
                const teamUser =
                    _.find(fight.team.right, { login: user.login }) || _.find(fight.team.left, { login: user.login });
                teamUser.hp_current = user.abilities.hp_current;
                teamUser.hp_max = user.abilities.hp_max;
                promises.push(user.save());
            }
            return await Promise.all(promises);
        } else {
            const bulk = UserModel.collection.initializeUnorderedBulkOp();
            _.each(userInfo, userDataInfo => {
                bulk.find({ login: userDataInfo.login }).updateOne({
                    $set: {
                        'current_fight_info.fight': fight._id,
                        'current_fight_info.updated_at': now,
                        'current_fight_info.hp': userDataInfo.hp,
                    },
                });
            });
            return bulk.execute();
        }
    },

    getResourceId() {
        return UserModel.modelName;
    },

    async takeClothesOff(user: IUser) {
        if (_.get(user, 'current_fight_info.fight', null) !== null) {
            throw new Error(); // todo: make custom error
        }
        const itemsToUndress = await user.getItems({ is_dressed_on: true });
        await Promise.all(_.map(itemsToUndress, item => user.takeClothOff(item)));
    },
});

/**
 * @param {function} next
 * @returns {*}
 */
async function beforeSave(next) {
    if (this.isModified('password')) {
        this.password = await bcrypt.hash(this.password, SALT_ROUNDS);
        this.sent_reset_password_at = null;
        this.reset_password_hash = null;
    }

    // todo check what is it
    // if (this.isModified('items')) {
    //     const itemsIds = _.map(this.items, (item: any) => (item.item._id || item.item).toString());
    //     _.each(this.sets, (userSet: any) => {
    //         userSet.items = _.map(_.intersection(
    //             itemsIds,
    //             _.map(userSet.items, (item: any) => item.toString())
    //         ), (itemId) => new mongoose.Types.ObjectId(itemId._id || itemId));
    //     });
    // }

    return next();
}

const UserModel = mongoose.model<IUser>('User', userSchema);
export default UserModel;
