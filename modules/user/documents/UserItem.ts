'use strict';

import * as mongoose from 'mongoose';

import UserItemNotFoundError from '../errors/UserItemNotFoundError';
import { IUserItem } from '../interfaces/user_item';

const userItemSchema = new mongoose.Schema(
    {
        item: mongoose.Schema.Types.Mixed,
        user: mongoose.Schema.Types.Mixed,
        is_dressed_on: {
            type: Boolean,
            default: false,
        },
    },
    {
        collection: 'user_items',
        timestamps: { createdAt: 'created_at', updatedAt: 'updated_at' },
    },
);

userItemSchema.index({ 'item._id': 1, 'user._id': 1 });
userItemSchema.index({ 'item.item_type': 1 });
userItemSchema.index({ 'item.min_user_rank': 1 });
userItemSchema.index({ 'item.min_user_level': 1 });

userItemSchema.static({
    async findByIdOrFail(userItemId: string) {
        if (!userItemId) {
            throw new UserItemNotFoundError('User ID is required');
        }

        const userItem = await UserItemModel.findById(userItemId);

        if (!userItem) {
            throw new UserItemNotFoundError('User with such ID is not found');
        }

        return userItem;
    },
});

const UserItemModel = mongoose.model<IUserItem>('UserItem', userItemSchema);
export default UserItemModel;
