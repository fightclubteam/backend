'use strict';

import * as mongoose from 'mongoose';
import * as jwt from 'jsonwebtoken';
import { IUserRoom } from '../interfaces/user';
import { documents } from '../../../_shared/documents';
import { pubSub } from '../../../_shared/pub-sub';

const userRoomSchema = new mongoose.Schema(
    {
        ws_id: {
            type: String,
            required: true,
        },
        login: {
            type: String,
            required: true,
        },
        level: {
            type: Number,
            required: true,
        },
        location: {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'Location',
            required: true,
        },
    },
    {
        timestamps: { createdAt: 'created_at', updatedAt: 'updated_at' },
        collection: 'user_rooms',
    },
)
    .index({ login: 1 })
    .index({ location: 1 })
    .index({ ws_id: 1 }, { unique: true });

userRoomSchema.static('addUserByJwtToken', async function(wsId: string, token: string) {
    let userId = null;
    try {
        const { sub } = jwt.decode(token) as any;
        userId = sub;
    } catch (e) {
        console.log(e);
    }
    if (!userId) {
        return;
    }
    // todo: add user is active end else
    const user = await documents.User.findById(userId);
    if (!user) {
        return console.log('User not found from JWT token.');
    }

    const userRoom = await UserRoomModel.findOneAndUpdate(
        { login: user.login },
        { ws_id: wsId, login: user.login, level: user.level, location: user.location },
        { new: true, upsert: true },
    );

    pubSub.publish('userLocationJoined', userRoom);
    return user;
});

userRoomSchema.static('removeUserByWsId', async function(wsId: string) {
    const userRoom = await UserRoomModel.findOneAndRemove({ ws_id: wsId });
    if (!userRoom) {
        return;
    }
    pubSub.publish('userLocationLeaved', userRoom);
});

const UserRoomModel = mongoose.model<IUserRoom>('UserRoom', userRoomSchema);
export default UserRoomModel;
