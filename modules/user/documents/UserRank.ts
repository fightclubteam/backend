'use strict';

import * as mongoose from 'mongoose';
import { IUserRank } from '../interfaces/user';

const userRankSchema = new mongoose.Schema(
    {
        slug: String,
        position: Number,
        title: String,
    },
    {
        timestamps: false,
        versionKey: false,
        collection: 'user_ranks',
    },
);

const UserRankModel = mongoose.model<IUserRank>('UserRank', userRankSchema);
export default UserRankModel;
