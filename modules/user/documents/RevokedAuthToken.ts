'use strict';

import * as config from 'config';
import * as mongoose from 'mongoose';
import { IRevokedAuthToken } from '../interfaces/revoked-auth-token';

const revokedAuthTokenSchema = new mongoose.Schema(
    {
        user: {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'User',
        },
        jti: {
            type: String,
            require: true,
        },
        iat: {
            type: Date,
            require: true,
            expires: +config.get('security.expires_in'),
        },
    },
    {
        collection: 'revoked_auth_tokens',
        timestamps: null,
    },
)
    .index({ user: 1 })
    .index({ jti: 1 }, { unique: true });

export default mongoose.model<IRevokedAuthToken>('RevokedAuthToken', revokedAuthTokenSchema);
