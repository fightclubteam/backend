'use strict';

import { UserError } from './UserError';

export default class UserExistsError extends UserError {
    constructor(public message: string, public code?: number | string, public parent?: Error) {
        super(message, code, parent);
    }
}
