'use strict';

import { ApplicationError } from '../../../errors';

export abstract class UserError extends ApplicationError {
    constructor(public message: string, public code?: number | string, public parent?: Error) {
        super(message, code, parent);
    }
}
