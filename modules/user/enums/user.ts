export const GUEST_ROLE = 'guest';

export const USER_GENDER = Object.freeze({
    MALE: 'MALE',
    FEMALE: 'FEMALE',
});

export const USER_ROLE = Object.freeze({
    ADMIN: 'ADMIN',
    DEALER: 'DEALER',
    CLAN_HEAD: 'CLAN_HEAD',
    MODERATOR: 'MODERATOR',
    USER: 'USER',
});
export const MAX_LOGIN_ATTEMPTS_COUNT = 5;
export const FORGOT_PASSWORD_EXPIRATION = 86400; // 1 day in seconds
