'use strict';

import * as mongoose from 'mongoose';
import { ILocationObject } from './location.object';
import { IUserRoom } from '../../user/interfaces/user';
import { IAclResource } from '../../../_shared/common.interfaces';

export interface ILocation extends mongoose.Document, IAclResource {
    id?: string;
    name?: string;
    label?: string;
    image?: string;
    can_attack?: boolean;
    is_default?: boolean;
    travel_time?: number;
    can_create_fight?: boolean;
    nearby_locations: ILocation[];
    objects: ILocationObject[];
    users: Promise<IUserRoom[]>;
    isNearbyLocation(location: ILocation): Promise<boolean>;
}

export interface ILocationDocument extends mongoose.Model<ILocation> {
    findDefaultLocation(): Promise<ILocation>;
}
