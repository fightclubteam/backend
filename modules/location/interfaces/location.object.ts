'use strict';

import * as mongoose from 'mongoose';

export interface ILocationObject extends mongoose.Document {
    name?: string;
    label?: string;
    image?: string;
    coordinates?: string;
}

export interface ILocationObjectDocument extends mongoose.Model<ILocationObject> {}
