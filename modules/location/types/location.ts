'use strict';

export default `
    type LocationObjectType {
        name: String!
        label: String!
        image: String!
        coordinates: String!
    }
    
    type UserLocation {
        login: String!
        level: Int!
    }
    
    type LocationType {
        _id: String!
        name: String!
        label: String!
        image: String!
        can_attack: Boolean!
        can_create_fight: Boolean!
        is_default: Boolean!
        objects: [LocationObjectType]
        nearby_locations: [LocationType]
        users: [UserLocation]!
    }
    
    type Subscription {
        userLocationJoined(location: String!): UserLocation!
        userLocationLeaved(location: String!): UserLocation!
    }
    
    type Mutation {
        changeLocation(locationName: String!): LocationType!
    }
`;
