'use strict';

import * as mongoose from 'mongoose';
import { ILocation } from '../interfaces/location';

const locationSchema = new mongoose.Schema(
    {
        name: {
            type: String,
            required: true,
            unique: true,
        },
        label: {
            type: String,
            required: true,
        },
        image: {
            type: String,
            required: true,
        },
        can_attack: {
            type: Boolean,
            required: true,
        },
        can_create_fight: {
            type: Boolean,
            required: true,
        },
        is_default: {
            type: Boolean,
            required: true,
            default: false,
        },
        travel_time: {
            // in sec
            type: Number,
            min: 0,
            default: 5,
        },
        nearby_locations: [
            {
                type: mongoose.Schema.Types.ObjectId,
                ref: 'Location',
            },
        ],
        objects: [
            {
                type: mongoose.Schema.Types.ObjectId,
                ref: 'LocationObject',
            },
        ],
    },
    {
        collection: 'locations',
        timestamps: null,
    },
)
    .index({ name: 1 }, { unique: true })
    .index({ is_default: 1 })
    .index({ 'objects.name': 1 });

locationSchema.virtual('users', {
    ref: 'UserRoom',
    localField: '_id',
    foreignField: 'location',
});

locationSchema.method('isNearbyLocation', async function(location) {
    const locationWithRelated = await this.populate({
        path: 'nearby_locations',
        match: { name: location },
    });

    return locationWithRelated.nearby_locations.length > 0;
});

locationSchema.static({
    getResourceId() {
        return LocationModel.modelName;
    },
});

locationSchema.static('findDefaultLocation', function() {
    return LocationModel.findOne({ is_default: true });
});

const LocationModel = mongoose.model<ILocation>('Location', locationSchema);

export default LocationModel;
