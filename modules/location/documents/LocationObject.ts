'use strict';

import * as mongoose from 'mongoose';
import { ILocationObject } from '../interfaces/location.object';

const locationObjectSchema = new mongoose.Schema(
    {
        name: {
            type: String,
            required: true,
        },
        label: {
            type: String,
            required: true,
        },
        image: {
            type: String,
            required: true,
        },
        coordinates: {
            type: String,
            required: true,
        },
    },
    {
        collection: 'location_objects',
        timestamps: null,
    },
).index({ name: 1 }, { unique: true });

const LocationObjectModel = mongoose.model<ILocationObject>('LocationObject', locationObjectSchema);

export default LocationObjectModel;
