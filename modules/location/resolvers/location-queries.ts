'use strict';

export default {
    Query: {},
    LocationType: {
        objects(location) {
            return location
                .populate('objects')
                .execPopulate()
                .then(l => l.objects);
        },
        nearby_locations(location) {
            return location
                .populate('nearby_locations')
                .execPopulate()
                .then(l => l.nearby_locations);
        },
        users(location) {
            return location
                .populate('users')
                .execPopulate()
                .then(l => l.users);
        },
    },
};
