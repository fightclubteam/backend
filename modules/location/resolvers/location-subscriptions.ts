'use strict';

import { pubSub } from '../../../_shared/pub-sub';
import { withFilter } from 'graphql-subscriptions';

export default {
    Subscription: {
        userLocationJoined: {
            subscribe: withFilter(
                () => pubSub.asyncIterator('userLocationJoined'),
                (room, { location }) => {
                    return room && String(room.location) === String(location);
                },
            ),
            resolve: payload => payload,
        },
        userLocationLeaved: {
            subscribe: withFilter(
                () => pubSub.asyncIterator('userLocationLeaved'),
                (room, { location }) => {
                    return room && String(room.location) === String(location);
                },
            ),
            resolve: payload => payload,
        },
    },
};
