'use strict';

import { mergeResolvers } from 'merge-graphql-schemas';

import locationMutationsResolvers from './location-mutations';
import locationQueriesResolvers from './location-queries';
import locationSubscriptionsResolvers from './location-subscriptions';

export default mergeResolvers([locationMutationsResolvers, locationQueriesResolvers, locationSubscriptionsResolvers]);
