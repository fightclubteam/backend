'use strict';

import { GraphQLObjectType, GraphQLList, GraphQLInt } from 'graphql';
import { documents } from '../../../_shared/documents';
// import {nodeInterface} from '../../../_shared/sequelize.node.interface';
// import {UserType} from '../../user/graphql'

// const graphqlSequelize = require('graphql-sequelize');
// const graphqlRelay = require('graphql-relay');
//
// const locationResolverFn = graphqlSequelize.resolver(models.Location.CanMove, {
//     before: function (options, args) {
//         options.order = options.order || [];
//         options.attributes.push([
//             literal('MoveFrom.order'),
//             'order'
//         ]);
//         return options;
//     }
// });
//
// const LocationObjectType = new GraphQLObjectType({
//     name: (models.LocationObject as any).name,
//     description: 'Location object',
//     fields: graphqlSequelize.attributeFields(models.LocationObject, {
//         exclude: [
//             'location_id',
//         ]
//     })
// });
// //
// export const LocationType = new GraphQLObjectType({
//     name: (documents.Location as any).name,
//     description: 'Location',
//     fields: () => ({
//         users_count: {
//             type: GraphQLInt,
//             async resolve(source) {
//                 let usersCount = await documents.User.count({
//                     where: {location_id: source.getDataValue('id')},
//                 });
//                 if (usersCount !== null) {
//                     return usersCount
//                 }
//             }
//         },
//         order: {
//             type: GraphQLInt,
//             resolve: (source, args, req, info): number => source.dataValues[info.fieldName]
//         },
//     }),
//     interfaces: [nodeInterface]
// });
