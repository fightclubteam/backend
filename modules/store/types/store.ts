'use strict';

export default `
    type StoreType implements Node {
        _id: ID!
        title: String!
        slug: String!
        tax: Float!
        items(limit: Int = 10, page: Int = 1, filters: ItemFilterType): PaginatedStoreItemType!
        myItems(limit: Int = 10, page: Int = 1, filters: ItemFilterType): PaginatedUserItemType!
    }
    
    type StoreItemType implements Node {
        _id: ID!
        item: ItemType!
        store: StoreType!
    }
    
    input StoreInputType {
        title: String!
        slug: String!
        tax: Float!
    }
    
    type Query {
        stores: [StoreType]
        store(store_id: String!): StoreType
        storeBySlug(slug: String!): StoreType
    }
    
    type Mutation {
        createStore(store: StoreInputType!): StoreType!
        removeStore(store_id: String!): Boolean
        
        addItemToStore(store_id: String!, item_id: String!): StoreType!
        removeItemFromStore(store_id: String!, item_id: String!): StoreType!
    }
`;
