'use strict';

import { StoreError } from './StoreError';

export default class StoreNotFoundError extends StoreError {
    constructor(public message: string, public code?: number | string, public parent?: Error) {
        super(message, code, parent);
    }
}
