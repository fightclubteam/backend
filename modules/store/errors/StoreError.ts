'use strict';

import { ApplicationError } from '../../../errors';

export abstract class StoreError extends ApplicationError {
    constructor(public message: string, public code?: number | string, public parent?: Error) {
        super(message, code, parent);
    }
}
