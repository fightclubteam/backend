'use strict';

import { StoreError } from './StoreError';

export default class StoreItemBuyError extends StoreError {
    constructor(public message: string, public code?: number | string, public parent?: Error) {
        super(message, code, parent);
    }
}
