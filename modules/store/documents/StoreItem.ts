'use strict';

import * as mongoose from 'mongoose';

import StoreItemNotFoundError from '../errors/StoreItemNotFoundError';
import { IStoreItem } from '../interfaces/store_item';

const storeItemSchema = new mongoose.Schema(
    {
        item: mongoose.Schema.Types.Mixed,
        store: mongoose.Schema.Types.Mixed,
    },
    {
        collection: 'store_items',
        timestamps: { createdAt: 'created_at', updatedAt: 'updated_at' },
    },
);

storeItemSchema.index({ 'item._id': 1, 'store._id': 1 });
storeItemSchema.index({ 'item.item_type': 1 });
storeItemSchema.index({ 'item.min_user_rank': 1 });
storeItemSchema.index({ 'item.min_user_level': 1 });

storeItemSchema.static({
    async findByIdOrFail(storeItemId: string) {
        if (!storeItemId) {
            throw new StoreItemNotFoundError('Store ID is required');
        }

        const storeItem = await StoreItemModel.findById(storeItemId);

        if (!storeItem) {
            throw new StoreItemNotFoundError('Store with such ID is not found');
        }

        return storeItem;
    },
});

const StoreItemModel = mongoose.model<IStoreItem>('StoreItem', storeItemSchema);
export default StoreItemModel;
