'use strict';

import * as mongoose from 'mongoose';

import { documents } from '../../../_shared/documents';
import { ItemInterface } from '../../item/interfaces/item';

import StoreItemNotFoundError from '../errors/StoreItemNotFoundError';
import StoreNotFoundError from '../errors/StoreNotFoundError';
import { IStore } from '../interfaces/store';
import { IStoreItem } from '../interfaces/store_item';

export const storeSchema = new mongoose.Schema(
    {
        title: {
            type: String,
            required: true,
        },
        slug: {
            type: String,
            required: true,
            unique: true,
        },
        tax: {
            type: Number,
            required: true,
            default: 0,
            min: 0,
            max: 1,
        },
    },
    {
        timestamps: { createdAt: 'created_at', updatedAt: 'updated_at' },
        toObject: {
            virtuals: true,
        },
        toJSON: {
            virtuals: true,
        },
    },
);

storeSchema.method('addItem', async function(item: ItemInterface) {
    await documents.StoreItem.create({
        item,
        store: this,
    });
    return this;
});

storeSchema.method('removeItem', function(item: IStoreItem) {
    if (item.store.toString() !== this._id.toString()) {
        throw new StoreItemNotFoundError('Provided item does not belong to current store');
    }
    return item.remove();
});

storeSchema.method('getItems', function(filter: object) {
    return documents.StoreItem.find({
        store: this._id,
        ...filter,
    });
});

storeSchema.method('getItemsCount', function(filter: object) {
    return documents.StoreItem.count({
        store: this._id,
        ...filter,
    });
});

storeSchema.static({
    async findByIdOrFail(storeId: string) {
        if (!storeId) {
            throw new StoreNotFoundError('Store ID is required');
        }

        const store = await StoreModel.findById(storeId);

        if (!store) {
            throw new StoreNotFoundError('Store with such ID is not found');
        }

        return store;
    },
});

const StoreModel = mongoose.model<IStore>('Store', storeSchema);
export default StoreModel;
