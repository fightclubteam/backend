'use strict';

import * as mongoose from 'mongoose';

import { IFindableByIdOrFail } from '../../../_shared/common.interfaces';
import { IItemsCollectionAware } from '../../item/interfaces/item';
import { IStoreItem } from './store_item';

export interface IStore extends mongoose.Document, IItemsCollectionAware<IStore, IStoreItem> {
    title: string;
    slug: string;
    tax: number;
}

export interface IStoreDocument
    extends mongoose.Model<IStore>,
        IFindableByIdOrFail<IStore>,
        IItemsCollectionAware<IStore, IStoreItem> {}
