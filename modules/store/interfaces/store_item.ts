'use strict';

import * as mongoose from 'mongoose';

import { IFindableByIdOrFail } from '../../../_shared/common.interfaces';

import { ItemInterface } from '../../item/interfaces/item';
import { IStore } from './store';

export interface IStoreItem extends mongoose.Document {
    item: ItemInterface;
    store: IStore;
}

export interface IStoreItemDocument extends mongoose.Model<IStoreItem>, IFindableByIdOrFail<IStoreItem> {}
