'use strict';

import { mergeResolvers } from 'merge-graphql-schemas';

import storeMutationsResolvers from './store-mutations';
import storeQueriesResolvers from './store-queries';
import storeSubscriptionsResolvers from './store-subscriptions';

export default mergeResolvers([storeQueriesResolvers, storeMutationsResolvers, storeSubscriptionsResolvers]);
