'use strict';

import { documents } from '../../../_shared/documents';
import { paginateModel } from '../../../_shared/helpers/pagination/pagination';

import { IItemsQuery } from '../../item/interfaces/item';

const defaultItemsFilter = {
    min_user_level: 0,
    min_user_rank: 0,
    types: [],
};

export default {
    Query: {
        stores(rootContext, args) {
            return documents.Store.find().limit(args.limit || 10);
        },
        store(rootContext, { store_id }) {
            return documents.Store.findById(store_id);
        },
        storeBySlug(rootContext, { slug }) {
            return documents.Store.findOne({ slug });
        },
    },
    StoreType: {
        async items(store, { limit, page, filters = defaultItemsFilter }: IItemsQuery, { user }) {
            const { min_user_level = 0, min_user_rank = 0, types = [] } = filters;
            const criteria = {};

            if (min_user_level > 0) {
                criteria['item.restrictions.min_user_level'] = {
                    $gt: min_user_level,
                };
            }

            if (min_user_rank > 0) {
                criteria['item.restrictions.min_user_rank'] = {
                    $gt: min_user_rank,
                };
            }

            if (types.length > 0) {
                Object.assign(criteria, {
                    'item.item_type': { $in: types },
                });
            }

            // Search current store only
            Object.assign(criteria, {
                'store._id': store._id,
            });

            return paginateModel(documents.StoreItem, page, limit, {
                populate: 'store',
                ...criteria,
            });
        },
        async myItems(store, { limit, page, filters = defaultItemsFilter }: IItemsQuery, { user }) {
            if (!user || user._id === '-1') {
                return [];
            }

            const { min_user_level = 0, min_user_rank = 0, types = [] } = filters;
            const criteria = {};

            if (min_user_level > 0) {
                criteria['item.restrictions.min_user_level'] = {
                    $gt: min_user_level,
                };
            }

            if (min_user_rank > 0) {
                criteria['item.restrictions.min_user_rank'] = {
                    $gt: min_user_rank,
                };
            }

            if (types.length > 0) {
                Object.assign(criteria, {
                    'item.item_type': { $in: types },
                });
            }

            // Search current store only
            Object.assign(criteria, {
                'user._id': user._id,
            });

            return paginateModel(documents.UserItem, page, limit, {
                populate: 'user',
                ...criteria,
            });
        },
    },
};
