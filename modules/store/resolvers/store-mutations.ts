'use strict';

import { documents } from '../../../_shared/documents';

export default {
    Mutation: {
        createStore(rootContext, { store }) {
            return documents.Store.create(store);
        },
        removeStore(rootContext, { store_id }) {
            return documents.Store.findByIdAndRemove(store_id);
        },
        async addItemToStore(rootContext, { store_id, item_id }) {
            const [store, item] = await Promise.all([
                documents.Store.findByIdOrFail(store_id),
                documents.Item.findByIdOrFail(item_id),
            ]);
            return store.addItem(item);
        },
        async removeItemFromStore(rootContext, { store_id, item_id }) {
            const [store, item] = await Promise.all([
                documents.Store.findByIdOrFail(store_id),
                documents.StoreItem.findByIdOrFail(item_id),
            ]);
            return store.removeItem(item);
        },
    },
};
