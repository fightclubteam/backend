'use strict';

import * as md5 from 'md5';
import { emojify } from 'node-emoji';
import { pubSub } from '../../../_shared/pub-sub';

export default {
    Mutation: {
        addChatMessage({ user }, args) {
            // don't need save it into db, just display it on client side
            const message: any = {
                sender: user.login,
                message: args.message,
                is_private: !!args.is_private,
                receivers: [].concat(args.receivers),
                created_at: new Date(),
            };

            message._id = md5(JSON.stringify(message));

            message.message = emojify(message.message, name => name);

            pubSub.publish('chatMessageAdded', message);

            return message;
        },
    },
};
