'use strict';

import * as _ from 'lodash';
import { withFilter } from 'graphql-subscriptions';
import { pubSub } from '../../../_shared/pub-sub';

export default {
    Subscription: {
        chatMessageAdded: {
            subscribe: withFilter(
                () => pubSub.asyncIterator('chatMessageAdded'),
                (message, { receiver }) =>
                    !message.is_private ||
                    _.isEmpty(message.receivers) ||
                    _.includes(message.receivers, receiver) ||
                    message.sender === receiver,
            ),
            resolve: payload => payload,
        },
    },
};
