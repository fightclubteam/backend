'use strict';

import { mergeResolvers } from 'merge-graphql-schemas';

import chatMutationsResolvers from './chat-mutations';
import chatQueriesResolvers from './chat-queries';
import chatSubscriptionsResolvers from './chat-subscriptions';

export default mergeResolvers([chatQueriesResolvers, chatMutationsResolvers, chatSubscriptionsResolvers]);
