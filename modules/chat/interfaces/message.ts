'use strict';

import * as mongoose from 'mongoose';

export interface IMessage extends mongoose.Document {
    id: string;
    sender: string;
    type: string;
    subtype: string;
    receivers?: string;
    text: string;
    location?: string;
    created_at: Date;
    spellcaster: string;
    target: string;
    effect: string;
}

export interface IMessageDocument extends mongoose.Model<IMessage> {}
