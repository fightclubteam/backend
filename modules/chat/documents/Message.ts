'use strict';

import * as _ from 'lodash';
import * as mongoose from 'mongoose';
import { IMessage } from '../interfaces/message';
import { MESSAGE_TYPE } from '../enums/message';

export default mongoose.model<IMessage>(
    'Message',
    new mongoose.Schema(
        {
            sender: {
                type: String,
                require: true,
            },
            receivers: [
                {
                    type: String,
                },
            ],
            text: {
                type: String,
                require: true,
                maxlength: [255, 'The value of `{PATH}` is longer than the maximum allowed length {MAXLENGTH}.'],
                trim: true,
            },
            type: {
                type: String,
                enum: _.values(MESSAGE_TYPE),
                require: true,
            },

            subtype: {
                type: String,
            },
            effect: {
                type: Object,
            },
            target: {
                type: String,
            },
            spellcaster: {
                type: String,
            },
        },
        {
            timestamps: { createdAt: 'created_at', updatedAt: 'updated_at' },
            versionKey: false,
        },
    ),
);
