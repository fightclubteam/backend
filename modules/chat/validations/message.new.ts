import * as Joi from 'joi';
import * as _ from 'lodash';

import { MESSAGE_TYPE } from '../enums/message';

export default Joi.object().keys({
    to_receivers: Joi.array()
        .unique()
        .items(Joi.string().max(20)),
    private_receivers: Joi.array()
        .unique()
        .items(Joi.string().max(20)),
    message: Joi.string()
        .max(255)
        .required(),
});
