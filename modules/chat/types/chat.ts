'use strict';

export default `
    type ChatMessageType {
        _id: String!
        sender: String!
        message: String!
        receivers: [String]!
        is_private: Boolean!
        created_at: String!
    }
    
    type ChatUserType {
        _id: String!
        login: String!
        location: String!
        level: Int!
    }
    
    type Query {
        chatUsers: [ChatUserType]!
    }
    
    type Mutation {
        joinToChat(login: String!, location: String!): ChatUserType!
        addChatMessage(message: String!, receivers: [String], is_private: Boolean): ChatMessageType!
    }
    
    type Subscription {
        chatMessageAdded(receiver: String!): ChatMessageType!
    }
`;
