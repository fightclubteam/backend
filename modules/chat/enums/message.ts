export const MESSAGE_TYPE = Object.freeze({
    GENERAL: 'GENERAL',
    SPECIFIC: 'SPECIFIC',
    PRIVATE: 'PRIVATE',
    SYSTEM: 'SYSTEM',
    SPELL: 'SPELL',
});
