'use strict';

import { ItemError } from './ItemError';

export default class ItemNotFoundError extends ItemError {
    constructor(public message: string, public code?: number | string, public parent?: Error) {
        super(message, code, parent);
    }
}
