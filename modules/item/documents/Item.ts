'use strict';

import * as mongoose from 'mongoose';
import * as _ from 'lodash';

import { IUser } from '../../user/interfaces/user';
import ItemNotFoundError from '../errors/ItemNotFoundError';
import { ItemInterface, ItemTypeEnum } from '../interfaces/item';
import { ITEM_TYPE_ENUM } from '../enums/item';

export const itemSchema = new mongoose.Schema(
    {
        title: {
            type: String,
            required: true,
        },
        description: {
            type: String,
            required: false,
        },
        weight: {
            type: Number,
            required: true,
            min: 0,
        },
        item_type: {
            type: String,
            required: true,
            enum: _.values(ITEM_TYPE_ENUM),
        },
        durability: {
            current: {
                type: Number,
                required: true,
                min: 0,
            },
            max: {
                type: Number,
                required: true,
                min: 0,
            },
        },
        restrictions: {
            min_user_level: {
                type: Number,
                required: true,
                default: 0,
                min: 0,
            },
            min_user_rank: {
                type: Number,
                required: true,
                default: 0,
                min: 0,
            },
            min_strength: {
                type: Number,
                required: true,
                default: 0,
                min: 0,
            },
            min_agility: {
                type: Number,
                required: true,
                default: 0,
                min: 0,
            },
            min_intuition: {
                type: Number,
                required: true,
                default: 0,
                min: 0,
            },
            min_endurance: {
                type: Number,
                required: true,
                default: 0,
                min: 0,
            },
        },
        price: {
            type: Number,
            required: true,
            min: 0,
        },
        mastery: {
            club: {
                type: Number,
                required: true,
                default: 0,
            },
            axe: {
                type: Number,
                required: true,
                default: 0,
            },
            knife: {
                type: Number,
                required: true,
                default: 0,
            },
            sword: {
                type: Number,
                required: true,
                default: 0,
            },
        },
        modifiers: {
            dodge: {
                type: Number,
                required: true,
                default: 0,
            },
            anti_dodge: {
                type: Number,
                required: true,
                default: 0,
            },
            critical: {
                type: Number,
                required: true,
                default: 0,
            },
            anti_critical: {
                type: Number,
                required: true,
                default: 0,
            },
        },
        abilities: {
            strength: {
                type: Number,
                required: true,
                default: 0,
            },
            agility: {
                type: Number,
                required: true,
                default: 0,
            },
            intuition: {
                type: Number,
                required: true,
                default: 0,
            },
            intelligence: {
                type: Number,
                required: true,
                default: 0,
            },
            hp: {
                type: Number,
                required: true,
                default: 0,
            },
            damage_min: {
                type: Number,
                required: true,
                default: 0,
            },
            damage_max: {
                type: Number,
                required: true,
                default: 0,
            },
            power: {
                type: Number,
                required: true,
                default: 0,
            },
        },
        armor: {
            head: {
                type: Number,
                required: true,
                default: 0,
            },
            chest: {
                type: Number,
                required: true,
                default: 0,
            },
            abdomen: {
                type: Number,
                required: true,
                default: 0,
            },
            girdle: {
                type: Number,
                required: true,
                default: 0,
            },
            legs: {
                type: Number,
                required: true,
                default: 0,
            },
        },
        image: {
            type: String,
        },
        can_craft: {
            type: Boolean,
            required: true,
            default: false,
        },
        can_give: {
            type: Boolean,
            required: true,
            default: false,
        },
        can_repair: {
            type: Boolean,
            required: true,
            default: false,
        },
        individual_id: {
            type: Number,
            required: true,
            default: 0,
            unique: true,
        },
        serial_number: {
            type: Number,
            required: true,
            default: 0,
            unique: true,
        },
    },
    {
        timestamps: { createdAt: 'created_at', updatedAt: 'updated_at' },
        toObject: { virtuals: true },
        toJSON: { virtuals: true },
    },
)
    .index({ item_type: 1 })
    .index({ individual_id: 1 })
    .index({ serial_number: 1 });

itemSchema.pre('save', generateIndividualId);
itemSchema.pre('save', generateSerialNumber);

itemSchema.method('canBeBought', function(user: IUser) {
    return (
        this.restrictions.min_user_level <= user.level &&
        this.restrictions.min_agility <= user.abilities.agility &&
        this.restrictions.min_strength <= user.abilities.strength &&
        this.restrictions.min_intuition <= user.abilities.intuition &&
        this.restrictions.min_endurance <= user.abilities.endurance
    );
});

itemSchema.static({
    findAllByType(type: ItemTypeEnum) {
        return ItemModel.find({ item_type: type });
    },
    isItemsConflict(item1: ItemInterface, item2: ItemInterface, dressedOnItemCount: number) {
        return (
            (item1.item_type === item2.item_type && !(item1.item_type === 'RING' && dressedOnItemCount < 3)) || // 3 max rings on user
            (_.includes(['SHIELD', 'KNIFE_LEFT'], item1.item_type) &&
                _.includes(['SHIELD', 'KNIFE_LEFT'], item2.item_type)) ||
            (_.includes(['AXE', 'SWORD', 'MACE', 'KNIFE'], item1.item_type) &&
                _.includes(['AXE', 'SWORD', 'MACE', 'KNIFE'], item2.item_type))
        );
    },
    async findByIdOrFail(itemId: string) {
        if (!itemId) {
            throw new ItemNotFoundError('Item ID is required');
        }

        const item = await ItemModel.findById(itemId);

        if (!item) {
            throw new ItemNotFoundError('Item with such ID is not found');
        }

        return item;
    },
});

async function generateIndividualId(next) {
    if (this.isNew) {
        let doc;
        let id;
        do {
            id = _.random(100000, 999999);
            doc = await ItemModel.findOne({
                individual_id: id,
            });
        } while (doc !== null);
        this.individual_id = id;
    }
    return next();
}

async function generateSerialNumber(next) {
    if (this.isNew) {
        let maxId;
        const maxIdAggregation = (await ItemModel.aggregate({
            $group: {
                _id: '',
                maxId: {
                    $max: '$serial_number',
                },
            },
        })) as any[];
        maxId = maxIdAggregation.length > 0 ? maxIdAggregation[0].maxId || 0 : 0;
        this.serial_number = maxId + 1;
    }
    return next();
}

const ItemModel = mongoose.model<ItemInterface>('Item', itemSchema);
export default ItemModel;
