'use strict';

export default `
    enum ItemTypeEnum {
        HELMET
        BRACELET
        AXE
        SWORD
        KNIFE
        MACE
        SHIRT
        ARMOR
        CLOAK
        BELT
        EARRINGS
        AMULET
        RING
        GLOVES
        SHIELD
        KNIFE_LEFT
        LEGGINGS
        BOOTS
    }
    
    enum ItemAbilityEnum {
        STRENGTH
        AGILITY
        INTUITION
        MODIFIER_CRITICAL
        MODIFIER_ANTI_CRITICAL
        MODIFIER_DODGE
        MODIFIER_ANTI_DODGE
        ARMOR
        HP
        DAMAGE
        POWER
    }
    
    type ItemTypeType {
        item_type: ItemTypeEnum!
        title: String!
    }
    
    type ItemDurabilityType {
        current: Int!
        max: Int!
    }
    
    type ItemArmorType {
        head: Int!
        chest: Int!
        abdomen: Int!
        girdle: Int!
        legs: Int!
    }
    
    input ItemArmorInputType {
        head: Int!
        chest: Int!
        abdomen: Int!
        girdle: Int!
        legs: Int!
    }
    
    type ItemRestrictionsType {
        min_user_level: Int!
        min_user_rank: Int!
        min_strength: Int!
        min_agility: Int!
        min_intuition: Int!
        min_endurance: Int!
    }
    
    type ItemsSetType {
        _id: ID!
        name: String!
        is_dressed_on: Boolean!
        items: [UserItemType]
    }
    
    input ItemDurabilityInputType {
        current: Int!
        max: Int!
    }
    
    input ItemRestrictionsInputType {
        min_user_level: Int
        min_user_rank: Int
        min_strength: Int
        min_agility: Int
        min_intuition: Int
        min_endurance: Int
    }
    
    type ItemType implements Node {
        _id: ID!
        item_type: ItemTypeEnum!
        title: String!
        description: String
        weight: Float!
        durability: ItemDurabilityType!
        restrictions: ItemRestrictionsType!
        abilities: ItemAbilityType!
        mastery: UserMastery!
        price: Float!
        image: String!
        can_craft: Boolean!
        can_repair: Boolean!
        can_give: Boolean!
        serial_number: Int
        individual_id: Int
        armor: ItemArmorType
    }
    
    type ItemAbilityType {
        strength: Int!
        agility: Int!
        intuition: Int!
        intelligence: Int!
        hp: Int!
        damage_min: Int!
        damage_max: Int!
    }
    
    input ItemAbilityInput {
        strength: Int!
        agility: Int!
        intuition: Int!
        intelligence: Int!
        hp: Int!
        damage_min: Int!
        damage_max: Int!
    }
    
    input ItemInputType {
        item_type: ItemTypeEnum!
        title: String!
        description: String
        weight: Int!
        durability: ItemDurabilityInputType!
        restrictions: ItemRestrictionsInputType!
        price: Float!
        abilities: ItemAbilityInput!
        mastery: UserMasteryInput!
        modifiers: UserModifierInput!
        image: String!
        can_craft: Boolean!
        can_repair: Boolean!
        can_give: Boolean!
        armor: ItemArmorInputType!
    }
    
    type PaginatedItemType {
        data: [ItemType]!
        pagination: PaginationType!
    }
    
    type PaginatedStoreItemType {
        data: [StoreItemType]!
        pagination: PaginationType!
    }
    
    type PaginatedUserItemType {
        data: [UserItemType]!
        pagination: PaginationType!
    }
    
    input ItemFilterType {
        min_user_rank: Int
        min_user_level: Int
        types: [ItemTypeEnum]
    }
    
    type Query {
        items(limit: Int = 10, page: Int = 1, filters: ItemFilterType): PaginatedItemType!
        item(item_id: String!): ItemType!
        itemTypes: [ItemTypeType]!
    }
    
    type Mutation {
        createItem(item: ItemInputType!): ItemType!
        removeItem(item_id: String!): Boolean
    }
`;
