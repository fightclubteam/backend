'use strict';

import * as mongoose from 'mongoose';

import { IFindableByIdOrFail } from '../../../_shared/common.interfaces';
import { IUserAbility, IUser } from '../../user/interfaces/user';

export type ItemTypeEnum =
    | 'HELMET'
    | 'BRACELET'
    | 'AXE'
    | 'SWORD'
    | 'KNIFE'
    | 'MACE'
    | 'SHIRT'
    | 'ARMOR'
    | 'CLOAK'
    | 'BELT'
    | 'KNIFE_LEFT'
    | 'SHIELD'
    | 'EARRINGS'
    | 'AMULET'
    | 'RING'
    | 'GLOVES'
    | 'LEGGINGS'
    | 'BOOTS';

export interface IItemDurability {
    current: number;
    max: number;
}

export interface IItemArmor {
    head: number;
    chest: number;
    abdomen: number;
    girdle: number;
    legs: number;
}

export interface IItemRestrictions {
    min_user_level?: number;
    min_user_rank?: number;
}

export interface IItemsCollectionAware<TInstance, TItemInstance> {
    addItem(item: ItemInterface): Promise<TInstance>;
    removeItem(item: TItemInstance): Promise<TInstance>;
    getItems(filter?: object): Promise<TItemInstance[]>;
    getItemsCount(filter: object): Promise<number>;
}

export interface ItemInterface extends mongoose.Document {
    title: string;
    description: string;
    weight: number;
    durability: IItemDurability;
    restrictions: IItemRestrictions;
    price: number;
    abilities: IUserAbility;
    image: string;
    can_craft: boolean;
    can_give: boolean;
    can_repair: boolean;
    item_type: ItemTypeEnum;
    individual_id: number;
    serial_number: number;
    armor: IItemArmor;

    canBeBought(user: IUser): boolean;
}

export interface IItemFilter {
    min_user_rank?: number;
    min_user_level?: number;
    types?: ItemTypeEnum[];
}

export interface IItemsQuery {
    limit?: number;
    page?: number;
    filters?: IItemFilter;
}

export interface IItemSearchableByType<TInstance> {
    findAllByType(type: string): Promise<TInstance | null>;
}

export interface IItemDocument
    extends mongoose.Model<ItemInterface>,
        IItemSearchableByType<ItemInterface>,
        IFindableByIdOrFail<ItemInterface> {
    isItemsConflict(item1: ItemInterface, item2: ItemInterface, dressedOnItemCount: number): boolean;
}
