'use strict';

import { promisify } from 'util';
import * as config from 'config';
import * as fs from 'fs';

import { documents } from '../../../_shared/documents';
import { uploadSharedFile } from '../../../_shared/helpers/files/s3';

import ItemFileUploadError from '../errors/ItemFileUploadError';

export default {
    Mutation: {
        async createItem(rootContext, { item }) {
            item = new documents.Item(item);

            if (item.image) {
                const filePath = `${config.get('temp_upload_path')}/${item.image}`;
                const pathname = `items/${item.item_type.toLowerCase()}/${item.image}`;
                try {
                    await uploadSharedFile(pathname, filePath);
                    item.image = pathname;
                    await promisify(fs.unlink)(filePath);
                } catch (e) {
                    throw new ItemFileUploadError('Error during file upload. Please try another one');
                }
            }

            return item.save();
        },
        removeItem(rootContext, { item_id }) {
            return documents.Item.findByIdAndRemove(item_id);
        },
    },
};
