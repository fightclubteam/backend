'use strict';

import { mergeResolvers } from 'merge-graphql-schemas';

import itemMutationsResolvers from './item-mutations';
import itemQueriesResolvers from './item-queries';
import itemSubscriptionsResolvers from './item-subscriptions';

export default mergeResolvers([itemQueriesResolvers, itemMutationsResolvers, itemSubscriptionsResolvers]);
