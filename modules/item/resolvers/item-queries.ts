'use strict';

import * as config from 'config';

import { documents } from '../../../_shared/documents';
import { paginateModel } from '../../../_shared/helpers/pagination/pagination';

import { IItemsQuery } from '../interfaces/item';

const defaultItemsFilter = {
    min_user_level: 0,
    min_user_rank: 0,
    types: [],
};

export default {
    Query: {
        items(rootContext, { limit = 10, page = 1, filters = defaultItemsFilter }: IItemsQuery) {
            const { min_user_level = 0, min_user_rank = 0, types = [] } = filters;
            const criteria = {};

            if (min_user_level > 0) {
                criteria['restrictions.min_user_level'] = {
                    $gt: min_user_level,
                };
            }

            if (min_user_rank > 0) {
                criteria['restrictions.min_user_rank'] = {
                    $gt: min_user_rank,
                };
            }

            if (types.length > 0) {
                const itemTypeKey = 'item_type';
                criteria[itemTypeKey] = { $in: types };
            }

            return paginateModel(documents.Item, page, limit, {
                populate: '',
                ...criteria,
            });
        },
        item(rootContext, { item_id }) {
            return documents.Item.findById(item_id).exec();
        },
        itemTypes() {
            return [
                {
                    item_type: 'HELMET',
                    title: 'Шлем',
                },
                {
                    item_type: 'BRACELET',
                    title: 'Наручи',
                },
                {
                    item_type: 'AXE',
                    title: 'Топор',
                },
                {
                    item_type: 'SWORD',
                    title: 'Меч',
                },
                {
                    item_type: 'KNIFE',
                    title: 'Ножи/Кинжалы',
                },
                {
                    item_type: 'MACE',
                    title: 'Дубина/Булава',
                },
                {
                    item_type: 'SHIRT',
                    title: 'Рубаха',
                },
                {
                    item_type: 'ARMOR',
                    title: 'Броня',
                },
                {
                    item_type: 'CLOAK',
                    title: 'Плащ',
                },
                {
                    item_type: 'BELT',
                    title: 'Пояс',
                },
                {
                    item_type: 'EARRINGS',
                    title: 'Серьги',
                },
                {
                    item_type: 'AMULET',
                    title: 'Амулет',
                },
                {
                    item_type: 'RING',
                    title: 'Кольцо',
                },
                {
                    item_type: 'GLOVES',
                    title: 'Перчатки',
                },
                {
                    item_type: 'SHIELD',
                    title: 'Щит',
                },
                {
                    item_type: 'KNIFE_LEFT',
                    title: 'Кинжал (левая рука)',
                },
                {
                    item_type: 'LEGGINGS',
                    title: 'Поножи',
                },
                {
                    item_type: 'BOOTS',
                    title: 'Ботинки',
                },
            ];
        },
    },
    ItemType: {
        image(item) {
            let image = item.image || '';
            if (!image.match(/^https?:\/\//)) {
                image = `https://${config.get('cdn_hostname')}/${item.image}`;
            }
            return image;
        },
    },
    ItemsSetType: {
        items(set) {
            return documents.UserItem.find({
                _id: {
                    $in: set.items,
                },
            });
        },
    },
};
