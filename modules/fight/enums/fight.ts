import { ITEM_TYPE_ENUM } from '../../item/enums/item';

export const FIGHT_TYPE = Object.freeze({
    PHYSICAL: 'PHYSICAL',
    GROUP: 'GROUP',
    CHAOTIC: 'CHAOTIC',
});

export const FIGHT_TYPE_FULL = Object.freeze({
    CURRENT: 'CURRENT',
    FINISHED: 'FINISHED',
    PHYSICAL: 'PHYSICAL',
    GROUP: 'GROUP',
    CHAOTIC: 'CHAOTIC',
});

export const FIGHT_PARTICIPANT_TEAM = Object.freeze({
    LEFT: 'LEFT',
    RIGHT: 'RIGHT',
});

export const BATTLE_TYPE = Object.freeze({
    FISTFUL: 'FISTFUL',
    WEAPON: 'WEAPON',
});

export const BATTLE_KIND = Object.freeze({
    WITH_SCUMBAG: 'WITH_SCUMBAG',
    NO_HOLDS_BARRED: 'NO_HOLDS_BARRED',
    DEADLY_WOUNDS: 'DEADLY_WOUNDS',
});

export const FIGHT_STATUS = Object.freeze({
    COMPLETED: 'COMPLETED',
    IN_PROGRESS: 'IN_PROGRESS',
    IN_BATTLE: 'IN_BATTLE',
});

export const FIGHT_TEAM_WIN = Object.freeze({
    LEFT: 'LEFT',
    RIGHT: 'RIGHT',
    DRAW: 'DRAW',
});

export const FIGHT_ACTION_BODY_PART = Object.freeze({
    HEAD: 'HEAD',
    CHEST: 'CHEST',
    ABDOMEN: 'ABDOMEN',
    GIRDLE: 'GIRDLE',
    LEGS: 'LEGS',
});

export const FIGHT_ROUND_STATUS = Object.freeze({
    COMPLETED: 'COMPLETED',
    EXPIRED: 'EXPIRED',
    IN_PROGRESS: 'IN_PROGRESS',
});

export const PARTICIPANT_TYPE = Object.freeze({
    ANY_LEVEL: 'ANY_LEVEL',
    MY_AND_LOWER_LEVEL: 'MY_AND_LOWER_LEVEL',
    LOWER_LEVEL: 'LOWER_LEVEL',
    MY_LEVEL: 'MY_LEVEL',
    MY_LEVEL_PLUS_ONE_MAX: 'MY_LEVEL_PLUS_ONE_MAX',
    MY_LEVEL_MINUS_ONE_MIN: 'MY_LEVEL_MINUS_ONE_MIN',
    MY_LEVEL_PLUS_MINUS_ONE: 'MY_LEVEL_PLUS_MINUS_ONE',
    CLAN_ONLY: 'CLAN_ONLY',
});

export const FIGHT_PARTICIPANT_STATUS = Object.freeze({
    WAITING_FOR_APPROVE: 'WAITING_FOR_APPROVE',
    APPROVED: 'APPROVED',
});

export const FIGHT_REJECTED_REASON = Object.freeze({
    NOT_ENOUGH_PARTICIPANTS: 'NOT_ENOUGH_PARTICIPANTS',
});
