'use strict';

import { ApplicationError } from '../../../errors';

export class FightError extends ApplicationError {
    constructor(public message: string, public code?: number | string, public parent?: Error) {
        super(message, code, parent);
    }
}

export class FightNotFoundError extends FightError {
    constructor(public message: string, public code?: number | string, public parent?: Error) {
        super(message, code, parent);
    }
}

export class FightStatusError extends FightError {
    constructor(public message: string, public code?: number | string, public parent?: Error) {
        super(message, code, parent);
    }
}

export class FightTypeError extends FightError {
    constructor(public message: string, public code?: number | string, public parent?: Error) {
        super(message, code, parent);
    }
}

export class FightUserIsBusyError extends FightError {
    constructor(public message: string, public code?: number | string, public parent?: Error) {
        super(message, code, parent);
    }
}

export class FightMaxRequestError extends FightError {
    constructor(public message: string, public code?: number | string, public parent?: Error) {
        super(message, code, parent);
    }
}

export class FightUserDiffLocationError extends FightError {
    constructor(public message: string, public code?: number | string, public parent?: Error) {
        super(message, code, parent);
    }
}

export class FightEmptyError extends FightError {
    constructor(public message: string, public code?: number | string, public parent?: Error) {
        super(message, code, parent);
    }
}

export class FightTeamLevelError extends FightError {
    constructor(public message: string, public code?: number | string, public parent?: Error) {
        super(message, code, parent);
    }
}

export class FightUserNotExistError extends FightError {
    constructor(public message: string, public code?: number | string, public parent?: Error) {
        super(message, code, parent);
    }
}
