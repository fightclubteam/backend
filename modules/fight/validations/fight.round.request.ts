import * as Joi from 'joi';
import * as _ from 'lodash';
import { FIGHT_ACTION_BODY_PART } from '../enums/fight';

export default (attackCount: number = 1, blockCount: number = 2) => {
    const bodyPartJoiSchema = Joi.string().valid(_.values(FIGHT_ACTION_BODY_PART));
    let blockSchema;
    if (blockCount === 1) {
        blockSchema = Joi.array()
            .length(1)
            .items(bodyPartJoiSchema)
            .required();
    } else {
        blockSchema = Joi.alternatives(generateFightActionBlockJoiSchema(blockCount));
    }

    return Joi.object().keys({
        login: Joi.string()
            .trim()
            .required(),
        attack: Joi.array()
            .length(attackCount)
            .items(bodyPartJoiSchema)
            .required(),
        block: blockSchema,
    });
};

function generateFightActionBlockJoiSchema(count: number) {
    if (count < 2) {
        throw new Error('Invalid block count items for generating joi schema.');
    }
    const values = _.values(FIGHT_ACTION_BODY_PART);
    const valueLength = values.length;

    if (count >= valueLength) {
        throw new Error(
            'Invalid block count items for generating joi schema, count items should be less that values length.',
        );
    }

    const schemas = [];

    for (let i = 0; i < valueLength; i++) {
        const items = [];
        for (let c = 0; c < count; c++) {
            items.push(Joi.string().valid(FIGHT_ACTION_BODY_PART[values[i + c] || values[i + c - valueLength]]));
        }
        schemas.push(
            Joi.array()
                .items(items)
                .length(count)
                .required(),
        );
    }

    return schemas;
}
