import * as Joi from 'joi';
import * as _ from 'lodash';

import { FIGHT_TYPE, BATTLE_TYPE, BATTLE_KIND, PARTICIPANT_TYPE } from '../enums/fight';

export default Joi.object().keys({
    fight_type: Joi.string()
        .valid(_.values(FIGHT_TYPE))
        .required(),
    battle_type: Joi.string()
        .valid(_.values(BATTLE_TYPE))
        .required(),
    battle_kind: Joi.string().valid(_.values(BATTLE_KIND)),
    right_count: Joi.number()
        .integer()
        .min(1)
        .when('fight_type', {
            is: FIGHT_TYPE.PHYSICAL,
            then: Joi.any()
                .default(1)
                .forbidden(),
            otherwise: Joi.required(),
        }),
    right_type: Joi.when('fight_type', {
        is: FIGHT_TYPE.PHYSICAL,
        then: Joi.string()
            .default(PARTICIPANT_TYPE.MY_LEVEL)
            .forbidden(),
        otherwise: Joi.valid(_.values(PARTICIPANT_TYPE)).required(),
    }),
    left_count: Joi.number()
        .integer()
        .min(1)
        .when('fight_type', {
            is: FIGHT_TYPE.PHYSICAL,
            then: Joi.any()
                .forbidden()
                .default(1),
            otherwise: Joi.required(),
        }),
    left_type: Joi.string().when('fight_type', {
        is: FIGHT_TYPE.PHYSICAL,
        then: Joi.string()
            .default(PARTICIPANT_TYPE.ANY_LEVEL)
            .forbidden(),
        otherwise: Joi.valid(_.values(PARTICIPANT_TYPE)).required(),
    }),
    timeout: Joi.number()
        .integer()
        .min(1)
        .max(5)
        .required(),
    comment: Joi.string().max(255),
    begins_at: Joi.number()
        .integer()
        .min(1)
        .when('fight_type', {
            is: FIGHT_TYPE.PHYSICAL,
            then: Joi.number()
                .integer()
                .default(60)
                .forbidden(),
            otherwise: Joi.required(),
        }),
});
