'use strict';

import * as _ from 'lodash';

export function generateEnumType(constMap) {
    return _.reduce(constMap, (result, value, key) => _.assign(result, { [key]: { value } }), {}) as any;
}
