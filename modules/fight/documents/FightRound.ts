'use strict';

import * as _ from 'lodash';
import * as mongoose from 'mongoose';
import { BATTLE_TYPE, FIGHT_ACTION_BODY_PART, FIGHT_ROUND_STATUS } from '../enums/fight';
import { FIGHT_ACTION_TYPE } from '../enums/fight.action';
import { IFight, FightType } from '../interfaces/fight';
import { IFightRound } from '../interfaces/fight.round';
import { documents } from '../../../_shared/documents';

const MEMBER_SCHEMA = {
    login: {
        type: String,
        require: true,
        trim: true,
    },
    attack: [
        {
            type: String,
            enum: _.values(FIGHT_ACTION_BODY_PART),
        },
    ],
    block: [
        {
            type: String,
            enum: _.values(FIGHT_ACTION_BODY_PART),
        },
    ],
    is_dodge: {
        type: Boolean,
        required: true,
        default: false,
    },
    is_critical: {
        type: Boolean,
        required: true,
        default: false,
    },
    inflicted_damage: {
        type: Number,
        require: true,
        min: 0,
        default: 0,
    },
    taken_damage: {
        type: Number,
        require: true,
        min: 0,
        default: 0,
    },
    hp_current: {
        type: Number,
        min: 0,
    },
    hp_max: {
        type: Number,
        min: 0,
    },
};

const fightRoundSchema = new mongoose.Schema(
    {
        fight: {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'Fight',
            require: true,
        },
        battle_type: {
            type: String,
            enum: _.values(BATTLE_TYPE),
            require: true,
        },
        status: {
            type: String,
            require: true,
            enum: _.values(FIGHT_ROUND_STATUS),
            default: FIGHT_ROUND_STATUS.IN_PROGRESS,
        },
        expires_at: {
            type: Date,
            require: true,
        },
        member_left: MEMBER_SCHEMA,
        member_right: MEMBER_SCHEMA,
    },
    {
        timestamps: { createdAt: 'created_at', updatedAt: 'updated_at' },
        collection: 'fight_rounds',
    },
)
    .index({ fight: 1, 'member_right.login': 1, 'member_left.login': 1 })
    .index({ status: 1 })
    .index({ expires_at: 1 })
    .index({ updated_at: -1 });

fightRoundSchema.method('inProgress', function() {
    return this.status !== FIGHT_ROUND_STATUS.IN_PROGRESS;
});

fightRoundSchema.method('markAsExpired', function() {
    this.status = FIGHT_ROUND_STATUS.EXPIRED;
    return this;
});

fightRoundSchema.static('createTeamsEmptyRounds', function(fight: IFight) {
    const firstRounds = [];
    _.each(fight.team.right, userRight => {
        _.each(fight.team.left, userLeft => {
            firstRounds.push({
                fight,
                battle_type: fight.battle_type,
                member_left: {
                    login: userLeft.login,
                    block: [],
                    attack: [],
                },
                member_right: {
                    login: userRight.login,
                    block: [],
                    attack: [],
                },
            });
        });
    });

    return documents.FightRound.insertMany(_.uniqWith(firstRounds, _.isEqual));
});

// fightRoundSchema.pre('update', addLogs);
fightRoundSchema.post('save', addLogs);

async function addLogs(doc) {
    if (this.isNew) {
        return;
    }

    if (
        _.isEmpty(doc.member_left.attack) ||
        _.isEmpty(doc.member_left.block) ||
        _.isEmpty(doc.member_right.attack) ||
        _.isEmpty(doc.member_right.block)
    ) {
        return;
    }

    const logs = await Promise.all([
        getLog(doc.battle_type, doc.member_left, doc.member_right),
        getLog(doc.battle_type, doc.member_right, doc.member_left),
    ]);
    const logsBlows = _.flatten(logs[0].logs_blows.concat(logs[1].logs_blows));
    const logsDead = _.flatten(logs[0].logs_dead.concat(logs[1].logs_dead));

    const promises = [
        documents.FightLog.create({
            fight: this.fight,
            logs: logsBlows,
        }),
    ];

    if (logsDead.length > 0) {
        promises.push(
            documents.FightLog.create({
                fight: this.fight,
                logs: logsDead,
            }),
        );
    }

    await Promise.all(promises);
}

async function getLog(battleType: FightType, attacker, defender) {
    const logs = [];
    for (let i = 0; i < attacker.attack.length; i++) {
        const bodyPart = attacker.attack[i];
        let action = FIGHT_ACTION_TYPE.ATTACK,
            inflictedDamage = attacker.inflicted_damage;

        if (defender.is_dodge) {
            action = FIGHT_ACTION_TYPE.DODGE;
            inflictedDamage = null;
        } else if (_.includes(defender.block, bodyPart)) {
            if (attacker.is_critical) {
                action = FIGHT_ACTION_TYPE.BREAK_THROUGH_BLOCK;
            } else {
                action = FIGHT_ACTION_TYPE.BLOCK;
                inflictedDamage = null;
            }
        } else if (attacker.is_critical) {
            action = FIGHT_ACTION_TYPE.CRITICAL;
        }

        const message = await documents.FightActionMessage.findRandomMessage(
            action as any,
            battleType as any,
            bodyPart,
        );
        logs.push({
            log: message.message,
            action,
            attacker: attacker.login,
            defender: defender.login,
            inflicted_damage: inflictedDamage,
            hp: {
                current: defender.hp_current,
                max: defender.hp_max,
            },
            body_part: {
                attack: attacker.attack[i],
                block: defender.block,
            },
        });
    }

    const logsDead = [];
    if (defender.hp_current <= 0) {
        const message = await documents.FightActionMessage.findRandomMessage(FIGHT_ACTION_TYPE.DEAD as any);

        logsDead.push({
            action: FIGHT_ACTION_TYPE.DEAD,
            log: message.message,
            login: defender.login,
        });
    }

    return {
        logs_blows: logs,
        logs_dead: logsDead,
    };
}

fightRoundSchema.static('findInProgressBlows', function(dateTo?: Date) {
    const params: any = {
        status: FIGHT_ROUND_STATUS.IN_PROGRESS,
    };

    if (dateTo) {
        params.expires_at = { $lte: dateTo };
    }
    return FightRoundModel.find(params);
});

const FightRoundModel = mongoose.model<IFightRound>('FightRound', fightRoundSchema);

export default FightRoundModel;
