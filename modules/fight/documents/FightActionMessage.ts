'use strict';

import * as mongoose from 'mongoose';
import * as _ from 'lodash';

import { IFightActionMessage } from '../interfaces/fight.action.message';
import { BattleType, FightActionBodyPartType } from '../interfaces/fight';
import { BATTLE_TYPE, FIGHT_ACTION_BODY_PART } from '../enums/fight';
import { FIGHT_ACTION_TYPE } from '../enums/fight.action';

const fightActionMessageSchema = new mongoose.Schema(
    {
        battle_type: {
            type: String,
            enum: _.values(BATTLE_TYPE),
            require: true,
        },
        body_part: {
            type: String,
            enum: _.values(FIGHT_ACTION_BODY_PART),
            require: true,
        },
        action: {
            type: String,
            enum: _.values(FIGHT_ACTION_TYPE),
            require: true,
        },
        message: {
            type: String,
            required: true,
        },
    },
    {
        timestamps: false,
        versionKey: false,
        collection: 'fight_action_messages',
    },
).index({ battle_type: 1, body_part: 1, action: 1 });

fightActionMessageSchema.static('findRandomMessage', async function(
    action: FightActionBodyPartType,
    battleType?: BattleType,
    bodyPart?: FightActionBodyPartType,
) {
    const params: any = { action };
    if (battleType) {
        params.battle_type = battleType;
    }

    if (bodyPart) {
        params.body_part = bodyPart;
    }

    const messages = await FightActionMessageModel.aggregate([{ $match: params }, { $sample: { size: 1 } }]);

    return _.first(messages);
});

const FightActionMessageModel = mongoose.model<IFightActionMessage>('FightActionMessage', fightActionMessageSchema);
export default FightActionMessageModel;
