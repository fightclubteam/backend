'use strict';

import * as _ from 'lodash';
import * as mongoose from 'mongoose';
import * as moment from 'moment';

import { IFight, IFightTeam, FightRoomType, IFightTypeCount } from '../interfaces/fight';
import {
    PARTICIPANT_TYPE,
    FIGHT_PARTICIPANT_STATUS,
    FIGHT_STATUS,
    FIGHT_ROUND_STATUS,
    FIGHT_TYPE_FULL,
    FIGHT_TYPE,
    BATTLE_TYPE,
    FIGHT_TEAM_WIN,
    BATTLE_KIND,
} from '../enums/fight';
import { documents } from '../../../_shared/documents';
import { IUser } from '../../user/interfaces/user';
import { ILocation } from '../../location/interfaces/location';

const MAX_TACTICS_COUNT = 25;

const TEAM_SCHEMA = [
    {
        login: {
            type: String,
            require: true,
            trim: true,
        },
        level: {
            type: Number,
            require: true,
        },
        status: {
            type: String,
            enum: _.values(FIGHT_PARTICIPANT_STATUS),
            require: true,
        },
        tactics: {
            hitting: {
                type: Number,
                require: true,
                min: 0,
                default: 0,
                set(value) {
                    return value > MAX_TACTICS_COUNT ? MAX_TACTICS_COUNT : value;
                },
            },
            critical: {
                type: Number,
                require: true,
                min: 0,
                default: 0,
                set(value) {
                    return value > MAX_TACTICS_COUNT ? MAX_TACTICS_COUNT : value;
                },
            },
            parry: {
                type: Number,
                require: true,
                min: 0,
                default: 0,
                set(value) {
                    return value > MAX_TACTICS_COUNT ? MAX_TACTICS_COUNT : value;
                },
            },
            block: {
                type: Number,
                require: true,
                min: 0,
                default: 0,
                set(value) {
                    return value > MAX_TACTICS_COUNT ? MAX_TACTICS_COUNT : value;
                },
            },
            inflicted_damage: {
                type: Number,
                require: true,
                min: 0,
                default: 0,
                set(value) {
                    return value > MAX_TACTICS_COUNT ? MAX_TACTICS_COUNT : value;
                },
            },
            spirituality_sincerity: {
                type: Number,
                require: true,
                min: 0,
                default: 0,
                set(value) {
                    return value > MAX_TACTICS_COUNT ? MAX_TACTICS_COUNT : value;
                },
            },
            counterblow: {
                type: Number,
                require: true,
                min: 0,
                default: 0,
                set(value) {
                    return value > MAX_TACTICS_COUNT ? MAX_TACTICS_COUNT : value;
                },
            },
        },
        hp_max: {
            type: Number,
            require: true,
            min: 0,
        },
        hp_current: {
            type: Number,
            min: 0,
            set(value) {
                return value < 0 ? 0 : value;
            },
        },
        mall_type: {
            type: Number,
            min: 0,
        },
        modifier_dodge: {
            type: Number,
            min: 0,
        },
        modifier_anti_dodge: {
            type: Number,
            min: 0,
        },
        modifier_critical: {
            type: Number,
            min: 0,
        },
        modifier_anti_critical: {
            type: Number,
            min: 0,
        },
        created_at: {
            type: Date,
            default: Date.now,
        },
    },
];

const fightSchema = new mongoose.Schema(
    {
        created_by: {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'User',
            require: true,
        },
        location: {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'Location',
            require: true,
        },
        fight_type: {
            type: String,
            enum: _.values(FIGHT_TYPE),
            require: true,
        },
        battle_kind: {
            type: String,
            enum: _.values(BATTLE_KIND),
            require: true,
        },
        battle_type: {
            type: String,
            enum: _.values(BATTLE_TYPE),
            require: true,
        },
        right_count: {
            type: Number,
            require: true,
            min: 1,
        },
        right_type: {
            type: String,
            enum: _.values(PARTICIPANT_TYPE),
            require: true,
        },
        left_count: {
            type: Number,
            require: true,
            min: 1,
        },
        left_type: {
            type: String,
            enum: _.values(PARTICIPANT_TYPE),
            require: true,
        },
        timeout: {
            type: Number,
            min: 1,
            max: 5,
            require: true,
        },
        comment: {
            type: String,
            maxlength: [255, 'The value of `{PATH}` is longer than the maximum allowed length {MAXLENGTH}.'],
        },
        status: {
            type: String,
            enum: _.values(FIGHT_STATUS),
            default: FIGHT_STATUS.IN_PROGRESS,
        },
        team: {
            left: TEAM_SCHEMA,
            right: TEAM_SCHEMA,
        },
        team_winner: {
            type: String,
            enum: _.values(FIGHT_TEAM_WIN),
        },
        begins_in: {
            type: Date,
            require: true,
        },
        finished_at: {
            type: Date,
        },
    },
    {
        collection: 'fights',
        timestamps: { createdAt: 'created_at', updatedAt: 'updated_at' },
    },
)
    .index({ created_by: 1 })
    .index({ begins_in: 1 })
    .index({ status: 1 })
    .index({ finished_at: 1 })
    .index({ location: 1 });

fightSchema.method('isTeamLevelValid', function(compareLevel: number, team: 'LEFT' | 'RIGHT'): boolean {
    const fightCreator = this.getCreator();
    if (!fightCreator) {
        return false;
    }
    const baseLevel = fightCreator.level;
    const value: string = this[team.toLowerCase() + '_type'];
    return (
        (PARTICIPANT_TYPE.MY_LEVEL === value && baseLevel === compareLevel) ||
        PARTICIPANT_TYPE.ANY_LEVEL === value ||
        (PARTICIPANT_TYPE.MY_AND_LOWER_LEVEL === value && baseLevel >= compareLevel) ||
        (PARTICIPANT_TYPE.LOWER_LEVEL === value && baseLevel > compareLevel) ||
        (PARTICIPANT_TYPE.MY_LEVEL_PLUS_ONE_MAX === value && baseLevel + 1 >= compareLevel) ||
        (PARTICIPANT_TYPE.MY_LEVEL_MINUS_ONE_MIN === value && baseLevel - 1 <= compareLevel) ||
        (PARTICIPANT_TYPE.MY_LEVEL_PLUS_MINUS_ONE === value && Math.abs(baseLevel - compareLevel) <= 1)
        /*todo: add clan only logic || (PARTICIPANT_TYPE.CLAN_ONLY === value)*/
    );
});

fightSchema.method('isTeamMemberExists', function(login: string): boolean {
    return !!this.getTeamUserByLogin(login);
});

fightSchema.method('getTeamUserByLogin', function(login: string) {
    return _.find(this.team.right, { login }) || _.find(this.team.left, { login });
});

fightSchema.method('getCreator', function(): IFightTeam | undefined {
    // todo: populate by created_by
    return _.find(this.team.left as IFightTeam[], { status: FIGHT_PARTICIPANT_STATUS.APPROVED as any });
});

fightSchema.method('getWaitingForApproveTeamMember', function(login: string): IFightTeam | undefined {
    return _.find(this.team.right as IFightTeam[], {
        login,
        status: FIGHT_PARTICIPANT_STATUS.WAITING_FOR_APPROVE as any,
    });
});

fightSchema.method('getWaitingForApproveTeamMembersCount', function(): number {
    return _.filter(this.team.right as IFightTeam[], {
        status: FIGHT_PARTICIPANT_STATUS.WAITING_FOR_APPROVE as any,
    }).length;
});

fightSchema.method('canJoinToWaitingForApproveTeam', function(): boolean {
    return this.getWaitingForApproveTeamMembersCount() < 1;
});

fightSchema.method('isPhysical', function(): boolean {
    return this.fight_type === FIGHT_TYPE.PHYSICAL;
});

fightSchema.method('isChaotic', function(): boolean {
    return this.fight_type === FIGHT_TYPE.CHAOTIC;
});

fightSchema.method('isGroup', function(): boolean {
    return this.fight_type === FIGHT_TYPE.GROUP;
});

fightSchema.method('inProgress', function(): boolean {
    return this.status === FIGHT_STATUS.IN_PROGRESS;
});

fightSchema.method('inBattle', function(): boolean {
    return this.status === FIGHT_STATUS.IN_BATTLE;
});

fightSchema.method('isCreator', function(creator: IUser): boolean {
    return String(this.created_by.login ? this.created_by.id : this.created_by) === String(creator.id);
});

fightSchema.method('isSameLocation', function(location: ILocation): boolean {
    return String(this.location.name ? this.location.id : this.location) === String(location.id);
});

fightSchema.method('isFightEmpty', function(): boolean {
    return this.team.left.length <= 1 && this.team.right.length < 1;
});

fightSchema.method('getTeamMembersCount', function(): number {
    return this.team.right.length + this.team.left.length;
});

fightSchema.method('makeFightInBattle', function(): Promise<IFight> {
    this.status = FIGHT_STATUS.IN_BATTLE;
    return this.save();
});

fightSchema.method('canStartFight', function(): boolean {
    return this.getTeamRightLoginList().length > 0;
});

fightSchema.method('getTeamRightLoginList', function(): string[] {
    return _.map(_.filter(this.team.right, { status: FIGHT_PARTICIPANT_STATUS.APPROVED }), 'login') as any;
});

fightSchema.method('getTeamLeftLoginList', function(): string[] {
    return _.map(_.filter(this.team.left, { status: FIGHT_PARTICIPANT_STATUS.APPROVED }), 'login') as any;
});

fightSchema.method('getAllTeamLoginList', function(): string[] {
    return _.uniq(this.getTeamRightLoginList().concat(this.getTeamLeftLoginList())) as any;
});

fightSchema.method('kickTeamMember', function(login: string): Promise<IFight> {
    this.team.right = _.reject(this.team.right, { login });
    this.team.left = _.reject(this.team.left, { login });

    return this.save();
});

fightSchema.method('markAdversaryAsApproved', function(login: string): IFight {
    const teamMember = this.getWaitingForApproveTeamMember(login);
    if (teamMember) {
        this.team.right = _.reject(this.team.right, { status: FIGHT_PARTICIPANT_STATUS.WAITING_FOR_APPROVE });
        teamMember.status = FIGHT_PARTICIPANT_STATUS.APPROVED;
        this.team.right.push(teamMember);
    }
    return this;
});

fightSchema.method('markInBattle', function() {
    this.status = FIGHT_STATUS.IN_BATTLE;
    return this;
});

fightSchema.method('isFistful', function() {
    return this.battle_type === BATTLE_TYPE.FISTFUL;
});

fightSchema.method('makeAsCompleted', function() {
    this.status = FIGHT_STATUS.COMPLETED;
    this.save();
});

fightSchema.method('isOnLeftTeam', function(login: string) {
    return !!_.find(this.team.left, { login });
});

fightSchema.method('isOnRightTeam', function(login: string) {
    return !this.isOnLeftTeam(login);
});

fightSchema.method('isUserWinner', function(login: string) {
    return (
        (this.isOnLeftTeam(login) && this.team_winner === FIGHT_TEAM_WIN.LEFT) ||
        (this.isOnRightTeam(login) && this.team_winner === FIGHT_TEAM_WIN.RIGHT)
    );
});

fightSchema.method('isDraw', function() {
    return this.team_winner === FIGHT_TEAM_WIN.DRAW;
});

fightSchema.method('getUserDamageInfo', async function(user: IUser) {
    const team = this.isOnLeftTeam(user.login) ? 'left' : 'right';
    const damageInfo = await documents.FightRound.aggregate([
        {
            $match: {
                [`member_${team}.login`]: user.login,
                fight: this,
            },
        },
        {
            $group: {
                _id: `$member_${team}.login`,
                inflicted_damage: {
                    $sum: `$member_${team}.inflicted_damage`,
                },
                taken_damage: {
                    $sum: `$member_${team}.taken_damage`,
                },
            },
        },
        { $project: { _id: 0, login: '$_id', inflicted_damage: 1, taken_damage: 1 } },
    ]);

    return _.find(damageInfo as any[], { login: user.login }) || { inflicted_damage: 0, taken_damage: 0 };
});

fightSchema.method('getFightDamageInfo', async function() {
    const damageInfo = await Promise.all([
        getFightInfoForTeam.call(this, 'left'),
        getFightInfoForTeam.call(this, 'right'),
    ]);
    return _.flatten(damageInfo);
});

fightSchema.method('getNextCharacterForAttack', async function(user: IUser) {
    if (user.isDead()) {
        return null;
    }

    let teamAttack = 'right',
        teamDefend = 'left';

    if (this.isOnLeftTeam(user.login)) {
        [teamAttack, teamDefend] = [teamDefend, teamAttack];
    }

    const fightRound = await documents.FightRound.findOne({
        fight: this,
        status: FIGHT_ROUND_STATUS.IN_PROGRESS,
        [`member_${teamAttack}.attack`]: { $eq: [] },
        [`member_${teamAttack}.block`]: { $eq: [] },
    });

    if (!fightRound) {
        return null;
    }

    const defendLogin = fightRound[`member_${teamDefend}`].login;
    const teamUser = this.getTeamUserByLogin(defendLogin);

    if (!teamUser || teamUser.hp_current <= 0) {
        return null;
    }

    return documents.User.findByUsername(defendLogin);
});

/**
 * Returns `true` if user can attack next character
 */
fightSchema.method('attack', async function(attackUser: IUser, defenderLogin: string, attackData: any) {
    if (!this.isTeamMemberExists(attackUser.login)) {
        throw new Error(`User "${attackUser.login}" does not exists in the fight.`);
    }

    if (attackUser.isDead()) {
        throw new Error(`User "${attackUser.login}" is dead, and can't attack.`);
    }

    if (!this.isTeamMemberExists(defenderLogin)) {
        throw new Error(`User "${defenderLogin}" does not exists in the fight.`);
    }

    const defendUser = await documents.User.findUserForFight(this, defenderLogin);

    if (!defendUser) {
        throw new Error(`User "${defenderLogin}" does not exists in the fight.`);
    }

    if (defendUser.isDead()) {
        throw new Error(`User "${defendUser.login}" is dead, and can't attack.`);
    }

    let teamAttacker = 'right',
        teamDefender = 'left',
        loginLeft = defendUser.login,
        loginRight = attackUser.login;

    if (this.isOnLeftTeam(attackUser.login)) {
        [teamAttacker, teamDefender] = [teamDefender, teamAttacker];
        [loginLeft, loginRight] = [loginRight, loginLeft];
    }

    const fightRound = await documents.FightRound
        .findOne({
            fight: this,
            status: FIGHT_ROUND_STATUS.IN_PROGRESS,
            [`member_${teamAttacker}.login`]: attackUser.login,
            [`member_${teamDefender}.login`]: defenderLogin,
            [`member_${teamAttacker}.attack`]: { $eq: [] },
            [`member_${teamAttacker}.block`]: { $eq: [] },
        })
        .sort({ updated_at: -1 });

    if (!fightRound) {
        throw new Error('User already attack.');
    }

    // if (fightRound.updated_at.valueOf() + 60000 * this.timeout < Date.now()) {
    //     throw new Error('Time for attack is done.');
    // }

    const attackUserRound = fightRound[`member_${teamAttacker}`];
    const defendUserRound = fightRound[`member_${teamDefender}`];

    attackUserRound.attack = attackData.attack;
    attackUserRound.block = attackData.block;

    const promises = [];

    let isUserIsBusyForAttacks = false;

    // todo: rewrite this piece of <strike>shit</strike> code
    if (!_.isEmpty(defendUserRound.attack) && !_.isEmpty(defendUserRound.block)) {
        const fightRoundScale = await documents.FightRoundScale.findFightRoundScale(this, loginLeft, loginRight);
        if (!fightRoundScale) {
            throw new Error('Fight round scale does not exists.');
        }

        isUserIsBusyForAttacks = await this.getNextCharacterForAttack(defendUser);
        isUserIsBusyForAttacks = !isUserIsBusyForAttacks;

        const roundScale = fightRoundScale.round.shift();

        const [teamAttackUser, teamDefendUser] = [
            this.getTeamUserByLogin(attackUser.login),
            this.getTeamUserByLogin(defendUser.login),
        ];

        attackUserRound.is_dodge = roundScale[`is_${teamAttacker}_user_dodge`];
        attackUserRound.is_critical = roundScale[`is_${teamAttacker}_user_critical`];

        defendUserRound.is_dodge = roundScale[`is_${teamDefender}_user_dodge`];
        defendUserRound.is_critical = roundScale[`is_${teamDefender}_user_critical`];

        calculateDamageAndTactics(
            attackUserRound,
            defendUserRound,
            attackUser,
            defendUser,
            teamAttackUser,
            teamDefendUser,
        );
        calculateDamageAndTactics(
            defendUserRound,
            attackUserRound,
            defendUser,
            attackUser,
            teamDefendUser,
            teamAttackUser,
        );

        fightRound.status = FIGHT_ROUND_STATUS.COMPLETED as any;

        promises.push(fightRoundScale.save(), attackUser.save(), defendUser.save(), this.save());

        if (!attackUser.isDead() && !defendUser.isDead()) {
            promises.push(
                documents.FightRound.create({
                    fight: this,
                    battle_type: this.battle_type,
                    [`member_${teamAttacker}`]: {
                        login: attackUser.login,
                    },
                    [`member_${teamDefender}`]: {
                        login: defenderLogin,
                    },
                }),
            );
        }
    } else {
        fightRound.expires_at = moment()
            .add(this.timeout, 'minutes')
            .toDate();
    }

    // todo: look on https://docs.mongodb.com/manual/tutorial/perform-two-phase-commits/
    await Promise.all(promises.concat(fightRound.save()));
    const result: any = {
        attacker_next_attack_info: null,
        defender_next_attack_info: null,
    };

    result.attacker_next_attack_info = await this.getInfoForNextAttack(attackUser);
    result.attacker_next_attack_info.rounds = await attachFightRoundScale(this, loginLeft, loginRight);

    if (isUserIsBusyForAttacks) {
        result.defender_next_attack_info = await this.getInfoForNextAttack(defendUser);
        result.defender_next_attack_info.rounds = result.attacker_next_attack_info.rounds;
    }

    return result;
});

async function attachFightRoundScale(fight, loginLeft, loginRight) {
    const fightRoundScale = await documents.FightRoundScale.findFightRoundScale(fight, loginLeft, loginRight);

    const fightRoundScaleJson = fightRoundScale.toJSON();

    return _.map(fightRoundScaleJson.round, round => ({
        ...round,
        login_right: fightRoundScaleJson.login_right,
        login_left: fightRoundScaleJson.login_left,
    }));
}

fightSchema.method('getInfoForNextAttack', async function(user: IUser) {
    let userRight = null;

    const attackCountPlaces = await user.getAttackCount();
    const defendCountPlaces = await user.getDefendCount();
    const userDamageInfo = await this.getUserDamageInfo(user);

    if (!user.isDead()) {
        userRight = await this.getNextCharacterForAttack(user);
    }

    const doc = this.toJSON();
    const team = doc.team;
    if (this.isOnRightTeam(user.login)) {
        [team.right, team.left] = [team.left, team.right];
    }

    const isUserDead = user.isDead();
    const attack = {
        _id: doc._id,
        created_at: doc.created_at,
        team,
        user_left: {
            login: user.login,
            level: user.level,
            hp_max: user.abilities.hp_max,
            hp_current: user.abilities.hp_current,
            tactics: (_.find(team.right.concat(team.left), { login: user.login }) as any).tactics,
        },
        user_right: null,
    };

    if (userRight && !isUserDead) {
        attack.user_right = {
            login: userRight.login,
            level: userRight.level,
            hp_max: userRight.abilities.hp_max,
            hp_current: userRight.abilities.hp_current,
        };
    }

    return {
        is_dead: isUserDead,
        timeout: this.timeout,
        ...userDamageInfo,
        attack_count_places: attackCountPlaces,
        defend_count_places: defendCountPlaces,
        attack,
    };
});

fightSchema.method('isFightEnded', function() {
    return (
        this.status === FIGHT_STATUS.COMPLETED ||
        this.team.left.every(t => t.hp_current === 0) ||
        this.team.right.every(t => t.hp_current === 0)
    );
});

fightSchema.static('isTeamMemberIsBusyInFight', async function(login: string) {
    const fightsCount = await FightModel.count({
        status: { $in: [FIGHT_STATUS.IN_PROGRESS, FIGHT_STATUS.IN_BATTLE] },
        $or: [{ 'team.right.login': login }, { 'team.left.login': login }],
    });
    return fightsCount > 0;
});

fightSchema.static('isTeamMemberIsInBattle', async function(login: string) {
    const fightsCount = await FightModel.count({
        status: FIGHT_STATUS.IN_BATTLE,
        $or: [{ 'team.right.login': login }, { 'team.left.login': login }],
    });
    return fightsCount > 0;
});

fightSchema.static('getInBattleFight', function(login: string) {
    return FightModel.findOne({
        status: FIGHT_STATUS.IN_BATTLE,
        $or: [{ 'team.right.login': login }, { 'team.left.login': login }],
    });
});

fightSchema.static('findInBattleFights', function() {
    return FightModel.find({ status: FIGHT_STATUS.IN_BATTLE });
});

fightSchema.static('findByTypeAndLocationForRoom', function(
    fightType: FightRoomType,
    location: ILocation,
    user?: IUser,
) {
    const query: any = { location };
    if (user) {
        query.$or = [{ 'team.right.login': user.login }, { 'team.left.login': user.login }];
    }

    switch (fightType) {
        case FIGHT_TYPE_FULL.CURRENT:
            query.status = FIGHT_STATUS.IN_BATTLE;
            break;

        case FIGHT_TYPE_FULL.FINISHED:
            const today = moment.utc();
            Object.assign(query, {
                status: FIGHT_STATUS.COMPLETED,
                finished_at: {
                    $gte: today
                        .clone()
                        .startOf('day')
                        .toDate(),
                    $lte: today.toDate(),
                },
            });
            break;

        default:
            Object.assign(query, {
                fight_type: fightType,
                status: FIGHT_STATUS.IN_PROGRESS,
            });
            break;
    }

    return FightModel.find(query).populate('created_by');
});

fightSchema.static('findInProgressFights', function(dateTo?: Date) {
    const params: any = {
        status: FIGHT_STATUS.IN_PROGRESS,
    };

    if (dateTo) {
        params.begins_in = { $lte: dateTo };
    }

    return FightModel.find(params);
});

fightSchema.static('aggregateUserFightsListInfo', async function(login: string, location: ILocation) {
    const aggResult: IFightTypeCount = {
        PHYSICAL: 0,
        CHAOTIC: 0,
        GROUP: 0,
        CURRENT: 0,
        FINISHED: 0,
    };
    const today = moment.utc();
    const [finishedUserFightsCount, fights] = await Promise.all([
        FightModel.count({
            location: location._id,
            $or: [{ 'team.right.login': login }, { 'team.left.login': login }],
            finished_at: {
                $gte: today
                    .clone()
                    .startOf('day')
                    .toDate(),
                $lte: today.toDate(),
            },
        }),
        FightModel.aggregate([
            {
                $match: {
                    location: location._id,
                    status: { $in: [FIGHT_STATUS.IN_PROGRESS, FIGHT_STATUS.IN_BATTLE] },
                },
            },
            {
                $group: {
                    _id: {
                        fight_type: '$fight_type',
                        status: '$status',
                    },
                    total: { $sum: 1 },
                },
            },
        ]) as any,
    ]);

    aggResult.FINISHED = finishedUserFightsCount;
    _.forEach(fights, (fight: any) => {
        if (fight._id.fight_type in aggResult && fight._id.status === FIGHT_STATUS.IN_PROGRESS) {
            aggResult[fight._id.fight_type] += fight.total;
        } else if (fight._id.status === FIGHT_STATUS.IN_BATTLE) {
            aggResult.CURRENT += fight.total;
        }
    });

    return aggResult;
});

fightSchema.pre('update', preSave);
fightSchema.pre('save', preSave);

async function preSave(next) {
    if (this.isModified('status')) {
        if (this.status === FIGHT_STATUS.COMPLETED) {
            this.finished_at = new Date();

            const survivedLeft = _.reject(this.team.left, { hp_current: 0 });
            const survivedRight = _.reject(this.team.right, { hp_current: 0 });

            if (survivedLeft.length === 0 && survivedRight.length === 0) {
                this.team_winner = FIGHT_TEAM_WIN.DRAW;
            } else if (survivedLeft.length > 0) {
                this.team_winner = FIGHT_TEAM_WIN.LEFT;
            } else {
                this.team_winner = FIGHT_TEAM_WIN.RIGHT;
            }

            await Promise.all([
                documents.User.updateLastFight(this),
                // remove round info, all data already stored in the FightLog
                documents.FightRound.remove({ fight: this._id }),
            ]);
        } else if (this.status === FIGHT_STATUS.IN_BATTLE) {
            // todo: temporary, update hp_max if user changed some artefact and etc.
            _.each(this.team.right, (userRight: any) => {
                userRight.hp_current = userRight.hp_max;
            });
            _.each(this.team.left, (userLeft: any) => {
                userLeft.hp_current = userLeft.hp_max;
            });

            // todo: look on https://docs.mongodb.com/manual/tutorial/perform-two-phase-commits/
            await Promise.all([
                documents.User.setUsersCurrentFight(this),
                documents.FightRoundScale.createTeamBlowsScale(this),
                documents.FightRound.createTeamsEmptyRounds(this),
            ]);
        }
    }

    return next();
}

function calculateDamageAndTactics(
    attackUserRound,
    defendUserRound,
    attackUser,
    defendUser,
    teamAttackUser,
    teamDefendUser,
) {
    const defendUserAttackDiff = _.difference(defendUserRound.attack, attackUserRound.block);

    if (attackUserRound.is_dodge) {
        // todo: add parry, counterblow tactics
        return;
    }

    const ifBlocked = _.isEmpty(defendUserAttackDiff);
    if (ifBlocked && !defendUserRound.is_critical) {
        teamAttackUser.tactics.block += 1;
        return;
    }

    if (!ifBlocked || defendUserRound.is_critical) {
        attackUserRound.taken_damage = defendUser.getDamageBetweenMinMax();
        if (defendUserRound.is_critical) {
            attackUserRound.taken_damage *= defendUserAttackDiff.length + 1;
            teamDefendUser.tactics.critical += ifBlocked ? 1 : 2;
        } else if (!ifBlocked) {
            teamDefendUser.tactics.hitting += 1;
        }
        defendUserRound.inflicted_damage = attackUserRound.taken_damage;

        attackUser.current_fight_info.hp -= attackUserRound.taken_damage;

        teamAttackUser.hp_current = attackUser.current_fight_info.hp;

        attackUserRound.hp_current = teamAttackUser.hp_current;
        attackUserRound.hp_max = teamAttackUser.hp_max;
    }
}

function getFightInfoForTeam(team: 'left' | 'right') {
    return documents.FightRound.aggregate([
        {
            $match: {
                fight: this._id,
            },
        },
        {
            $group: {
                _id: `$member_${team}.login`,
                inflicted_damage: {
                    $sum: `$member_${team}.inflicted_damage`,
                },
                taken_damage: {
                    $sum: `$member_${team}.taken_damage`,
                },
            },
        },
        { $project: { _id: 0, login: '$_id', inflicted_damage: 1, taken_damage: 1 } },
    ]);
}

const FightModel = mongoose.model<IFight>('Fight', fightSchema);
export default FightModel;
