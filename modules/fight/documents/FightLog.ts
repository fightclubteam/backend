'use strict';

import * as mongoose from 'mongoose';
import * as _ from 'lodash';

import { IFightLog } from '../interfaces/fight.log';
import { FIGHT_ACTION_BODY_PART } from '../enums/fight';
import { pubSub } from '../../../_shared/pub-sub';
import { FIGHT_ACTION_TYPE } from '../enums/fight.action';

const fightLogSchema = new mongoose.Schema(
    {
        fight: {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'Fight',
            require: true,
        },
        logs: [
            {
                log: {
                    type: String,
                    require: true,
                },
                action: {
                    type: String,
                    enum: _.values(FIGHT_ACTION_TYPE),
                    require: true,
                },
                attacker: {
                    type: String,
                },
                defender: {
                    type: String,
                },
                login: {
                    type: String,
                },
                inflicted_damage: {
                    type: Number,
                    min: 0,
                },
                hp: {
                    current: {
                        type: Number,
                        min: 0,
                    },
                    max: {
                        type: Number,
                        min: 0,
                    },
                },
                body_part: {
                    attack: [
                        {
                            type: String,
                            enum: _.values(FIGHT_ACTION_BODY_PART),
                            require: true,
                        },
                    ],
                    block: [
                        {
                            type: String,
                            enum: _.values(FIGHT_ACTION_BODY_PART),
                            require: true,
                        },
                    ],
                },
                created_at: {
                    type: Date,
                    default: Date.now,
                },
            },
        ],
    },
    {
        timestamps: { createdAt: 'created_at', updatedAt: 'updated_at' },
        collection: 'fight_logs',
    },
)
    .index({ fight: -1 })
    .index({ updated_at: -1 });

fightLogSchema.post('save', function(doc: any) {
    pubSub.publish('battleLogAdded', doc);
});

const FightLogModel = mongoose.model<IFightLog>('FightLog', fightLogSchema);
export default FightLogModel;
