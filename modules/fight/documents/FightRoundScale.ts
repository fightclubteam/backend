'use strict';

import * as mongoose from 'mongoose';
import * as _ from 'lodash';
import { IFightRoundScale } from '../interfaces/fight.round.scale';
import { IFight } from '../interfaces/fight';
import { uniformDistributionIntegers } from '../../../_shared/helper';
import { documents } from '../../../_shared/documents';

const fightRoundScaleSchema = new mongoose.Schema(
    {
        fight: {
            type: mongoose.Schema.Types.ObjectId,
            ref: 'Fight',
            require: true,
        },
        login_left: {
            type: String,
            require: true,
        },
        login_right: {
            type: String,
            require: true,
        },
        round: [
            {
                round: {
                    type: Number,
                    require: true,
                    min: 0,
                },
                is_left_user_dodge: {
                    type: Boolean,
                    require: true,
                },
                is_right_user_dodge: {
                    type: Boolean,
                    require: true,
                },
                is_left_user_critical: {
                    type: Boolean,
                    require: true,
                },
                is_right_user_critical: {
                    type: Boolean,
                    require: true,
                },
            },
        ],
    },
    {
        timestamps: null,
        collection: 'fight_round_scale',
    },
)
    .index({ fight: 1 })
    .index({ login_left: 1, login_right: 1 })
    .index({ round: 1 });

fightRoundScaleSchema.static('findFightRoundScale', function(fight: IFight, loginLeft: string, loginRight: string) {
    return FightRoundScaleModel.findOne({ fight, login_left: loginLeft, login_right: loginRight });
});

fightRoundScaleSchema.static('createTeamBlowsScale', function(fight: IFight) {
    const data = [];
    _.each(fight.team.right, userRight => {
        _.each(fight.team.left, userLeft => {
            const scaleRow = {
                login_right: userRight.login,
                login_left: userLeft.login,
                fight,
                round: [],
            };

            const mallType = documents.User.getCommonMallType(userRight.mall_type, userLeft.mall_type);

            const criticalListRight = uniformDistributionIntegers(
                mallType * (userRight.modifier_critical - userLeft.modifier_anti_critical),
            );
            const dodgeListRight = uniformDistributionIntegers(
                mallType * (userRight.modifier_dodge - userLeft.modifier_anti_dodge),
            );

            const criticalListLeft = uniformDistributionIntegers(
                mallType * (userLeft.modifier_critical - userRight.modifier_anti_critical),
            );
            const dodgeListLeft = uniformDistributionIntegers(
                mallType * (userLeft.modifier_dodge - userRight.modifier_anti_dodge),
            );

            for (let i = 0; i < 100; i++) {
                scaleRow.round.push({
                    round: i,
                    is_left_user_dodge: !!dodgeListLeft[i],
                    is_right_user_dodge: !!dodgeListRight[i],
                    is_left_user_critical: !!criticalListLeft[i],
                    is_right_user_critical: !!criticalListRight[i],
                });
            }

            data.push(scaleRow);
        });
    });

    return FightRoundScaleModel.insertMany(data);
});

const FightRoundScaleModel = mongoose.model<IFightRoundScale>('FightRoundScale', fightRoundScaleSchema);

export default FightRoundScaleModel;
