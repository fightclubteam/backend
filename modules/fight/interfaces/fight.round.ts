'use strict';

import * as mongoose from 'mongoose';
import DocumentQuery = mongoose.DocumentQuery;
import { BattleType, FightActionBodyPartType, IFight, FightRoundStatusType } from './fight';

export interface IFightRoundMember {
    login: string;
    attack: FightActionBodyPartType[];
    block: FightActionBodyPartType[];
    inflicted_damage: number;
    taken_damage: number;
    hp_current?: number;
    hp_max?: number;
    is_dodge: boolean;
    is_critical: boolean;
    created_at: Date;
}

export interface IFightRound extends mongoose.Document {
    fight: string;
    battle_type: BattleType;
    status: FightRoundStatusType;
    member_right: IFightRoundMember;
    member_left: IFightRoundMember;
    expires_at?: Date;
    created_at: Date;
    updated_at: Date;

    inProgress(): boolean;
}

export interface IFightRoundDocument extends mongoose.Model<IFightRound> {
    createTeamsEmptyRounds(fight: IFight): DocumentQuery<IFightRound[], IFightRound>;
    findInProgressBlows(dateTo?: Date): DocumentQuery<IFightRound[], IFightRound>;
}
