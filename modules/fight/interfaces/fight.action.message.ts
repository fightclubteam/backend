'use strict';

import * as mongoose from 'mongoose';
import { BattleType, FightActionBodyPartType } from './fight';

export type FightActionType = 'BLOCK' | 'ATTACK' | 'DEAD' | 'DODGE' | 'CRITICAL' | 'BREAK_THROUGH_BLOCK';

export interface IFightActionMessage extends mongoose.Document {
    fight: string;
    battle_type: BattleType;
    body_part: FightActionBodyPartType;
    action: FightActionType;
    message: string;
}

export interface IFightActionMessageDocument extends mongoose.Model<IFightActionMessage> {
    findRandomMessage(
        action: FightActionType,
        battleType?: BattleType,
        bodyPart?: FightActionBodyPartType,
    ): Promise<IFightActionMessage>;
}
