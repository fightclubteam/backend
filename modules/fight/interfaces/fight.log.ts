'use strict';

import * as mongoose from 'mongoose';
import { FightActionBodyPartType } from './fight';
import { FightActionType } from './fight.action.message';

export interface IFightSingleLog {
    action: FightActionType;
    log: string;
    login: string;
    attacker: string;
    defender: string;
    inflicted_damage?: number;
    hp: {
        current: number;
        max: number;
    };
    body_part: {
        attack: FightActionBodyPartType[];
        block: FightActionBodyPartType[];
    };
    created_at: Date;
}

export interface IFightLog extends mongoose.Document {
    id: string;
    fight: string;
    logs: IFightSingleLog[];
    created_at: Date;
    updated_at: Date;
}

export interface IFightLogDocument extends mongoose.Model<IFightLog> {}
