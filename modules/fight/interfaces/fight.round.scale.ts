'use strict';

import * as mongoose from 'mongoose';
import { IFight } from './fight';

export interface IFightRoundScaleRound {
    round: number;
    is_left_user_dodge: boolean;
    is_right_user_dodge: boolean;
    is_left_user_critical: boolean;
    is_right_user_critical: boolean;
}

export interface IFightRoundScale extends mongoose.Document {
    fight: string;
    round: IFightRoundScaleRound[];
    login_left: string;
    login_right: string;
}

export interface IFightRoundScaleDocument extends mongoose.Model<IFightRoundScale> {
    createTeamBlowsScale(fight: IFight);
    findFightRoundScale(fight: IFight, loginLeft: string, loginRight: string);
}
