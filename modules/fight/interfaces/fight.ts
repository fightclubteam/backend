'use strict';

import * as mongoose from 'mongoose';
import DocumentQuery = mongoose.DocumentQuery;
import { IUser } from '../../user/interfaces/user';
import { ILocation } from '../../location/interfaces/location';

export interface IFightTeam {
    login: string;
    status: FightTeamStatusType;
    tactics: IFightTeamTactics;
    level: number;
    hp_max: number;
    hp_current: number;
    mall_type: number;
    modifier_dodge: number;
    modifier_anti_dodge: number;
    modifier_critical: number;
    modifier_anti_critical: number;
    created_at: Date;
}

export type FightType = 'PHYSICAL' | 'GROUP' | 'CHAOTIC';
export type BattleKindType = 'WITH_SCUMBAG' | 'NO_HOLDS_BARRED' | 'DEADLY_WOUNDS';
export type BattleType = 'FISTFUL' | 'WEAPON';
export type FightTeamType = 'LEFT' | 'RIGHT';
export type FightStatusType = 'COMPLETED' | 'IN_PROGRESS' | 'IN_BATTLE';
export type FightTeamStatusType = 'WAITING_FOR_APPROVE' | 'APPROVED';
export type FightActionBodyPartType = 'HEAD' | 'CHEST' | 'ABDOMEN' | 'GIRDLE' | 'LEGS';
export type FightRoundStatusType = 'COMPLETED' | 'EXPIRED' | 'CHEST';

export type FightParticipantType =
    | 'ANY_LEVEL'
    | 'MY_AND_LOWER_LEVEL'
    | 'LOWER_LEVEL'
    | 'MY_LEVEL'
    | 'MY_LEVEL_PLUS_ONE_MAX'
    | 'MY_LEVEL_MINUS_ONE_MIN'
    | 'MY_LEVEL_PLUS_MINUS_ONE'
    | 'CLAN_ONLY';

export interface IFightTypeCount {
    PHYSICAL: number;
    CHAOTIC: number;
    GROUP: number;
    CURRENT: number;
    FINISHED: number;
}

export interface IFightTeamTactics {
    hitting: number;
    critical: number;
    dodge: number;
    parry: number;
    inflicted_damage: number;
    spirituality_sincerity: number;
    counterblow: number;
}
export interface IDamageInfo {
    inflicted_damage: number;
    taken_damage: number;
    login: string;
}

export type FightRoomType = 'PHYSICAL' | 'CHAOTIC' | 'GROUP' | 'CURRENT' | 'FINISHED';
export type FightTeamWinnerType = 'LEFT' | 'RIGHT' | 'DRAW';

export interface IFight extends mongoose.Document {
    id?: string;
    created_by: string;
    location: string;
    fight_type: FightType;
    battle_kind?: BattleKindType;
    battle_type: BattleType;
    right_count?: number;
    right_type?: FightParticipantType;
    left_count?: number;
    left_type?: FightParticipantType;
    timeout: number;
    comment?: string;
    team: { left: IFightTeam[]; right: IFightTeam[] };
    status: FightStatusType;
    team_winner?: FightTeamWinnerType;

    begins_in: Date;
    finished_at?: Date;
    created_at?: Date;
    updated_at?: Date;

    isOnLeftTeam(login: string): boolean;
    isOnRightTeam(login: string): boolean;
    canStartFight(): boolean;
    getAllTeamLoginList(): string[];
    getTeamRightLoginList(): string[];
    getTeamLeftLoginList(): string[];
    getTeamMembersCount(): number;
    getCreator(): IFightTeam | undefined;
    getWaitingForApproveTeamMember(login: string): IFightTeam | undefined;
    getWaitingForApproveTeamMembersCount(): number;
    canJoinToWaitingForApproveTeam(): boolean;
    isTeamLevelValid(compareLevel: number, team: FightTeamType): boolean;
    isTeamMemberExists(login: string): boolean;
    getTeamUserByLogin(login: string): IFightTeam | undefined;
    isCreator(creator: IUser): boolean;
    isSameLocation(location: ILocation): boolean;
    isFightEmpty(): boolean;
    inProgress(): boolean;
    inBattle(): boolean;
    isPhysical(): boolean;
    isFistful(): boolean;
    isChaotic(): boolean;
    isGroup(): boolean;
    isDraw(): boolean;
    isUserWinner(login: string): boolean;

    attack(attackingUser: string, protectingUser: string, attackData: any /*todo: add interface*/);
    isRoundEnded(): Promise<boolean>;
    getNextCharacterForAttack(login: string): Promise<IUser>;
    getUserDamageInfo(user: IUser): Promise<{ inflicted_damage: number; taken_damage: number }>;
    getFightDamageInfo(): Promise<IDamageInfo[]>;
    makeFightInBattle(): DocumentQuery<IFight | null, IFight>;
    kickTeamMember(login: string): Promise<IFight>;
    markAdversaryAsApproved(login: string): IFight;
    markInBattle(): IFight;
    getInfoForNextAttack(user: IUser): Promise<any>;
    isFightEnded(): boolean;
    makeAsCompleted(): Promise<IFight>;
}

export interface IFightDocument extends mongoose.Model<IFight> {
    aggregateUserFightsListInfo(login: string, location: ILocation): Promise<IFightTypeCount>;
    findInProgressFights(dateTo?: Date): DocumentQuery<IFight[], IFight>;
    findByTypeAndLocationForRoom(
        fightType: FightRoomType,
        location: string,
        user?: IUser,
    ): DocumentQuery<IFight[], IFight>;
    isTeamMemberIsInBattle(login: string): Promise<boolean>;
    isTeamMemberIsBusyInFight(login: string): Promise<boolean>;
    getInBattleFight(login: string): DocumentQuery<IFight | null, IFight>;
    findInBattleFights(): DocumentQuery<IFight[], IFight>;
}
