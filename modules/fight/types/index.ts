'use strict';

import battleTypes from './battle';
import fightTypes from './fight';

export default [battleTypes, fightTypes];
