'use strict';

export default `
    enum FightTypeEnum {
        PHYSICAL
        GROUP
        CHAOTIC
    }
    
    enum FightTypeRoomEnum {
        PHYSICAL
        GROUP
        CHAOTIC
        FINISHED
        CURRENT
    }
    
    enum FightStatusEnum {
        COMPLETED
        IN_PROGRESS
        IN_BATTLE
    }
    
    enum FightTeamWinnerEnum {
        LEFT
        RIGHT
        DRAW
    }
    
    enum FightParticipantStatusEnum {
        WAITING_FOR_APPROVE
        APPROVED
    }
    
    enum FightParticipantTypeEnum {
        ANY_LEVEL
        MY_AND_LOWER_LEVEL
        LOWER_LEVEL
        MY_LEVEL
        MY_LEVEL_PLUS_ONE_MAX
        MY_LEVEL_MINUS_ONE_MIN
        MY_LEVEL_PLUS_MINUS_ONE
        CLAN_ONLY
    }
    
    enum FightParticipantTeamEnum {
        LEFT
        RIGHT
    }
    
    enum BattleTypeEnum {
        FISTFUL
        WEAPON
    }
    
    enum BattleKindEnum {
        WITH_SCUMBAG
        NO_HOLDS_BARRED
        DEADLY_WOUNDS
    }
    
    enum FightActionBodyPartEnum {
        HEAD
        CHEST
        ABDOMEN
        GIRDLE
        LEGS
    }
    
    enum FightRejectReasonEnum {
        NOT_ENOUGH_PARTICIPANTS
    }
    
    type FightUserType {
        login: String!
        level: Int!
        status: FightParticipantStatusEnum!
        created_at: String!
    }
    
    type FightHostType {
        login: String!
        level: Int!
    }
    
    type FightTeam {
        left: [FightUserType]!
        right: [FightUserType]!
    }
    
    type FightDecline {
        fight_id: String!
    }
    
    type FightType {
        _id: String!
        created_by: FightHostType!
        battle_type: BattleTypeEnum!
        battle_kind: BattleKindEnum
        fight_type: FightTypeEnum!
        status: FightStatusEnum!
        timeout: Int!
        left_count: Int!
        left_type: FightParticipantTypeEnum!
        right_count: Int!
        right_type: FightParticipantTypeEnum!
        team: FightTeam!
        team_winner: FightTeamWinnerEnum
        status: FightStatusEnum!
        comment: String
        begins_in: String
        finished_at: String
        created_at: String!
    }
    
    type FightTypesListCountType {
        PHYSICAL: Int
        CHAOTIC: Int
        GROUP: Int
        CURRENT: Int
        FINISHED: Int
    }
    
    type FightReject {
        fight_id: String!
        reason_type: FightRejectReasonEnum!
    }
    
    type FightApprove {
        fight_id: String!
    }
    
    type DeclineFight {
        fight_id: String!
    }
    
    type Query {
        fightTypesListCount: FightTypesListCountType!
        fights(fight_type: FightTypeRoomEnum!, userId: String): [FightType]!
    }
    
    type Subscription {
        fightCreated(fight_type: FightTypeEnum!, location: String!): FightType!
        fightJoined(fight_type: FightTypeEnum!, location: String!): FightType!
        fightRefused(fight_type: FightTypeEnum!, location: String!): FightType!
        fightApproved: FightApprove!
        fightRejected(fight_type: FightTypeEnum, location: String!): FightReject!
        fightDeclined(fight_type: FightTypeEnum!, location: String!): FightDecline!
    }
    
    type Mutation {
        createFight(
            battle_type: BattleTypeEnum!
            battle_kind: BattleKindEnum
            fight_type: FightTypeEnum!
            timeout: Int!
            comment: String
            begins_at: Int
            right_count: Int
            left_count: Int
        ): FightType!
        declineFight(fight_id: String!): DeclineFight!
        joinFight(team: FightParticipantTeamEnum!, fight_id: String!): FightType!
        refuseFight(fight_id: String!): FightType!
        respondFight(login: String!, approve: Boolean!, fight_id: String!): FightType!
    }
`;
