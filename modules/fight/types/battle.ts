'use strict';

export default `
    type BattleUserTacticsType {
        hitting: Int!
        critical: Int!
        counterblow: Int!
        block: Int!
        inflicted_damage: Int!
        spirituality_sincerity: Int!
        parry: Int!
    }
    
    type BattleTeamUser {
        login: String!
        level: Int!
        hp_max: Int!
        hp_current: Int!
    }
    
    enum BattleVictoryStatusEnum {
        WIN
        DEFEAT
        DRAW
    }
    
    type BattleUser {
        login: String!
        level: Int!
        hp_max: Int!
        hp_current: Int!
        tactics: BattleUserTacticsType!
    }
    
    type BattleLogHp {
        current: Int
        max: Int
    }
    
    type BattleLogBodyPartAction {
        attack: [FightActionBodyPartEnum]!
        block: [FightActionBodyPartEnum]!
    }
    
    type BattleRoundScale {
        round: Int!
        is_left_user_dodge: Boolean!
        is_right_user_dodge: Boolean!
        is_left_user_critical: Boolean!
        is_right_user_critical: Boolean!
        login_right: String!
        login_left: String!
    }
    
    type BattleLog {
        _id: String!
        action: String!
        log: String!
        login: String
        attacker: String
        defender: String
        inflicted_damage: Int
        hp: BattleLogHp
        body_part: BattleLogBodyPartAction
        created_at: String!
    }
    
    type BattleFullLogList {
        _id: String!
        team: BattleTeam!
        rounds: [[BattleLog]]!
    }
    
    type BattleTeam {
        left: [BattleTeamUser]!
        right: [BattleTeamUser]!
    }
    
    type Attack {
        _id: String!
        team: BattleTeam!
        user_left: BattleUser!
        user_right: BattleTeamUser
        created_at: String!
    }
    
    type Battle {
        attack: Attack!
        timeout: Int!
        inflicted_damage: Int!
        taken_damage: Int!
        is_dead: Boolean!
        attack_count_places: Int!
        defend_count_places: Int!
        rounds: [BattleRoundScale]
    }
    
    type BattleEnd {
        exp: Int!
        inflicted_damage: Int!
        taken_damage: Int!
        victory_status: BattleVictoryStatusEnum!
    }
    
    type BattleStart {
        fight_id: String!
    }
    
    type Query {
        battle(fight_id: String!): Battle!
        getCurrentBattleId: String
        battleLogs(fight_id: String!): [[BattleLog]]!
        battleFullLogs(fight_id: String!): BattleFullLogList!
    }
    
    type Mutation {
        attack(login: String!, attack: [FightActionBodyPartEnum]!, block: [FightActionBodyPartEnum]!): Battle!
    }
    
    type Subscription {
        battleStarted(login: String!): BattleStart!
        battleEnded(login: String!): BattleEnd!
        blowsChanged(login: String!): Battle!
        blowsExpired(login: String!): Battle!
        battleLogAdded(fight_id: String!): [[BattleLog]]!
    }
`;
