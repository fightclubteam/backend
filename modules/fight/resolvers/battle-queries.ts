'use strict';

import * as mongoose from 'mongoose';
import * as _ from 'lodash';
import { documents } from '../../../_shared/documents';
import { FightNotFoundError, FightStatusError, FightUserNotExistError } from '../errors/index';

export default {
    Query: {
        async getCurrentBattleId({ user }) {
            const fightInBattle = await documents.Fight.getInBattleFight(user.login);
            return fightInBattle ? fightInBattle.id : null;
        },
        async battle({ user }, args) {
            const fight = await documents.Fight.findById(args.fight_id).populate('created_by');
            if (!fight) {
                throw new FightNotFoundError('Fight not found.');
            }

            if (!fight.inBattle()) {
                throw new FightStatusError('Fight is not in battle.');
            }

            if (!fight.isTeamMemberExists(user.login)) {
                throw new FightUserNotExistError('User does not exists in the battle.');
            }

            return fight.getInfoForNextAttack(user);
        },
        async battleLogs({ user }, { fight_id }) {
            const battleLogs = await documents.FightLog
                .find({ fight: new mongoose.Types.ObjectId(fight_id) })
                .limit(10)
                .sort({ updated_at: -1 });

            return _.map(battleLogs, 'logs');
        },
        async battleFullLogs({ user }, { fight_id }) {
            const [logs, fight] = await Promise.all([
                documents.FightLog.find({ fight: new mongoose.Types.ObjectId(fight_id) }).sort({ updated_at: -1 }),
                documents.Fight.findById(fight_id),
            ]);

            if (!fight) {
                throw new FightNotFoundError('Fight not found.');
            }

            const team = fight.team;

            if (_.get(user, 'login', null) !== null && fight.isOnRightTeam(user.login)) {
                [team.right, team.left] = [team.left, team.right];
            }

            return {
                _id: fight_id,
                team,
                rounds: _.map(logs, 'logs'),
            };
        },
    },
};
