'use strict';

import { documents } from '../../../_shared/documents';
import { FIGHT_TYPE_FULL } from '../enums/fight';

export default {
    Query: {
        fightTypesListCount({ user }) {
            return documents.Fight.aggregateUserFightsListInfo(user.login, user.location);
        },
        fights({ user }, args) {
            return documents.Fight
                .findByTypeAndLocationForRoom(
                    args.fight_type,
                    user.location,
                    FIGHT_TYPE_FULL.FINISHED === args.fight_type ? user : undefined,
                )
                .lean();
        },
    },
};
