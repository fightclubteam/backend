'use strict';

import * as Joi from 'joi';
import * as _ from 'lodash';

import { documents } from '../../../_shared/documents';
import { FightNotFoundError } from '../errors/index';
import { pubSub } from '../../../_shared/pub-sub';

import FightRoundRequestValidator from '../validations/fight.round.request';

export default {
    Mutation: {
        async attack({ user, acl }, args) {
            args = Joi.attempt(
                args,
                FightRoundRequestValidator(await user.getAttackCount(), await user.getDefendCount()).options({
                    stripUnknown: true,
                }),
            ) as any;
            const fight = await documents.Fight.getInBattleFight(user.login);

            if (!fight) {
                throw new FightNotFoundError('Fight not found.');
            }

            const {
                attacker_next_attack_info: attackerNextAttackInfo,
                defender_next_attack_info: defenderNextAttackInfo,
            } = await fight.attack(user, args.login, _.omit(args, ['login']));

            const isFightEnded = fight.isFightEnded();
            if (defenderNextAttackInfo && !isFightEnded) {
                pubSub.publish('blowsChanged', defenderNextAttackInfo);
            }

            if (isFightEnded) {
                const [fightDamageInfo] = await Promise.all([fight.getFightDamageInfo(), fight.makeAsCompleted()]);
                const isDraw = fight.isDraw();
                _.each(fightDamageInfo, userDamageInfo => {
                    const victoryStatus = isDraw ? 'DRAW' : fight.isUserWinner(userDamageInfo.login) ? 'WIN' : 'DEFEAT';
                    pubSub.publish('battleEnded', {
                        ...userDamageInfo,
                        exp: victoryStatus === 'WIN' ? 20 : 0,
                        victory_status: victoryStatus,
                    });
                });
            }

            return attackerNextAttackInfo;
        },
    },
};
