'use strict';

import * as _ from 'lodash';
import { pubSub } from '../../../_shared/pub-sub';
import { withFilter } from 'graphql-subscriptions';

export default {
    Subscription: {
        battleStarted: {
            subscribe: withFilter(
                () => pubSub.asyncIterator('battleStarted'),
                (fight, { login }) => _.includes(fight.members, login),
            ),
            resolve: payload => payload,
        },
        blowsChanged: {
            subscribe: withFilter(
                () => pubSub.asyncIterator('blowsChanged'),
                (fight, { login }) => fight.attack.user_left.login === login,
            ),
            resolve: payload => payload,
        },
        battleEnded: {
            subscribe: withFilter(
                () => pubSub.asyncIterator('battleEnded'),
                (fight, { login }) => fight.login === login,
            ),
            resolve: payload => payload,
        },
        battleLogAdded: {
            subscribe: withFilter(
                () => pubSub.asyncIterator('battleLogAdded'),
                (battleLogs, { fight_id }) => battleLogs.fight === fight_id,
            ),
            resolve: payload => {
                return [payload.logs];
            },
        },
    },
};
