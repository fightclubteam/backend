'use strict';

import { mergeResolvers } from 'merge-graphql-schemas';

import battleMutationsResolvers from './battle-mutations';
import battleQueriesResolvers from './battle-queries';
import battleSubscriptionsResolvers from './battle-subscriptions';
import fightMutationsResolvers from './fight-mutations';
import fightQueriesResolvers from './fight-queries';
import fightSubscriptionsResolvers from './fight-subscriptions';

export default mergeResolvers([
    battleMutationsResolvers,
    fightQueriesResolvers,
    battleQueriesResolvers,
    fightMutationsResolvers,
    fightSubscriptionsResolvers,
    battleSubscriptionsResolvers,
]);
