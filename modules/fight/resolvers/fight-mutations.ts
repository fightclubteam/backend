'use strict';

import * as Joi from 'joi';
import * as moment from 'moment';

import { documents } from '../../../_shared/documents';
import { pubSub } from '../../../_shared/pub-sub';
import {
    FightEmptyError,
    FightError,
    FightNotFoundError,
    FightStatusError,
    FightTeamLevelError,
    FightTypeError,
    FightUserDiffLocationError,
    FightUserIsBusyError,
    FightUserNotExistError,
} from '../errors/index';

import FightNewValidator from './../validations/fight.new';

import { FIGHT_PARTICIPANT_STATUS, FIGHT_PARTICIPANT_TEAM } from '../enums/fight';

export default {
    Mutation: {
        async createFight({ user }, args) {
            const fightData = Joi.attempt(args, FightNewValidator.options({ stripUnknown: true })) as any;

            const isUserBusy = await documents.Fight.isTeamMemberIsBusyInFight(user.login);
            if (isUserBusy) {
                throw new FightUserIsBusyError('User is busy on other fight.');
            }

            if (user.hasLowHP()) {
                throw new Error('Cannot create a fight, user has small amount of HP.');
            }

            try {
                const beginsIn = moment
                    .utc()
                    .add(fightData.begins_at, 'minutes')
                    .toDate();
                const fight = (await documents.Fight.create({
                    ...fightData,
                    created_by: user,
                    location: user.location,
                    team: {
                        left: [
                            {
                                login: user.login,
                                level: user.level,
                                status: FIGHT_PARTICIPANT_STATUS.APPROVED,
                                hp_max: user.abilities.hp_max,
                                mall_type: user.abilities.mall_type,
                                modifier_dodge: user.modifiers.dodge,
                                modifier_anti_dodge: user.modifiers.anti_dodge,
                                modifier_critical: user.modifiers.critical,
                                modifier_anti_critical: user.modifiers.anti_critical,
                            },
                        ],
                        right: [],
                    },
                    begins_in: beginsIn,
                })) as any;

                pubSub.publish('fightCreated', fight);
                return fight;
            } catch (err) {
                throw new FightError("Can't create a fight. " + err.message);
            }
        },
        async declineFight({ user }, args) {
            const fight = await documents.Fight.findById(args.fight_id);

            if (!fight) {
                throw new FightNotFoundError('Fight not found.');
            }

            if (!fight.inProgress()) {
                throw new FightStatusError('Fight is not in progress.');
            }

            if (!fight.isCreator(user)) {
                throw new FightError('Cannot decline the fight, you are not the host of the fight.');
            }

            if (!fight.isFightEmpty()) {
                throw new FightEmptyError('Cannot decline the fight, other user joined to the fight.');
            }

            if (!fight.isSameLocation(user.location)) {
                throw new FightUserDiffLocationError('Cannot decline the fight, user located on other location.');
            }

            const { location, fight_type } = fight;
            await fight.remove();

            pubSub.publish('fightDeclined', {
                fight_id: args.fight_id,
                location,
                fight_type,
            });

            return { fight_id: args.fight_id };
        },
        async joinFight({ user }, args) {
            const fight = await documents.Fight.findById(args.fight_id).populate('created_by');

            if (!fight) {
                throw new FightNotFoundError('Fight not found.');
            }

            if (!fight.inProgress()) {
                throw new FightStatusError('Fight is not in progress.');
            }

            if (fight.isTeamMemberExists(user.login)) {
                throw new FightUserNotExistError('Invalid fight data structure. User already exists in fight');
            }

            if (fight.isPhysical() && args.team === FIGHT_PARTICIPANT_TEAM.LEFT) {
                throw new FightTypeError("Invalid fight data structure. Can't create a request to left team.");
            }

            if (!fight.canJoinToWaitingForApproveTeam()) {
                throw new FightError(
                    'Invalid fight data structure. Limit of waiting for approve team members is done.',
                );
            }

            const isUserBusy = await documents.Fight.isTeamMemberIsBusyInFight(user.login);
            if (isUserBusy) {
                throw new FightUserIsBusyError('User is busy on other fight.');
            }

            if (!fight.isSameLocation(user.location)) {
                throw new FightUserDiffLocationError('Cannot join to the fight, user located on other location.');
            }

            if (!fight.isTeamLevelValid(user.level, args.team)) {
                throw new FightTeamLevelError('User level invalid.');
            }

            const teamUserData = {
                login: user.login,
                level: user.level,
                status: FIGHT_PARTICIPANT_STATUS.APPROVED,
                hp_max: user.abilities.hp_max,
                mall_type: user.abilities.mall_type,
                modifier_dodge: user.modifiers.dodge,
                modifier_anti_dodge: user.modifiers.anti_dodge,
                modifier_critical: user.modifiers.critical,
                modifier_anti_critical: user.modifiers.anti_critical,
            };

            if (fight.isPhysical()) {
                teamUserData.status = FIGHT_PARTICIPANT_STATUS.WAITING_FOR_APPROVE;
            }

            fight.team[args.team.toLowerCase()].push(teamUserData);

            await fight.save();

            // const params = { fight_id: args.fight_id };
            // if (fight.isPhysical()) {
            //     Object.assign(params, {
            //         login: user.login,
            //         level: user.level,
            //         created_at: new Date(),
            //         //todo: add some clan icons if needs
            //     });
            // } else {
            //     if (fight.isGroup()) {
            //         Object.assign(params, {
            //             login: user.login,
            //             level: user.level,
            //             //todo: add some clan icons if needs
            //         });
            //     } else if (fight.isChaotic()) {
            //         Object.assign(params, { team_members_count: fight.getTeamMembersCount() });
            //     }
            // }
            pubSub.publish('fightJoined', fight);
            return fight;
        },
        async refuseFight({ user }, args) {
            const fight = await documents.Fight.findById(args.fight_id).populate('created_by');

            if (!fight) {
                throw new FightNotFoundError('Fight not found.');
            }

            if (!fight.inProgress()) {
                throw new FightStatusError('Fight is not in progress.');
            }

            if (!fight.isPhysical()) {
                throw new FightTypeError("Invalid fight data structure. Can't create a request to right team.");
            }

            if (!fight.getWaitingForApproveTeamMember(user.login)) {
                throw new FightUserNotExistError('Invalid login.');
            }

            if (!fight.isSameLocation(user.location)) {
                throw new FightUserDiffLocationError(
                    'Cannot refuse to the joined fight, user located on other location.',
                );
            }

            await fight.kickTeamMember(user.login);
            pubSub.publish('fightRefused', fight);
            return fight;
        },
        async respondFight({ user }, args) {
            const fight = await documents.Fight.findById(args.fight_id).populate('created_by');

            if (!fight) {
                throw new FightNotFoundError('Fight not found.');
            }

            if (!fight.isCreator(user)) {
                throw new FightError('Cannot approve or decline the fight request, you are not the host of the fight.');
            }

            if (!fight.inProgress()) {
                throw new FightStatusError('Fight is not in progress.');
            }

            if (!fight.isPhysical()) {
                throw new FightTypeError('Fight is not physical.');
            }

            if (!fight.getWaitingForApproveTeamMember(args.login)) {
                throw new FightUserNotExistError('Invalid login.');
            }

            if (args.approve) {
                await fight
                    .markAdversaryAsApproved(args.login)
                    .markInBattle()
                    .save();
                pubSub.publish('battleStarted', { fight_id: args.fight_id, members: fight.getAllTeamLoginList() });
            } else {
                await fight.kickTeamMember(args.login);
                pubSub.publish('fightRefused', fight);
            }

            return fight;
        },
    },
};
