'use strict';

import { pubSub } from '../../../_shared/pub-sub';
import { withFilter } from 'graphql-subscriptions';

export default {
    Subscription: {
        fightCreated: {
            subscribe: withFilter(
                () => pubSub.asyncIterator('fightCreated'),
                (fight, { fight_type, location }) =>
                    fight.fight_type === fight_type && String(fight.location._id) === String(location),
            ),
            resolve: payload => payload,
        },
        fightDeclined: {
            subscribe: withFilter(
                () => pubSub.asyncIterator('fightDeclined'),
                (fight, { fight_type, location }) =>
                    String(fight.location) === String(location) && (!fight_type || fight_type === fight.fight_type),
            ),
            resolve: payload => payload,
        },
        fightJoined: {
            subscribe: withFilter(
                () => pubSub.asyncIterator('fightJoined'),
                (fight, { fight_type, location }) =>
                    String(fight.location) === String(location) && (!fight_type || fight_type === fight.fight_type),
            ),
            resolve: payload => payload,
        },
        fightRefused: {
            subscribe: withFilter(
                () => pubSub.asyncIterator('fightRefused'),
                (fight, { fight_type, location }) =>
                    String(fight.location) === String(location) && (!fight_type || fight_type === fight.fight_type),
            ),
            resolve: payload => payload,
        },
        fightRejected: {
            subscribe: withFilter(
                () => pubSub.asyncIterator('fightRejected'),
                (fight, { fight_type, location }) =>
                    String(fight.location) === String(location) && (!fight_type || fight_type === fight.fight_type),
            ),
            resolve: payload => payload,
        },
    },
};
