'use strict';

export class ApplicationError extends Error {
    public parents: Error[];

    /**
     * @param {String=} message
     * @param {String|Number} [code]
     * @param {Error} [parent] The previous error used for the error chaining.
     */
    constructor(public message: string, public code?: number | string, public parent?: Error) {
        super(message);
        this.name = this.constructor.name;
        this.code = code || this.name;

        this.stack = (new Error() as any).stack;
        // todo: some issues with expandParent() and GraphQL subscriptions, fix that!
        // this.expandParent();
    }

    public expandParent() {
        let parentStack = [];

        if (this.parent instanceof Error) {
            parentStack.push({
                name: this.parent.name,
                message: this.parent.message,
                stack: this.parent.stack,
            });
        }
        if (this.parent instanceof ApplicationError) {
            parentStack = parentStack.concat(this.parent.expandParent());
        }

        this.parents = parentStack;
        return parentStack;
    }
}

/**
 * HttpError represents an error caused by an improper request of the end-user.
 *
 * HttpError can be differentiated via its [[status]] property value which
 * keeps a standard HTTP status code (e.g. 404, 500). Error handlers may use this status code
 * to decide how to format the error page.
 *
 * Throwing an HttpError like in the following example will result in the 404 page to be displayed.
 *
 * ```js
 * if (item === null) { // item does not exist
 *     throw new HttpError(404, 'The requested Item could not be found.');
 * }
 * ```
 */
export class HttpError extends ApplicationError {
    /**
     * @param {Number} status
     * @param {String} message
     * @param {String|Number} [code]
     * @param {Error} [parent]
     */
    constructor(public status: number, message: string, code: number | string, parent?: Error) {
        super(message, code, parent);
    }
}

/**
 * NotFoundHttpError represents a "Not Found" HTTP error with status code 404.
 *
 * @link https://www.w3.org/Protocols/rfc2616/rfc2616-sec10.html#sec10.4.5
 */
export class NotFoundHttpError extends HttpError {
    constructor(message: string, code?: string | number, parent?: Error) {
        super(404, message, code, parent);
    }
}

/**
 * ForbiddenHttpError represents a "Forbidden" HTTP error with status code 403.
 *
 * Use this error when a user has been authenticated but is not allowed to
 * perform the requested action. If the user is not authenticated, consider
 * using a 401 [[UnauthorizedHttpError]]. If you do not want to
 * expose authorization information to the user, it is valid to respond with a
 * 404 [[NotFoundHttpError]].
 *
 * @link http://www.w3.org/Protocols/rfc2616/rfc2616-sec10.html#sec10.4.4
 */
export class ForbiddenHttpError extends HttpError {
    constructor(message: string, code?: string | number, parent?: Error) {
        super(403, message, code, parent);
    }
}

/**
 * BadRequestHttpError represents a "Bad Request" HTTP error with status code 400.
 *
 * Use this error to represent a generic client error. In many cases, there
 * may be an HTTP error that more precisely describes the error. In that
 * case, consider using the more precise error to provide the user with
 * additional information.
 *
 * @link http://www.w3.org/Protocols/rfc2616/rfc2616-sec10.html#sec10.4.1
 */
export class BadRequestHttpError extends HttpError {
    constructor(message: string, code?: string | number, parent?: Error) {
        super(400, message, code, parent);
    }
}

/**
 * ConflictHttpError represents a "Conflict" HTTP error with status code 409
 *
 * @link http://www.w3.org/Protocols/rfc2616/rfc2616-sec10.html#sec10.4.10
 */
export class ConflictHttpError extends HttpError {
    constructor(message: string, code?: string | number, parent?: Error) {
        super(409, message, code, parent);
    }
}

/**
 * MethodNotAllowedHttpError represents a "Method Not Allowed" HTTP error with status code 405.
 *
 * @link http://www.w3.org/Protocols/rfc2616/rfc2616-sec10.html#sec10.4.6
 */
export class MethodNotAllowedHttpError extends HttpError {
    constructor(message: string, code?: string | number, parent?: Error) {
        super(405, message, code, parent);
    }
}

/**
 * GoneHttpError represents a "Gone" HTTP error with status code 410
 *
 * Throw a GoneHttpError when a user requests a resource that no longer exists
 * at the requested url. For example, after a record is deleted, future requests
 * for that record should return a 410 GoneHttpError instead of a 404
 * [[NotFoundHttpError]].
 *
 * @link http://www.w3.org/Protocols/rfc2616/rfc2616-sec10.html#sec10.4.11
 */
export class GoneHttpError extends HttpError {
    constructor(message: string, code?: string | number, parent?: Error) {
        super(410, message, code, parent);
    }
}

/**
 * NotAcceptableHttpError represents a "Not Acceptable" HTTP error with status code 406
 *
 * Use this error when the client requests a Content-Type that your
 * application cannot return. Note that, according to the HTTP 1.1 specification,
 * you are not required to respond with this status code in this situation.
 *
 * @link http://www.w3.org/Protocols/rfc2616/rfc2616-sec10.html#sec10.4.7
 */
export class NotAcceptableHttpError extends HttpError {
    constructor(message: string, code?: string | number, parent?: Error) {
        super(406, message, code, parent);
    }
}

/**
 * ServerErrorHttpError represents an "Internal Server Error" HTTP error with status code 500.
 *
 * @link http://www.w3.org/Protocols/rfc2616/rfc2616-sec10.html#sec10.5.1
 */
export class ServerErrorHttpError extends HttpError {
    constructor(message: string, code?: string | number, parent?: Error) {
        super(500, message, code, parent);
    }
}

/**
 * UnauthorizedHttpError represents an "Unauthorized" HTTP error with status code 401
 *
 * Use this error to indicate that a client needs to authenticate or login
 * to perform the requested action. If the client is already authenticated and
 * is simply not allowed to perform the action, consider using a 403
 * [[ForbiddenHttpError]] or 404 [[NotFoundHttpError]] instead.
 *
 * @link http://www.w3.org/Protocols/rfc2616/rfc2616-sec10.html#sec10.4.2
 */
export class UnauthorizedHttpError extends HttpError {
    constructor(message: string, code?: string | number, parent?: Error) {
        super(401, message, code, parent);
    }
}

/**
 * UnprocessableEntityHttpError represents an "Unprocessable Entity" HTTP
 * error with status code 422.
 *
 * Use this error to inform that the server understands the content type of
 * the request entity and the syntax of that request entity is correct but the server
 * was unable to process the contained instructions. For example, to return form
 * validation errors.
 *
 * @link http://www.webdav.org/specs/rfc2518.html#STATUS_422
 */
export class UnprocessableEntityHttpError extends HttpError {
    constructor(message: string, code?: string | number, parent?: Error) {
        super(422, message, code, parent);
    }
}

/**
 * TooManyRequestsHttpError represents a "Too Many Requests" HTTP error with status code 429
 *
 * Use this error to indicate that a client has made too many requests in a
 * given period of time. For example, you would throw this error when
 * 'throttling' an API user.
 *
 * @link http://tools.ietf.org/search/rfc6585#section-4
 */
export class TooManyRequestsHttpError extends HttpError {
    constructor(message: string, code?: string | number, parent?: Error) {
        super(429, message, code, parent);
    }
}

/**
 * UnsupportedMediaTypeHttpError represents an "Unsupported Media Type" HTTP error with status code 415
 *
 * Use this error when the client sends data in a format that your
 * application does not understand. For example, you would throw this error
 * if the client POSTs XML data to an action or controller that only accepts
 * JSON.
 *
 * @link http://www.w3.org/Protocols/rfc2616/rfc2616-sec10.html#sec10.4.16
 */
export class UnsupportedMediaTypeHttpError extends HttpError {
    constructor(message: string, code?: string | number, parent?: Error) {
        super(415, message, code, parent);
    }
}

/**
 * NotImplementedHttpError represents an "Not Implemented" HTTP error with status code 501
 *
 * @link http://www.w3.org/Protocols/rfc2616/rfc2616-sec10.html#sec10.5.2
 */
export class NotImplementedHttpError extends HttpError {
    constructor(message: string, code?: string | number, parent?: Error) {
        super(501, message, code, parent);
    }
}
