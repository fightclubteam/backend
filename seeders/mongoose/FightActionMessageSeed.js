'use strict';

module.exports = [
    {
        "battle_type": "FISTFUL",
        "body_part": "ABDOMEN",
        "action": "ATTACK",
        "message": "_DEFENDER_ замешкался, и за это храбрый _ATTACKER_, улыбаясь, саданул укол грудью по печени."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "ABDOMEN",
        "action": "ATTACK",
        "message": "_DEFENDER_ забылся, и тут бесчувственный _ATTACKER_, разбежавшись, рубанул ногой по левой почке."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "ABDOMEN",
        "action": "ATTACK",
        "message": "_DEFENDER_ думал не о том, и расстроенный _ATTACKER_ ударил ногой в живот."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "ABDOMEN",
        "action": "ATTACK",
        "message": "_DEFENDER_ поперхнулся, но вдруг обезумевший _ATTACKER_ сдуру вмазал лбом в живот."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "ABDOMEN",
        "action": "ATTACK",
        "message": "_DEFENDER_ растерялся, как вдруг расстроенный _ATTACKER_ сдуру вмазал кулаком по печени."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "ABDOMEN",
        "action": "ATTACK",
        "message": "_DEFENDER_ засмотрелся на печени, а в это время расстроенный _ATTACKER_ сдуру вмазал ребром руки по печени."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "ABDOMEN",
        "action": "ATTACK",
        "message": "_DEFENDER_ думал не о том, и мужественный _ATTACKER_ ударил лбом в пупок."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "ABDOMEN",
        "action": "ATTACK",
        "message": "_DEFENDER_ пытался что-то сказать но вдруг, неожиданно _ATTACKER_ отчаянно проткнул кулаком в пупок."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "ABDOMEN",
        "action": "ATTACK",
        "message": "_DEFENDER_ высморкался, и в это время хитрый _ATTACKER_ нехотя уколол коленом в живот."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "ABDOMEN",
        "action": "ATTACK",
        "message": "_DEFENDER_ думал не о том, и продвинутый _ATTACKER_, улыбаясь, саданул укол коленом в пупок."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "ABDOMEN",
        "action": "ATTACK",
        "message": "_DEFENDER_ растерялся, как вдруг храбрый _ATTACKER_ не подумав, рубанул ногой по правой почке."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "ABDOMEN",
        "action": "ATTACK",
        "message": "_DEFENDER_ замешкался, и за это бесчувственный _ATTACKER_ приложил удар коленом по левой почке."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "ABDOMEN",
        "action": "ATTACK",
        "message": "_DEFENDER_ ковырялся в зубах, и тут _ATTACKER_ отчаянно проткнул коленом в пупок."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "ABDOMEN",
        "action": "ATTACK",
        "message": "_DEFENDER_ забылся, и тут храбрый _ATTACKER_ ударил лбом по левой почке."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "ABDOMEN",
        "action": "ATTACK",
        "message": "_DEFENDER_ замешкался, и за это неустрашимый _ATTACKER_ нехотя уколол правой ногой в живот."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "ABDOMEN",
        "action": "ATTACK",
        "message": "_DEFENDER_ обернулся, как внезапно жестокий _ATTACKER_, улыбаясь, саданул укол коленом в пупок."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "ABDOMEN",
        "action": "ATTACK",
        "message": "_DEFENDER_ обернулся, как внезапно злобный _ATTACKER_ отчаянно проткнул коленом по печени."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "ABDOMEN",
        "action": "ATTACK",
        "message": "_DEFENDER_ забылся, и тут бесчувственный _ATTACKER_ сдуру вмазал кулаком в живот."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "ABDOMEN",
        "action": "ATTACK",
        "message": "_DEFENDER_ замешкался, и за это мужественный _ATTACKER_ сдуру вмазал лбом по левой почке."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "ABDOMEN",
        "action": "ATTACK",
        "message": "_DEFENDER_ растерялся, как вдруг мужественный _ATTACKER_ сдуру вмазал ребром руки по правой почке."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "ABDOMEN",
        "action": "ATTACK",
        "message": "_DEFENDER_ высморкался, и в это время наглый _ATTACKER_ сдуру вмазал правой ногой по левой почке."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "ABDOMEN",
        "action": "ATTACK",
        "message": "_DEFENDER_ думал не о том, и обезумевший _ATTACKER_ ударил грудью в пупок."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "ABDOMEN",
        "action": "ATTACK",
        "message": "_DEFENDER_ ковырялся в зубах, и тут мужественный _ATTACKER_ не подумав, рубанул левой ногой в пупок."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "ABDOMEN",
        "action": "ATTACK",
        "message": "_DEFENDER_ замешкался, и за это мужественный _ATTACKER_ отчаянно проткнул правой ногой по правой почке."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "ABDOMEN",
        "action": "ATTACK",
        "message": "_DEFENDER_ ковырялся в зубах, и тут злобный _ATTACKER_, улыбаясь, саданул укол правой ногой в пупок."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "ABDOMEN",
        "action": "ATTACK",
        "message": "_DEFENDER_ пришел в себя, но в это время наглый _ATTACKER_ приложил удар ногой по печени."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "ABDOMEN",
        "action": "ATTACK",
        "message": "_DEFENDER_ растерялся, как вдруг продвинутый _ATTACKER_ нехотя уколол коленом в живот."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "ABDOMEN",
        "action": "ATTACK",
        "message": "_DEFENDER_ думал не о том, и обезумевший _ATTACKER_, улыбаясь, саданул укол левой ногой в живот."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "ABDOMEN",
        "action": "ATTACK",
        "message": "_DEFENDER_ растерялся, как вдруг разъяренный _ATTACKER_ сдуру вмазал ногой по правой почке."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "ABDOMEN",
        "action": "ATTACK",
        "message": "_DEFENDER_ пришел в себя, но в это время продвинутый _ATTACKER_ нехотя уколол ногой в живот."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "ABDOMEN",
        "action": "ATTACK",
        "message": "_DEFENDER_ высморкался, и в это время мужественный _ATTACKER_ ударил ребром руки по левой почке."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "ABDOMEN",
        "action": "ATTACK",
        "message": "_DEFENDER_ обернулся, как внезапно мужественный _ATTACKER_ отчаянно проткнул грудью по левой почке."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "ABDOMEN",
        "action": "ATTACK",
        "message": "_DEFENDER_ ковырялся в зубах, и тут мужественный _ATTACKER_ ударил кулаком в живот."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "ABDOMEN",
        "action": "ATTACK",
        "message": "_DEFENDER_ растерялся, как вдруг жестокий _ATTACKER_, разбежавшись, рубанул ребром руки в живот."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "ABDOMEN",
        "action": "ATTACK",
        "message": "_DEFENDER_ высморкался, и в это время наглый _ATTACKER_ сдуру вмазал левой ногой по печени."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "ABDOMEN",
        "action": "ATTACK",
        "message": "_DEFENDER_ поперхнулся, но вдруг _ATTACKER_ не подумав, рубанул кулаком в живот."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "ABDOMEN",
        "action": "ATTACK",
        "message": "_DEFENDER_ поперхнулся, но вдруг неустрашимый _ATTACKER_ приложил удар кулаком по правой почке."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "ABDOMEN",
        "action": "ATTACK",
        "message": "_DEFENDER_ пытался что-то сказать но вдруг, неожиданно хитрый _ATTACKER_, разбежавшись, рубанул лбом в пупок."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "ABDOMEN",
        "action": "ATTACK",
        "message": "_DEFENDER_ засмотрелся на печени, а в это время _ATTACKER_ нехотя уколол лбом по левой почке."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "ABDOMEN",
        "action": "ATTACK",
        "message": "_DEFENDER_ пришел в себя, но в это время разъяренный _ATTACKER_ отчаянно проткнул ребром руки по левой почке."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "ABDOMEN",
        "action": "BLOCK",
        "message": "_ATTACKER_ подскользнулся, и _DEFENDER_ остановил удар ногой по левой почке."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "ABDOMEN",
        "action": "BLOCK",
        "message": "_ATTACKER_ потерял самоконтроль, вследствие чего мужественный _DEFENDER_ остановил удар грудью в пупок."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "ABDOMEN",
        "action": "BLOCK",
        "message": "_ATTACKER_ закашлялся, и разъяренный _DEFENDER_ остановил удар коленом по левой почке."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "ABDOMEN",
        "action": "BLOCK",
        "message": "_ATTACKER_ пытался провести удар, но _DEFENDER_ остановил удар лбом в живот."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "ABDOMEN",
        "action": "BLOCK",
        "message": "_ATTACKER_ думал не о том, и обезумевший _DEFENDER_ остановил удар правой ногой в живот."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "ABDOMEN",
        "action": "BLOCK",
        "message": "_ATTACKER_ подскользнулся, и _DEFENDER_ отбил удар ногой по правой почке."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "ABDOMEN",
        "action": "BLOCK",
        "message": "_ATTACKER_ закашлялся, и расстроенный _DEFENDER_ заблокировал удар левой ногой в печень."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "ABDOMEN",
        "action": "BLOCK",
        "message": "_ATTACKER_ потерял самоконтроль, вследствие чего расстроенный _DEFENDER_ заблокировал удар левой ногой в пупок."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "ABDOMEN",
        "action": "BLOCK",
        "message": "_ATTACKER_ потерял самоконтроль, вследствие чего наглый _DEFENDER_ заблокировал удар ребром руки по левой почке."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "ABDOMEN",
        "action": "BLOCK",
        "message": "_ATTACKER_ потерял самоконтроль, вследствие чего злобный _DEFENDER_ заблокировал удар правой ногой по правой почке."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "ABDOMEN",
        "action": "BLOCK",
        "message": "_ATTACKER_ подскользнулся, и жестокий _DEFENDER_ заблокировал удар коленом по правой почке."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "ABDOMEN",
        "action": "BLOCK",
        "message": "_ATTACKER_ старался провести удар, но злобный _DEFENDER_ отбил удар правой ногой в печень."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "ABDOMEN",
        "action": "BLOCK",
        "message": "_ATTACKER_ пытался провести удар, но расстроенный _DEFENDER_ остановил удар ногой в пупок."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "ABDOMEN",
        "action": "BLOCK",
        "message": "_ATTACKER_ думал о живот, вследствие чего _DEFENDER_ остановил удар левой ногой по правой почке."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "ABDOMEN",
        "action": "BLOCK",
        "message": "_ATTACKER_ закашлялся, и расстроенный _DEFENDER_ отбил удар лбом по левой почке."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "ABDOMEN",
        "action": "BLOCK",
        "message": "_ATTACKER_ старался провести удар, но неустрашимый _DEFENDER_ отбил удар кулаком по правой почке."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "ABDOMEN",
        "action": "BLOCK",
        "message": "_ATTACKER_ подскользнулся, и _DEFENDER_ остановил удар грудью в живот."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "ABDOMEN",
        "action": "BLOCK",
        "message": "_ATTACKER_ пытался нанести удар, но мужественный _DEFENDER_ отбил удар левой ногой в печень."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "ABDOMEN",
        "action": "BLOCK",
        "message": "_ATTACKER_ думал о живот, вследствие чего продвинутый _DEFENDER_ остановил удар кулаком в пупок."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "ABDOMEN",
        "action": "BLOCK",
        "message": "_ATTACKER_ пытался нанести удар, но _DEFENDER_ отбил удар кулаком по левой почке."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "ABDOMEN",
        "action": "BLOCK",
        "message": "_ATTACKER_ думал о живот, вследствие чего расстроенный _DEFENDER_ остановил удар левой ногой в пупок."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "ABDOMEN",
        "action": "BLOCK",
        "message": "_ATTACKER_ закашлялся, и _DEFENDER_ отбил удар кулаком в пупок."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "ABDOMEN",
        "action": "BLOCK",
        "message": "_ATTACKER_ подскользнулся, и наглый _DEFENDER_ отбил удар правой ногой в печень."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "ABDOMEN",
        "action": "BLOCK",
        "message": "_ATTACKER_ думал не о том, и наглый _DEFENDER_ остановил удар кулаком в живот."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "ABDOMEN",
        "action": "BLOCK",
        "message": "_ATTACKER_ закашлялся, и наглый _DEFENDER_ остановил удар грудью в печень."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "ABDOMEN",
        "action": "BLOCK",
        "message": "_ATTACKER_ думал о живот, вследствие чего наглый _DEFENDER_ заблокировал удар правой ногой по правой почке."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "ABDOMEN",
        "action": "BLOCK",
        "message": "_ATTACKER_ думал не о том, и разъяренный _DEFENDER_ остановил удар кулаком по левой почке."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "ABDOMEN",
        "action": "BLOCK",
        "message": "_ATTACKER_ подскользнулся, и _DEFENDER_ остановил удар правой ногой по левой почке."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "ABDOMEN",
        "action": "BLOCK",
        "message": "_ATTACKER_ пытался нанести удар, но храбрый _DEFENDER_ отбил удар грудью по правой почке."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "ABDOMEN",
        "action": "BLOCK",
        "message": "_ATTACKER_ пытался нанести удар, но жестокий _DEFENDER_ заблокировал удар грудью в печень."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "ABDOMEN",
        "action": "BREAK_THROUGH_BLOCK",
        "message": "_DEFENDER_ обернулся, как внезапно продвинутый _ATTACKER_, напугав всех, неслышно подойдя сзади ударил, пробив блок, булыжником в живот."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "ABDOMEN",
        "action": "BREAK_THROUGH_BLOCK",
        "message": "_DEFENDER_ думал не о том, и расстроенный _ATTACKER_, пробив блок, наступил на ногу врага, ударил по печени."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "ABDOMEN",
        "action": "BREAK_THROUGH_BLOCK",
        "message": "_DEFENDER_ пришел в себя, но в это время жестокий _ATTACKER_, пробив блок, ласково заломил руку за спину соперника, ударил по левой почке."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "ABDOMEN",
        "action": "BREAK_THROUGH_BLOCK",
        "message": "_DEFENDER_ засмотрелся на &lt;вырезано цензурой&gt;, а в это время неустрашимый _ATTACKER_, пробив блок, наступил на ногу врага, ударил в живот."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "ABDOMEN",
        "action": "BREAK_THROUGH_BLOCK",
        "message": "_DEFENDER_ высморкался, и в это время _ATTACKER_, напугав всех, неслышно подойдя сзади ударил, пробив блок, булыжником по правой почке."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "ABDOMEN",
        "action": "BREAK_THROUGH_BLOCK",
        "message": "_DEFENDER_ растерялся, как вдруг бесчувственный _ATTACKER_, пробив блок, наступил на ногу врага, ударил в пупок."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "ABDOMEN",
        "action": "BREAK_THROUGH_BLOCK",
        "message": "_DEFENDER_ поперхнулся, но вдруг _ATTACKER_, пробив блок, провел ужасный бросок через пупок оппонента, ударил по правой почке."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "ABDOMEN",
        "action": "BREAK_THROUGH_BLOCK",
        "message": "_DEFENDER_ думал не о том, и расстроенный _ATTACKER_, пробив блок, наступил на ногу врага, ударил по правой почке."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "ABDOMEN",
        "action": "BREAK_THROUGH_BLOCK",
        "message": "_DEFENDER_ пытался что-то сказать но вдруг, неожиданно расстроенный _ATTACKER_, пробив блок, провел ужасный бросок через пупок оппонента, ударил по левой почке."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "ABDOMEN",
        "action": "BREAK_THROUGH_BLOCK",
        "message": "_DEFENDER_ засмотрелся на &lt;вырезано цензурой&gt;, а в это время _ATTACKER_, пробив блок, ласково заломил руку за спину соперника, ударил по левой почке."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "ABDOMEN",
        "action": "BREAK_THROUGH_BLOCK",
        "message": "_DEFENDER_ высморкался, и в это время _ATTACKER_, пробив блок, провел ужасный бросок через пупок оппонента, ударила по печени."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "ABDOMEN",
        "action": "BREAK_THROUGH_BLOCK",
        "message": "_DEFENDER_ замешкался, и за это мужественный _ATTACKER_, напугав всех, неслышно подойдя сзади ударил, пробив блок, булыжником в пупок."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "ABDOMEN",
        "action": "BREAK_THROUGH_BLOCK",
        "message": "_DEFENDER_ поперхнулся, но вдруг _ATTACKER_, пробив блок, наступил на ногу врага, ударил по печени."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "ABDOMEN",
        "action": "BREAK_THROUGH_BLOCK",
        "message": "_DEFENDER_ замешкался, и за это _ATTACKER_, пробив блок, ласково заломил руку за спину соперника, ударил по правой почке."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "ABDOMEN",
        "action": "BREAK_THROUGH_BLOCK",
        "message": "_DEFENDER_ замешкался, и за это неустрашимый _ATTACKER_, пробив блок, провел ужасный бросок через пупок оппонента, ударил в живот."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "ABDOMEN",
        "action": "BREAK_THROUGH_BLOCK",
        "message": "_DEFENDER_ забылся, и тут _ATTACKER_, напугав всех, неслышно подойдя сзади ударил, пробив блок, булыжником по печени."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "ABDOMEN",
        "action": "BREAK_THROUGH_BLOCK",
        "message": "_DEFENDER_ растерялся, как вдруг хитрый _ATTACKER_, пробив блок, укусил в нос противника, ударил по правой почке."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "ABDOMEN",
        "action": "CRITICAL",
        "message": "_DEFENDER_ обернулся, как внезапно хитрый _ATTACKER_, напугав всех, неслышно подойдя сзади ударил булыжником оппонента в живот."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "ABDOMEN",
        "action": "CRITICAL",
        "message": "_DEFENDER_ обернулся, как внезапно хитрый _ATTACKER_, проклиная этот сайт, провел ужасный бросок через пупок оппонента, попал в живот."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "ABDOMEN",
        "action": "CRITICAL",
        "message": "_DEFENDER_ замешкался, и за это храбрый _ATTACKER_, напугав всех, укусил в нос противника и ударил по правой почке."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "ABDOMEN",
        "action": "CRITICAL",
        "message": "_DEFENDER_ пытался что-то сказать но вдруг, неожиданно бесчувственный _ATTACKER_, напугав всех, неслышно подойдя сзади ударил булыжником оппонента по левой почке."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "ABDOMEN",
        "action": "CRITICAL",
        "message": "_DEFENDER_ растерялся, как вдруг _ATTACKER_, напугав всех, неслышно подойдя сзади ударил булыжником оппонента в живот."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "ABDOMEN",
        "action": "CRITICAL",
        "message": "_DEFENDER_ думал не о том, и неустрашимый _ATTACKER_, напугав всех, неслышно подойдя сзади ударил булыжником оппонента в живот."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "ABDOMEN",
        "action": "CRITICAL",
        "message": "_DEFENDER_ пытался что-то сказать но вдруг, неожиданно _ATTACKER_, сказав &quot;БУ!&quot;, ласково заломил руку за спину соперника, ударил в пупок."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "ABDOMEN",
        "action": "CRITICAL",
        "message": "_DEFENDER_ обернулся, как внезапно _ATTACKER_, напугав всех, укусил в нос противника и ударил в живот."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "ABDOMEN",
        "action": "CRITICAL",
        "message": "_DEFENDER_ засмотрелся на &lt;вырезано цензурой&gt;, а в это время мужественный _ATTACKER_, напугав всех, неслышно подойдя сзади ударил булыжником оппонента по печени."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "ABDOMEN",
        "action": "CRITICAL",
        "message": "_DEFENDER_ забылся, и тут _ATTACKER_, сказав &quot;БУ!&quot;, ласково заломил руку за спину соперника, ударил по печени."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "ABDOMEN",
        "action": "CRITICAL",
        "message": "_DEFENDER_ думал не о том, и храбрый _ATTACKER_, напугав всех, неслышно подойдя сзади ударил булыжником оппонента по правой почке."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "ABDOMEN",
        "action": "CRITICAL",
        "message": "_DEFENDER_ пытался что-то сказать но вдруг, неожиданно расстроенный _ATTACKER_, напугав всех, укусил в нос противника и ударил в пупок."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "ABDOMEN",
        "action": "CRITICAL",
        "message": "_DEFENDER_ ковырялся в зубах, и тут неустрашимый _ATTACKER_, сказав &quot;БУ!&quot;, ласково заломил руку за спину соперника, ударил по правой почке."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "ABDOMEN",
        "action": "CRITICAL",
        "message": "_DEFENDER_ пришел в себя, но в это время _ATTACKER_, сказав &quot;БУ!&quot;, ласково заломил руку за спину соперника, ударил по левой почке."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "ABDOMEN",
        "action": "CRITICAL",
        "message": "_DEFENDER_ растерялся, как вдруг жестокий _ATTACKER_, наступил на ногу врага, ударил по печени."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "ABDOMEN",
        "action": "CRITICAL",
        "message": "_DEFENDER_ пытался что-то сказать но вдруг, неожиданно наглый _ATTACKER_, наступил на ногу врага, ударил по правой почке."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "ABDOMEN",
        "action": "DODGE",
        "message": "_ATTACKER_ поскользнулся, и жестокий _DEFENDER_, увернулся от удара коленом в живот."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "ABDOMEN",
        "action": "DODGE",
        "message": "_ATTACKER_ пытался нанести удар, но расстроенный _DEFENDER_, увернулся от удара левой пяткой в пупок."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "ABDOMEN",
        "action": "DODGE",
        "message": "_ATTACKER_ поскользнулся, и мужественный _DEFENDER_, отскочил от удара лбом по животу."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "ABDOMEN",
        "action": "DODGE",
        "message": "_ATTACKER_ потерял самоконтроль, вследствие чего _DEFENDER_, уклонился от удара кулаком по печени."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "ABDOMEN",
        "action": "DODGE",
        "message": "_ATTACKER_ пытался нанести удар, но мужественный _DEFENDER_, отскочил от удара ребром руки в живот."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "ABDOMEN",
        "action": "DODGE",
        "message": "_ATTACKER_ потерял самоконтроль, вследствие чего наглый _DEFENDER_, увернулся от удара коленом в живот."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "ABDOMEN",
        "action": "DODGE",
        "message": "_ATTACKER_ пытался провести удар, но храбрый _DEFENDER_, уклонился от удара пяткой в живот."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "ABDOMEN",
        "action": "DODGE",
        "message": "_ATTACKER_ думал не о том, и продвинутый _DEFENDER_, уклонился от удара ногой в живот."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "ABDOMEN",
        "action": "DODGE",
        "message": "_ATTACKER_ старался провести удар, но _DEFENDER_, уклонился от удара правой пяткой по левой почке."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "ABDOMEN",
        "action": "DODGE",
        "message": "_ATTACKER_ старался провести удар, но обезумевший _DEFENDER_, уклонился от удара коленом по левой почке."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "ABDOMEN",
        "action": "DODGE",
        "message": "_ATTACKER_ думал о &lt;вырезано цензурой&gt;, вследствие чего разъяренный _DEFENDER_, увернулся от удара ребром руки по печени."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "ABDOMEN",
        "action": "DODGE",
        "message": "_ATTACKER_ потерял самоконтроль, вследствие чего разъяренный _DEFENDER_, отскочил от удара грудью в живот."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "ABDOMEN",
        "action": "DODGE",
        "message": "_ATTACKER_ закашлялся, и _DEFENDER_, увернулся от удара кулаком по правой почке."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "ABDOMEN",
        "action": "DODGE",
        "message": "_ATTACKER_ закашлялся, и мужественный _DEFENDER_, отскочил от удара копчиком в пупок."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "ABDOMEN",
        "action": "DODGE",
        "message": "_ATTACKER_ думал не о том, и злобный _DEFENDER_, уклонился от удара левой ногой в живот."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "ABDOMEN",
        "action": "DODGE",
        "message": "_ATTACKER_ старался провести удар, но _DEFENDER_, отскочил от удара коленом по печени."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "ABDOMEN",
        "action": "DODGE",
        "message": "_ATTACKER_ пытался провести удар, но _DEFENDER_, отпрыгнул от удара левой пятки в пупок."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "ABDOMEN",
        "action": "DODGE",
        "message": "_ATTACKER_ пытался нанести удар, но _DEFENDER_, уклонился от удара ребром руки по животу."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "ABDOMEN",
        "action": "DODGE",
        "message": "_ATTACKER_ поскользнулся, и _DEFENDER_, отпрыгнул от удара грудью в пупок."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "ABDOMEN",
        "action": "DODGE",
        "message": "_ATTACKER_ думал не о том, и бесчувственный _DEFENDER_, увернулся от удара правой ногой в печень."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "ABDOMEN",
        "action": "DODGE",
        "message": "_ATTACKER_ старался провести удар, но бесчувственный _DEFENDER_, отпрыгнул от удара лбом в пупок."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "ABDOMEN",
        "action": "DODGE",
        "message": "_ATTACKER_ старался провести удар, но продвинутый _DEFENDER_, увернулся от удара ногой  по правой почке."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "CHEST",
        "action": "CRITICAL",
        "message": "_DEFENDER_ засмотрелся на &lt;вырезано цензурой&gt;, а в это время неустрашимый _ATTACKER_, расслабившись, расцарапал нос соперника и ударил в корпус."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "CHEST",
        "action": "CRITICAL",
        "message": "_DEFENDER_ пытался что-то сказать но вдруг, неожиданно хитрый _ATTACKER_, расслабившись, расцарапал нос соперника и ударил в сердце ."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "CHEST",
        "action": "CRITICAL",
        "message": "_DEFENDER_ замешкался, и за это жестокий _ATTACKER_, проклиная этот сайт, провел ужасный бросок через пупок оппонента, попал в корпус."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "CHEST",
        "action": "CRITICAL",
        "message": "_DEFENDER_ растерялся, как вдруг храбрый _ATTACKER_, расслабившись, расцарапал нос соперника и ударил в грудь."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "CHEST",
        "action": "CRITICAL",
        "message": "_DEFENDER_ пытался что-то сказать но вдруг, неожиданно храбрый _ATTACKER_, показав сразу два пальца, наступил на ногу врага и ударил в солнечное сплетение."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "CHEST",
        "action": "CRITICAL",
        "message": "_DEFENDER_ замешкался, и за это продвинутый _ATTACKER_, напугав всех, неслышно подойдя сзади ударил булыжником оппонента в солнечное сплетение."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "CHEST",
        "action": "CRITICAL",
        "message": "_DEFENDER_ поперхнулся, но вдруг _ATTACKER_, проклиная этот сайт, провел ужасный бросок через пупок оппонента, попал в область лопаток."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "CHEST",
        "action": "CRITICAL",
        "message": "_DEFENDER_ обернулся, как внезапно _ATTACKER_, сказав &quot;БУ!&quot;, ласково заломил руку за спину соперника, ударил в бок."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "CHEST",
        "action": "CRITICAL",
        "message": "_DEFENDER_ поперхнулся, но вдруг наглый _ATTACKER_, показав сразу два пальца, наступил на ногу врага и ударил в область лопаток."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "CHEST",
        "action": "CRITICAL",
        "message": "_DEFENDER_ замешкался, и за это _ATTACKER_, напугав всех, неслышно подойдя сзади ударила булыжником оппонента в солнечное сплетение."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "CHEST",
        "action": "CRITICAL",
        "message": "_DEFENDER_ пытался что-то сказать но вдруг, неожиданно мужественный _ATTACKER_, расслабившись, расцарапал нос соперника и ударил в грудь."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "CHEST",
        "action": "CRITICAL",
        "message": "_DEFENDER_ забылся, и тут _ATTACKER_, сказав &quot;БУ!&quot;, ласково заломил руку за спину соперника, ударил в область лопаток."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "CHEST",
        "action": "CRITICAL",
        "message": "_DEFENDER_ пытался что-то сказать но вдруг, неожиданно храбрый _ATTACKER_, напугав всех, неслышно подойдя сзади ударил булыжником оппонента в корпус."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "CHEST",
        "action": "ATTACK",
        "message": "_DEFENDER_ растерялся, как вдруг мужественный _ATTACKER_, разбежавшись, рубанул левой ногой в бок."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "CHEST",
        "action": "ATTACK",
        "message": "_DEFENDER_ пытался что-то сказать но вдруг, неожиданно обезумевший _ATTACKER_ приложил удар ребром руки в бок."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "CHEST",
        "action": "ATTACK",
        "message": "_DEFENDER_ растерялся, как вдруг мужественный _ATTACKER_, разбежавшись, рубанул лбом в сердце."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "CHEST",
        "action": "ATTACK",
        "message": "_DEFENDER_ пришел в себя, но в это время _ATTACKER_ сдуру вмазал грудью в сердце."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "CHEST",
        "action": "ATTACK",
        "message": "_DEFENDER_ поперхнулся, но вдруг продвинутый _ATTACKER_ приложил удар правой ногой в корпус."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "CHEST",
        "action": "ATTACK",
        "message": "_DEFENDER_ обернулся, как внезапно неустрашимый _ATTACKER_ нехотя уколол грудью в корпус."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "CHEST",
        "action": "ATTACK",
        "message": "_DEFENDER_ растерялся, как вдруг расстроенный _ATTACKER_ отчаянно проткнул грудью по желудку."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "CHEST",
        "action": "ATTACK",
        "message": "_DEFENDER_ обернулся, как внезапно _ATTACKER_, разбежавшись, рубанул правой ногой в бок."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "CHEST",
        "action": "ATTACK",
        "message": "_DEFENDER_ растерялся, как вдруг _ATTACKER_ нехотя уколол грудью в бок."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "CHEST",
        "action": "ATTACK",
        "message": "_DEFENDER_ засмотрелся на &lt;вырезано цензурой&gt;, а в это время хитрый _ATTACKER_ приложил удар ребром руки в область лопаток."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "CHEST",
        "action": "ATTACK",
        "message": "_DEFENDER_ ковырялся в зубах, и тут злобный _ATTACKER_ нехотя уколол левой ногой в сердце."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "CHEST",
        "action": "ATTACK",
        "message": "_DEFENDER_ поперхнулся, но вдруг храбрый _ATTACKER_ отчаянно проткнул левой ногой в солнечное сплетение."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "CHEST",
        "action": "ATTACK",
        "message": "_DEFENDER_ засмотрелся на &lt;вырезано цензурой&gt;, а в это время разъяренный _ATTACKER_ приложил удар кулаком в бок."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "CHEST",
        "action": "ATTACK",
        "message": "_DEFENDER_ замешкался, и за это хитрый _ATTACKER_ отчаянно проткнул левой ногой по желудку."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "CHEST",
        "action": "ATTACK",
        "message": "_DEFENDER_ пытался что-то сказать но вдруг, неожиданно злобный _ATTACKER_ сдуру вмазал левой ногой в корпус."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "CHEST",
        "action": "ATTACK",
        "message": "_DEFENDER_ поперхнулся, но вдруг расстроенный _ATTACKER_, улыбаясь, саданул укол лбом в корпус."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "CHEST",
        "action": "ATTACK",
        "message": "_DEFENDER_ высморкался, и в это время разъяренный _ATTACKER_ сдуру вмазал левой ногой в сердце."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "CHEST",
        "action": "ATTACK",
        "message": "_DEFENDER_ ковырялся в зубах, и тут _ATTACKER_ ударил правой ногой в бок."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "CHEST",
        "action": "ATTACK",
        "message": "_DEFENDER_ думал не о том, и расстроенный _ATTACKER_, разбежавшись, рубанул коленом в грудь."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "CHEST",
        "action": "ATTACK",
        "message": "_DEFENDER_ засмотрелся на &lt;вырезано цензурой&gt;, а в это время разъяренный _ATTACKER_ отчаянно проткнул ребром руки в солнечное сплетение."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "CHEST",
        "action": "ATTACK",
        "message": "_DEFENDER_ замешкался, и за это злобный _ATTACKER_ отчаянно проткнул коленом в солнечное сплетение."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "CHEST",
        "action": "ATTACK",
        "message": "_DEFENDER_ растерялся, как вдруг _ATTACKER_, улыбаясь, саданул укол грудью в грудь."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "CHEST",
        "action": "ATTACK",
        "message": "_DEFENDER_ забылся, и тут _ATTACKER_, улыбаясь, саданул укол кулаком в область лопаток."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "CHEST",
        "action": "ATTACK",
        "message": "_DEFENDER_ поперхнулся, но вдруг неустрашимый _ATTACKER_, разбежавшись, рубанул ребром руки в область лопаток."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "CHEST",
        "action": "ATTACK",
        "message": "_DEFENDER_ пришел в себя, но в это время мужественный _ATTACKER_ приложил удар левой ногой в бок."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "CHEST",
        "action": "ATTACK",
        "message": "_DEFENDER_ пришел в себя, но в это время хитрый _ATTACKER_ приложил удар кулаком в грудь."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "CHEST",
        "action": "ATTACK",
        "message": "_DEFENDER_ поперхнулся, но вдруг мужественный _ATTACKER_ ударил правой ногой в сердце."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "CHEST",
        "action": "ATTACK",
        "message": "_DEFENDER_ обернулся, как внезапно расстроенный _ATTACKER_ не подумав, рубанул грудью в сердце."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "CHEST",
        "action": "ATTACK",
        "message": "_DEFENDER_ обернулся, как внезапно бесчувственный _ATTACKER_ не подумав, рубанул левой ногой по желудку."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "CHEST",
        "action": "ATTACK",
        "message": "_DEFENDER_ обернулся, как внезапно злобный _ATTACKER_, улыбаясь, саданул укол правой ногой в солнечное сплетение."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "CHEST",
        "action": "ATTACK",
        "message": "_DEFENDER_ растерялся, как вдруг наглый _ATTACKER_ отчаянно проткнул левой ногой в солнечное сплетение."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "CHEST",
        "action": "ATTACK",
        "message": "_DEFENDER_ забылся, и тут расстроенный _ATTACKER_, улыбаясь, саданул укол лбом в солнечное сплетение."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "CHEST",
        "action": "ATTACK",
        "message": "_DEFENDER_ растерялся, как вдруг злобный _ATTACKER_ отчаянно проткнул грудью в бок."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "CHEST",
        "action": "BLOCK",
        "message": "_ATTACKER_ закашлялся, и бесчувственный _DEFENDER_ отбил удар правой ногой в солнечное сплетение."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "CHEST",
        "action": "BLOCK",
        "message": "_ATTACKER_ пытался нанести удар, но мужественный _DEFENDER_ заблокировал удар лбом в корпус."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "CHEST",
        "action": "BLOCK",
        "message": "_ATTACKER_ подскользнулся, и расстроенный _DEFENDER_ заблокировал удар ребром руки в сердце."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "CHEST",
        "action": "BLOCK",
        "message": "_ATTACKER_ потерял самоконтроль, вследствие чего мужественный _DEFENDER_ отбил удар грудью в сердце."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "CHEST",
        "action": "BLOCK",
        "message": "_ATTACKER_ думал не о том, и бесчувственный _DEFENDER_ остановил удар левой ногой в область лопаток."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "CHEST",
        "action": "BLOCK",
        "message": "_ATTACKER_ закашлялся, и мужественный _DEFENDER_ отбил удар ребром руки в солнечное сплетение."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "CHEST",
        "action": "BLOCK",
        "message": "_ATTACKER_ потерял самоконтроль, вследствие чего хитрый _DEFENDER_ отбил удар лбом в сердце."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "CHEST",
        "action": "BLOCK",
        "message": "_ATTACKER_ пытался нанести удар, но обезумевший _DEFENDER_ заблокировал удар ребром руки в солнечное сплетение."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "CHEST",
        "action": "BLOCK",
        "message": "_ATTACKER_ пытался провести удар, но _DEFENDER_ заблокировал удар ребром руки в бок."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "CHEST",
        "action": "BLOCK",
        "message": "_ATTACKER_ пытался нанести удар, но мужественный _DEFENDER_ остановил удар лбом в грудь."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "CHEST",
        "action": "BLOCK",
        "message": "_ATTACKER_ думал о &lt;вырезано цензурой&gt;, вследствие чего неустрашимый _DEFENDER_ остановил удар правой ногой по желудку."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "CHEST",
        "action": "BLOCK",
        "message": "_ATTACKER_ пытался нанести удар, но бесчувственный _DEFENDER_ заблокировал удар грудью по желудку."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "CHEST",
        "action": "BLOCK",
        "message": "_ATTACKER_ пытался провести удар, но жестокий _DEFENDER_ остановил удар кулаком в грудь."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "CHEST",
        "action": "BLOCK",
        "message": "_ATTACKER_ пытался провести удар, но _DEFENDER_ остановил удар лбом в солнечное сплетение."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "CHEST",
        "action": "BLOCK",
        "message": "_ATTACKER_ закашлялся, и жестокий _DEFENDER_ отбил удар ногой по желудку."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "CHEST",
        "action": "BLOCK",
        "message": "_ATTACKER_ пытался нанести удар, но неустрашимый _DEFENDER_ заблокировал удар кулаком в корпус."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "CHEST",
        "action": "BLOCK",
        "message": "_ATTACKER_ пытался провести удар, но храбрый _DEFENDER_ остановил удар правой ногой в сердце."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "CHEST",
        "action": "BLOCK",
        "message": "_ATTACKER_ потерял самоконтроль, вследствие чего хитрый _DEFENDER_ отбил удар кулаком в грудь."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "CHEST",
        "action": "BLOCK",
        "message": "_ATTACKER_ пытался провести удар, но _DEFENDER_ заблокировал удар ребром руки в солнечное сплетение."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "CHEST",
        "action": "BLOCK",
        "message": "_ATTACKER_ думал о &lt;вырезано цензурой&gt;, вследствие чего обезумевший _DEFENDER_ заблокировал удар грудью в область лопаток."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "CHEST",
        "action": "BLOCK",
        "message": "_ATTACKER_ пытался провести удар, но _DEFENDER_ остановил удар лбом по желудку."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "CHEST",
        "action": "BLOCK",
        "message": "_ATTACKER_ думал не о том, и мужественный _DEFENDER_ отбил удар ногой в бок."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "CHEST",
        "action": "BLOCK",
        "message": "_ATTACKER_ старался провести удар, но разъяренный _DEFENDER_ отбил удар лбом в солнечное сплетение."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "CHEST",
        "action": "BLOCK",
        "message": "_ATTACKER_ подскользнулся, и продвинутый _DEFENDER_ заблокировал удар кулаком в грудь."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "CHEST",
        "action": "BLOCK",
        "message": "_ATTACKER_ подскользнулся, и расстроенный _DEFENDER_ заблокировал удар лбом в солнечное сплетение."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "CHEST",
        "action": "BLOCK",
        "message": "_ATTACKER_ подскользнулся, и жестокий _DEFENDER_ остановил удар грудью по желудку."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "CHEST",
        "action": "BLOCK",
        "message": "_ATTACKER_ закашлялся, и бесчувственный _DEFENDER_ отбил удар левой ногой в корпус."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "CHEST",
        "action": "BLOCK",
        "message": "_ATTACKER_ потерял самоконтроль, вследствие чего расстроенный _DEFENDER_ отбил удар грудью по желудку."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "CHEST",
        "action": "BLOCK",
        "message": "_ATTACKER_ думал о &lt;вырезано цензурой&gt;, вследствие чего хитрый _DEFENDER_ остановил удар ногой в солнечное сплетение."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "CHEST",
        "action": "BLOCK",
        "message": "_ATTACKER_ закашлялся, и мужественный _DEFENDER_ отбил удар тупым лезвием в корпус."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "CHEST",
        "action": "BLOCK",
        "message": "_ATTACKER_ закашлялся, и хитрый _DEFENDER_ отбил удар лбом в область лопаток."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "CHEST",
        "action": "BLOCK",
        "message": "_ATTACKER_ думал не о том, и продвинутый _DEFENDER_ остановил удар ножнами в пах."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "CHEST",
        "action": "BLOCK",
        "message": "_ATTACKER_ подскользнулся, и наглый _DEFENDER_ заблокировал удар лбом в сердце."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "CHEST",
        "action": "BLOCK",
        "message": "_ATTACKER_ пытался нанести удар, но бесчувственный _DEFENDER_ остановил удар гардой в грудь."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "CHEST",
        "action": "BLOCK",
        "message": "_ATTACKER_ думал о &lt;вырезано цензурой&gt;, вследствие чего злобный _DEFENDER_ остановил удар ребром руки по желудку."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "CHEST",
        "action": "BREAK_THROUGH_BLOCK",
        "message": "_DEFENDER_ пришел в себя, но в это время неустрашимый _ATTACKER_, напугав всех, неслышно подойдя сзади ударил, пробив блок, булыжником по желудку."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "CHEST",
        "action": "BREAK_THROUGH_BLOCK",
        "message": "_DEFENDER_ пытался что-то сказать но вдруг, неожиданно жестокий _ATTACKER_, пробив блок, расцарапал нос соперника, ударил в солнечное сплетение."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "CHEST",
        "action": "BREAK_THROUGH_BLOCK",
        "message": "_DEFENDER_ обернулся, как внезапно мужественный _ATTACKER_, пробив блок, расцарапал нос соперника, ударил в корпус."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "CHEST",
        "action": "BREAK_THROUGH_BLOCK",
        "message": "_DEFENDER_ забылся, и тут неустрашимый _ATTACKER_, пробив блок, наступил на ногу врага, ударил в грудь."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "CHEST",
        "action": "BREAK_THROUGH_BLOCK",
        "message": "_DEFENDER_ высморкался, и в это время хитрый _ATTACKER_, напугав всех, неслышно подойдя сзади ударил, пробив блок, булыжником по желудку."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "CHEST",
        "action": "BREAK_THROUGH_BLOCK",
        "message": "_DEFENDER_ поперхнулся, но вдруг _ATTACKER_, пробив блок, провел ужасный бросок через пупок оппонента, ударил в корпус."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "CHEST",
        "action": "BREAK_THROUGH_BLOCK",
        "message": "_DEFENDER_ пытался что-то сказать но вдруг, неожиданно хитрый _ATTACKER_, пробив блок, провел ужасный бросок через пупок оппонента, ударил в корпус."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "CHEST",
        "action": "BREAK_THROUGH_BLOCK",
        "message": "_DEFENDER_ думал не о том, и мужественный _ATTACKER_, пробив блок, укусил в нос противника, ударил в корпус."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "CHEST",
        "action": "BREAK_THROUGH_BLOCK",
        "message": "_DEFENDER_ засмотрелся на &lt;вырезано цензурой&gt;, а в это время неустрашимый _ATTACKER_, пробив блок,  расслабившись, расцарапал нос соперника и ударил в корпус."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "CHEST",
        "action": "BREAK_THROUGH_BLOCK",
        "message": "_DEFENDER_ забылся, и тут _ATTACKER_, пробив блок, сказав &quot;БУ!&quot;, ласково заломил руку за спину соперника, ударил в область лопаток."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "CHEST",
        "action": "BREAK_THROUGH_BLOCK",
        "message": "_DEFENDER_ замешкался, и за это жестокий _ATTACKER_, пробив блок, проклиная этот сайт, провел ужасный бросок через пупок оппонента, попал в корпус."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "CHEST",
        "action": "DODGE",
        "message": "_ATTACKER_ думал не о том, и прекрасный _DEFENDER_, уклонился от удара левой ноги в сердце."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "CHEST",
        "action": "DODGE",
        "message": "_ATTACKER_ пытался провести удар, но _DEFENDER_, отскочил от удара копчиком в грудь."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "CHEST",
        "action": "DODGE",
        "message": "_ATTACKER_ пытался провести удар, но расстроенный _DEFENDER_, отпрыгнул от удара тяжелым кулаком в корпус."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "CHEST",
        "action": "DODGE",
        "message": "_ATTACKER_ думал о &lt;вырезано цензурой&gt;, вследствие чего _DEFENDER_, уклонился от удара ногой в сердце."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "CHEST",
        "action": "DODGE",
        "message": "_ATTACKER_ поскользнулся, и разъяренный _DEFENDER_, увернулся от коленом в область лопаток."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "CHEST",
        "action": "DODGE",
        "message": "_ATTACKER_ думал о &lt;вырезано цензурой&gt;, вследствие чего разъяренный _DEFENDER_, увернулся от удара левой пяткой в область лопаток."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "CHEST",
        "action": "DODGE",
        "message": "_ATTACKER_ поскользнулся, и расстроенный _DEFENDER_, отскочил от удара лбом в солнечное сплетение."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "CHEST",
        "action": "DODGE",
        "message": "_ATTACKER_ поскользнулся, и продвинутый _DEFENDER_, уклонился от удара коленом в корпус."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "CHEST",
        "action": "DODGE",
        "message": "_ATTACKER_ думал не о том, и продвинутый _DEFENDER_, уклонился от удара правой ногой в сердце."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "CHEST",
        "action": "DODGE",
        "message": "_ATTACKER_ старался провести удар, но неустрашимый _DEFENDER_, увернулся от удара левой ногой в грудь."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "CHEST",
        "action": "DODGE",
        "message": "_ATTACKER_ пытался нанести удар, но храбрый _DEFENDER_, увернулся от удара головой в область лопаток."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "GIRDLE",
        "action": "ATTACK",
        "message": "_DEFENDER_ замешкался, и за это храбрый _ATTACKER_, улыбаясь, саданул укол грудью по &lt;вырезано цензурой&gt;."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "GIRDLE",
        "action": "ATTACK",
        "message": "_DEFENDER_ забылся, и тут бесчувственный _ATTACKER_, разбежавшись, рубанул ногой по левой ягодице."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "GIRDLE",
        "action": "ATTACK",
        "message": "_DEFENDER_ думал не о том, и расстроенный _ATTACKER_ ударил ногой в пах."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "GIRDLE",
        "action": "ATTACK",
        "message": "_DEFENDER_ поперхнулся, но вдруг обезумевший _ATTACKER_ сдуру вмазал лбом в пах."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "GIRDLE",
        "action": "ATTACK",
        "message": "_DEFENDER_ растерялся, как вдруг расстроенный _ATTACKER_ сдуру вмазал кулаком по &lt;вырезано цензурой&gt;."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "GIRDLE",
        "action": "ATTACK",
        "message": "_DEFENDER_ засмотрелся на &lt;вырезано цензурой&gt;, а в это время расстроенный _ATTACKER_ сдуру вмазал ребром руки по &lt;вырезано цензурой&gt;."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "GIRDLE",
        "action": "ATTACK",
        "message": "_DEFENDER_ думал не о том, и мужественный _ATTACKER_ ударил лбом в промежность."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "GIRDLE",
        "action": "ATTACK",
        "message": "_DEFENDER_ пытался что-то сказать но вдруг, неожиданно _ATTACKER_ отчаянно проткнул кулаком в промежность."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "GIRDLE",
        "action": "ATTACK",
        "message": "_DEFENDER_ высморкался, и в это время хитрый _ATTACKER_ нехотя уколол коленом в пах."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "GIRDLE",
        "action": "ATTACK",
        "message": "_DEFENDER_ думал не о том, и продвинутый _ATTACKER_, улыбаясь, саданул укол коленом в промежность."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "GIRDLE",
        "action": "ATTACK",
        "message": "_DEFENDER_ растерялся, как вдруг храбрый _ATTACKER_ не подумав, рубанул ногой по правой ягодице."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "GIRDLE",
        "action": "ATTACK",
        "message": "_DEFENDER_ замешкался, и за это бесчувственный _ATTACKER_ приложил удар коленом по левой ягодице."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "GIRDLE",
        "action": "ATTACK",
        "message": "_DEFENDER_ ковырялся в зубах, и тут _ATTACKER_ отчаянно проткнул коленом в промежность."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "GIRDLE",
        "action": "ATTACK",
        "message": "_DEFENDER_ забылся, и тут храбрый _ATTACKER_ ударил лбом по левой ягодице."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "GIRDLE",
        "action": "ATTACK",
        "message": "_DEFENDER_ замешкался, и за это неустрашимый _ATTACKER_ нехотя уколол правой ногой в пах."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "GIRDLE",
        "action": "ATTACK",
        "message": "_DEFENDER_ обернулся, как внезапно жестокий _ATTACKER_, улыбаясь, саданул укол коленом в промежность."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "GIRDLE",
        "action": "ATTACK",
        "message": "_DEFENDER_ обернулся, как внезапно злобный _ATTACKER_ отчаянно проткнул коленом по &lt;вырезано цензурой&gt;."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "GIRDLE",
        "action": "ATTACK",
        "message": "_DEFENDER_ приложил удар грудью по левой ягодице."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "GIRDLE",
        "action": "ATTACK",
        "message": "_DEFENDER_ забылся, и тут бесчувственный _ATTACKER_ сдуру вмазал кулаком в пах."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "GIRDLE",
        "action": "ATTACK",
        "message": "_DEFENDER_ замешкался, и за это мужественный _ATTACKER_ сдуру вмазал лбом по левой ягодице."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "GIRDLE",
        "action": "ATTACK",
        "message": "_DEFENDER_ растерялся, как вдруг мужественный _ATTACKER_ сдуру вмазал ребром руки по правой ягодице."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "GIRDLE",
        "action": "ATTACK",
        "message": "_DEFENDER_ высморкался, и в это время наглый _ATTACKER_ сдуру вмазал правой ногой по левой ягодице."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "GIRDLE",
        "action": "ATTACK",
        "message": "_DEFENDER_ думал не о том, и обезумевший _ATTACKER_ ударил грудью в промежность."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "GIRDLE",
        "action": "ATTACK",
        "message": "_DEFENDER_ ковырялся в зубах, и тут мужественный _ATTACKER_ не подумав, рубанул левой ногой в промежность."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "GIRDLE",
        "action": "ATTACK",
        "message": "_DEFENDER_ замешкался, и за это мужественный _ATTACKER_ отчаянно проткнул правой ногой по правой ягодице."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "GIRDLE",
        "action": "ATTACK",
        "message": "_DEFENDER_ ковырялся в зубах, и тут злобный _ATTACKER_, улыбаясь, саданул укол правой ногой в промежность."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "GIRDLE",
        "action": "ATTACK",
        "message": "_DEFENDER_ пришел в себя, но в это время наглый _ATTACKER_ приложил удар ногой по &lt;вырезано цензурой&gt;."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "GIRDLE",
        "action": "ATTACK",
        "message": "_DEFENDER_ растерялся, как вдруг продвинутый _ATTACKER_ нехотя уколол коленом в пах."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "GIRDLE",
        "action": "ATTACK",
        "message": "_DEFENDER_ думал не о том, и обезумевший _ATTACKER_, улыбаясь, саданул укол левой ногой в пах."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "GIRDLE",
        "action": "ATTACK",
        "message": "_DEFENDER_ растерялся, как вдруг разъяренный _ATTACKER_ сдуру вмазал ногой по правой ягодице."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "GIRDLE",
        "action": "ATTACK",
        "message": "_DEFENDER_ пришел в себя, но в это время продвинутый _ATTACKER_ нехотя уколол ногой в пах."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "GIRDLE",
        "action": "ATTACK",
        "message": "_DEFENDER_ высморкался, и в это время мужественный _ATTACKER_ ударил ребром руки по левой ягодице."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "GIRDLE",
        "action": "ATTACK",
        "message": "_DEFENDER_ обернулся, как внезапно мужественный _ATTACKER_ отчаянно проткнул грудью по левой ягодице."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "GIRDLE",
        "action": "ATTACK",
        "message": "_DEFENDER_ ковырялся в зубах, и тут мужественный _ATTACKER_ ударил кулаком в пах."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "GIRDLE",
        "action": "ATTACK",
        "message": "_DEFENDER_ растерялся, как вдруг жестокий _ATTACKER_, разбежавшись, рубанул ребром руки в пах."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "GIRDLE",
        "action": "ATTACK",
        "message": "_DEFENDER_ высморкался, и в это время наглый _ATTACKER_ сдуру вмазал левой ногой по &lt;вырезано цензурой&gt;."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "GIRDLE",
        "action": "ATTACK",
        "message": "_DEFENDER_ поперхнулся, но вдруг _ATTACKER_ не подумав, рубанул кулаком в пах."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "GIRDLE",
        "action": "ATTACK",
        "message": "_DEFENDER_ поперхнулся, но вдруг неустрашимый _ATTACKER_ приложил удар кулаком по правой ягодице."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "GIRDLE",
        "action": "ATTACK",
        "message": "_DEFENDER_ пытался что-то сказать но вдруг, неожиданно хитрый _ATTACKER_, разбежавшись, рубанул лбом в промежность."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "GIRDLE",
        "action": "ATTACK",
        "message": "_DEFENDER_ засмотрелся на &lt;вырезано цензурой&gt;, а в это время _ATTACKER_ нехотя уколол лбом по левой ягодице."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "GIRDLE",
        "action": "ATTACK",
        "message": "_DEFENDER_ пришел в себя, но в это время разъяренный _ATTACKER_ отчаянно проткнул ребром руки по левой ягодице."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "GIRDLE",
        "action": "BLOCK",
        "message": "_ATTACKER_ подскользнулся, и _DEFENDER_ остановил удар ногой по левой ягодице."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "GIRDLE",
        "action": "BLOCK",
        "message": "_ATTACKER_ потерял самоконтроль, вследствие чего мужественный _DEFENDER_ остановил удар грудью в промежность."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "GIRDLE",
        "action": "BLOCK",
        "message": "_ATTACKER_ закашлялся, и разъяренный _DEFENDER_ остановил удар коленом по левой ягодице."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "GIRDLE",
        "action": "BLOCK",
        "message": "_ATTACKER_ пытался провести удар, но _DEFENDER_ остановил удар лбом по &lt;вырезано цензурой&gt;."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "GIRDLE",
        "action": "BLOCK",
        "message": "_ATTACKER_ думал не о том, и обезумевший _DEFENDER_ остановил удар правой ногой по &lt;вырезано цензурой&gt;."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "GIRDLE",
        "action": "BLOCK",
        "message": "_ATTACKER_ подскользнулся, и _DEFENDER_ отбил удар ногой по правой ягодице."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "GIRDLE",
        "action": "BLOCK",
        "message": "_ATTACKER_ закашлялся, и расстроенный _DEFENDER_ заблокировал удар левой ногой в пах."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "GIRDLE",
        "action": "BLOCK",
        "message": "_ATTACKER_ потерял самоконтроль, вследствие чего расстроенный _DEFENDER_ заблокировал удар левой ногой в промежность."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "GIRDLE",
        "action": "BLOCK",
        "message": "_ATTACKER_ потерял самоконтроль, вследствие чего наглый _DEFENDER_ заблокировал удар ребром руки по левой ягодице."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "GIRDLE",
        "action": "BLOCK",
        "message": "_ATTACKER_ потерял самоконтроль, вследствие чего злобный _DEFENDER_ заблокировал удар правой ногой по правой ягодице."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "GIRDLE",
        "action": "BLOCK",
        "message": "_ATTACKER_ подскользнулся, и жестокий _DEFENDER_ заблокировал удар коленом по правой ягодице."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "GIRDLE",
        "action": "BLOCK",
        "message": "_ATTACKER_ старался провести удар, но злобный _DEFENDER_ отбил удар правой ногой в пах."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "GIRDLE",
        "action": "BLOCK",
        "message": "_ATTACKER_ пытался провести удар, но расстроенный _DEFENDER_ остановил удар ногой в промежность."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "GIRDLE",
        "action": "BLOCK",
        "message": "_ATTACKER_ думал о &lt;вырезано цензурой&gt;, вследствие чего _DEFENDER_ остановил удар левой ногой по правой ягодице."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "GIRDLE",
        "action": "BLOCK",
        "message": "_ATTACKER_ закашлялся, и расстроенный _DEFENDER_ отбил удар лбом по левой ягодице."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "GIRDLE",
        "action": "BLOCK",
        "message": "_ATTACKER_ старался провести удар, но неустрашимый _DEFENDER_ отбил удар кулаком по правой ягодице."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "GIRDLE",
        "action": "BLOCK",
        "message": "_ATTACKER_ подскользнулся, и _DEFENDER_ остановил удар грудью по &lt;вырезано цензурой&gt;."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "GIRDLE",
        "action": "BLOCK",
        "message": "_ATTACKER_ пытался нанести удар, но мужественный _DEFENDER_ отбил удар левой ногой в пах."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "GIRDLE",
        "action": "BLOCK",
        "message": "_ATTACKER_ думал о &lt;вырезано цензурой&gt;, вследствие чего продвинутый _DEFENDER_ остановил удар кулаком в промежность."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "GIRDLE",
        "action": "BLOCK",
        "message": "_ATTACKER_ пытался нанести удар, но _DEFENDER_ отбил удар кулаком по левой ягодице."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "GIRDLE",
        "action": "BLOCK",
        "message": "_ATTACKER_ думал о &lt;вырезано цензурой&gt;, вследствие чего расстроенный _DEFENDER_ остановил удар левой ногой в промежность."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "GIRDLE",
        "action": "BLOCK",
        "message": "_ATTACKER_ закашлялся, и _DEFENDER_ отбил удар кулаком в промежность."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "GIRDLE",
        "action": "BLOCK",
        "message": "_ATTACKER_ подскользнулся, и наглый _DEFENDER_ отбил удар правой ногой в пах."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "GIRDLE",
        "action": "BLOCK",
        "message": "_ATTACKER_ думал не о том, и наглый _DEFENDER_ остановил удар кулаком по &lt;вырезано цензурой&gt;."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "GIRDLE",
        "action": "BLOCK",
        "message": "_ATTACKER_ закашлялся, и наглый _DEFENDER_ остановил удар грудью в пах."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "GIRDLE",
        "action": "BLOCK",
        "message": "_ATTACKER_ думал о &lt;вырезано цензурой&gt;, вследствие чего наглый _DEFENDER_ заблокировал удар правой ногой по правой ягодице."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "GIRDLE",
        "action": "BLOCK",
        "message": "_ATTACKER_ думал не о том, и разъяренный _DEFENDER_ остановил удар кулаком по левой ягодице."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "GIRDLE",
        "action": "BLOCK",
        "message": "_ATTACKER_ подскользнулся, и _DEFENDER_ остановил удар правой ногой по левой ягодице."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "GIRDLE",
        "action": "BLOCK",
        "message": "_ATTACKER_ пытался нанести удар, но храбрый _DEFENDER_ отбил удар грудью по правой ягодице."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "GIRDLE",
        "action": "BLOCK",
        "message": "_ATTACKER_ пытался нанести удар, но жестокий _DEFENDER_ заблокировал удар грудью в пах."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "GIRDLE",
        "action": "BREAK_THROUGH_BLOCK",
        "message": "_DEFENDER_ обернулся, как внезапно продвинутый _ATTACKER_, напугав всех, неслышно подойдя сзади ударил, пробив блок, булыжником в промежность."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "GIRDLE",
        "action": "BREAK_THROUGH_BLOCK",
        "message": "_DEFENDER_ думал не о том, и расстроенный _ATTACKER_, пробив блок, наступил на ногу врага, ударил по &lt;вырезано цензурой&gt;."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "GIRDLE",
        "action": "BREAK_THROUGH_BLOCK",
        "message": "_DEFENDER_ пришел в себя, но в это время жестокий _ATTACKER_, пробив блок, ласково заломил руку за спину соперника, ударил по левой ягодице."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "GIRDLE",
        "action": "BREAK_THROUGH_BLOCK",
        "message": "_DEFENDER_ засмотрелся на &lt;вырезано цензурой&gt;, а в это время неустрашимый _ATTACKER_, пробив блок, наступил на ногу врага, ударил в промежность."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "GIRDLE",
        "action": "BREAK_THROUGH_BLOCK",
        "message": "_DEFENDER_ высморкался, и в это время _ATTACKER_, напугав всех, неслышно подойдя сзади ударил, пробив блок, булыжником по правой ягодице."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "GIRDLE",
        "action": "BREAK_THROUGH_BLOCK",
        "message": "_DEFENDER_ растерялся, как вдруг бесчувственный _ATTACKER_, пробив блок, наступил на ногу врага, ударил в промежность."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "GIRDLE",
        "action": "BREAK_THROUGH_BLOCK",
        "message": "_DEFENDER_ поперхнулся, но вдруг _ATTACKER_, пробив блок, провел ужасный бросок через пупок оппонента, ударил по правой ягодице."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "GIRDLE",
        "action": "BREAK_THROUGH_BLOCK",
        "message": "_DEFENDER_ думал не о том, и расстроенный _ATTACKER_, пробив блок, наступил на ногу врага, ударил по правой ягодице."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "GIRDLE",
        "action": "BREAK_THROUGH_BLOCK",
        "message": "_DEFENDER_ пытался что-то сказать но вдруг, неожиданно расстроенный _ATTACKER_, пробив блок, провел ужасный бросок через пупок оппонента, ударил по левой ягодице."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "GIRDLE",
        "action": "BREAK_THROUGH_BLOCK",
        "message": "_DEFENDER_ засмотрелся на &lt;вырезано цензурой&gt;, а в это время _ATTACKER_, пробив блок, ласково заломил руку за спину соперника, ударил по левой ягодице."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "GIRDLE",
        "action": "BREAK_THROUGH_BLOCK",
        "message": "_DEFENDER_ высморкался, и в это время _ATTACKER_, пробив блок, провел ужасный бросок через пупок оппонента, ударила по &lt;вырезано цензурой&gt;."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "GIRDLE",
        "action": "BREAK_THROUGH_BLOCK",
        "message": "_DEFENDER_ замешкался, и за это мужественный _ATTACKER_, напугав всех, неслышно подойдя сзади ударил, пробив блок, булыжником в промежность."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "GIRDLE",
        "action": "BREAK_THROUGH_BLOCK",
        "message": "_DEFENDER_ поперхнулся, но вдруг _ATTACKER_, пробив блок, наступил на ногу врага, ударил по &lt;вырезано цензурой&gt;."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "GIRDLE",
        "action": "BREAK_THROUGH_BLOCK",
        "message": "_DEFENDER_ замешкался, и за это _ATTACKER_, пробив блок, ласково заломил руку за спину соперника, ударил по правой ягодице."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "GIRDLE",
        "action": "BREAK_THROUGH_BLOCK",
        "message": "_DEFENDER_ замешкался, и за это неустрашимый _ATTACKER_, пробив блок, провел ужасный бросок через пупок оппонента, ударил в промежность."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "GIRDLE",
        "action": "BREAK_THROUGH_BLOCK",
        "message": "_DEFENDER_ забылся, и тут _ATTACKER_, напугав всех, неслышно подойдя сзади ударил, пробив блок, булыжником по &lt;вырезано цензурой&gt;."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "GIRDLE",
        "action": "BREAK_THROUGH_BLOCK",
        "message": "_DEFENDER_ растерялся, как вдруг хитрый _ATTACKER_, пробив блок, укусил в нос противника, ударил по правой ягодице."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "GIRDLE",
        "action": "CRITICAL",
        "message": "_DEFENDER_ обернулся, как внезапно хитрый _ATTACKER_, напугав всех, неслышно подойдя сзади ударил булыжником оппонента в пах."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "GIRDLE",
        "action": "CRITICAL",
        "message": "_DEFENDER_ обернулся, как внезапно хитрый _ATTACKER_, проклиная этот сайт, провел ужасный бросок через пупок оппонента, попал в пах."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "GIRDLE",
        "action": "CRITICAL",
        "message": "_DEFENDER_ замешкался, и за это храбрый _ATTACKER_, напугав всех, укусил в нос противника и ударил по правой ягодице."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "GIRDLE",
        "action": "CRITICAL",
        "message": "_DEFENDER_ пытался что-то сказать но вдруг, неожиданно бесчувственный _ATTACKER_, напугав всех, неслышно подойдя сзади ударил булыжником оппонента по левой ягодице."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "GIRDLE",
        "action": "CRITICAL",
        "message": "_DEFENDER_ растерялся, как вдруг _ATTACKER_, напугав всех, неслышно подойдя сзади ударил булыжником оппонента в промежность."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "GIRDLE",
        "action": "CRITICAL",
        "message": "_DEFENDER_ думал не о том, и неустрашимый _ATTACKER_, напугав всех, неслышно подойдя сзади ударил булыжником оппонента в пах."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "GIRDLE",
        "action": "CRITICAL",
        "message": "_DEFENDER_ пытался что-то сказать но вдруг, неожиданно _ATTACKER_, сказав &quot;БУ!&quot;, ласково заломил руку за спину соперника, ударил в пах."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "GIRDLE",
        "action": "CRITICAL",
        "message": "_DEFENDER_ обернулся, как внезапно _ATTACKER_, напугав всех, укусил в нос противника и ударил в пах."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "GIRDLE",
        "action": "CRITICAL",
        "message": "_DEFENDER_ засмотрелся на &lt;вырезано цензурой&gt;, а в это время мужественный _ATTACKER_, напугав всех, неслышно подойдя сзади ударил булыжником оппонента по &lt;вырезано цензурой&gt;."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "GIRDLE",
        "action": "CRITICAL",
        "message": "_DEFENDER_ забылся, и тут _ATTACKER_, сказав &quot;БУ!&quot;, ласково заломил руку за спину соперника, ударил по &lt;вырезано цензурой&gt;."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "GIRDLE",
        "action": "CRITICAL",
        "message": "_DEFENDER_ думал не о том, и храбрый _ATTACKER_, напугав всех, неслышно подойдя сзади ударил булыжником оппонента по правой ягодице."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "GIRDLE",
        "action": "CRITICAL",
        "message": "_DEFENDER_ пытался что-то сказать но вдруг, неожиданно расстроенный _ATTACKER_, напугав всех, укусил в нос противника и ударил в пах."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "GIRDLE",
        "action": "CRITICAL",
        "message": "_DEFENDER_ ковырялся в зубах, и тут неустрашимый _ATTACKER_, сказав &quot;БУ!&quot;, ласково заломил руку за спину соперника, ударил по правой ягодице."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "GIRDLE",
        "action": "CRITICAL",
        "message": "_DEFENDER_ пришел в себя, но в это время _ATTACKER_, сказав &quot;БУ!&quot;, ласково заломил руку за спину соперника, ударил по левой ягодице."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "GIRDLE",
        "action": "CRITICAL",
        "message": "_DEFENDER_ растерялся, как вдруг жестокий _ATTACKER_, наступил на ногу врага, ударил по &lt;вырезано цензурой&gt;."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "GIRDLE",
        "action": "CRITICAL",
        "message": "_DEFENDER_ пытался что-то сказать но вдруг, неожиданно наглый _ATTACKER_, наступил на ногу врага, ударил по правой ягодице."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "GIRDLE",
        "action": "DODGE",
        "message": "_ATTACKER_ поскользнулся, и жестокий _DEFENDER_, увернулся от удара ногой в промежность."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "GIRDLE",
        "action": "DODGE",
        "message": "_ATTACKER_ пытался нанести удар, но расстроенный _DEFENDER_, увернулся от удара коленом в пах."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "GIRDLE",
        "action": "DODGE",
        "message": "_ATTACKER_ поскользнулся, и мужественный _DEFENDER_, отскочил от удара кулаком по правой ягодице."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "GIRDLE",
        "action": "DODGE",
        "message": "_ATTACKER_ потерял самоконтроль, вследствие чего _DEFENDER_, уклонился от удара ребром руки по правой ягодице."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "GIRDLE",
        "action": "DODGE",
        "message": "_ATTACKER_ пытался нанести удар, но мужественный _DEFENDER_, отскочил от удара копчиком в промежность."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "GIRDLE",
        "action": "DODGE",
        "message": "_ATTACKER_ потерял самоконтроль, вследствие чего наглый _DEFENDER_, увернулся от удара головой в промежность."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "GIRDLE",
        "action": "DODGE",
        "message": "_ATTACKER_ пытался провести удар, но храбрый _DEFENDER_, уклонился от удара локтем в промежность."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "GIRDLE",
        "action": "DODGE",
        "message": "_ATTACKER_ думал не о том, и продвинутый _DEFENDER_, уклонился от удара коленом в промежность."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "GIRDLE",
        "action": "DODGE",
        "message": "_ATTACKER_ старался провести удар, но _DEFENDER_, уклонился от удара правой ногой по левой ягодице."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "GIRDLE",
        "action": "DODGE",
        "message": "_ATTACKER_ старался провести удар, но обезумевший _DEFENDER_, уклонился от удара грудью по левой ягодице."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "GIRDLE",
        "action": "DODGE",
        "message": "_ATTACKER_ думал о &lt;вырезано цензурой&gt;, вследствие чего разъяренный _DEFENDER_, увернулся от удара коленом по &lt;вырезано цензурой&gt;."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "GIRDLE",
        "action": "DODGE",
        "message": "_ATTACKER_ потерял самоконтроль, вследствие чего разъяренный _DEFENDER_, отскочил от удара кулаком в промежность."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "GIRDLE",
        "action": "DODGE",
        "message": "_ATTACKER_ закашлялся, и _DEFENDER_, увернулся от удара правой ногой по правой ягодице."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "GIRDLE",
        "action": "DODGE",
        "message": "_ATTACKER_ закашлялся, и мужественный _DEFENDER_, отскочил от удара левой пяткой в промежность."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "GIRDLE",
        "action": "DODGE",
        "message": "_ATTACKER_ думал не о том, и злобный _DEFENDER_, уклонился от удара локтем в пах."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "GIRDLE",
        "action": "DODGE",
        "message": "_ATTACKER_ пытался провести удар, но _DEFENDER_, отпрыгнул от удара правой ногой в пах."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "GIRDLE",
        "action": "DODGE",
        "message": "_ATTACKER_ пытался нанести удар, но _DEFENDER_, уклонился от удара затылком по &lt;вырезано цензурой&gt;."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "GIRDLE",
        "action": "DODGE",
        "message": "_ATTACKER_ поскользнулся, и _DEFENDER_, отпрыгнул от удара кулаком в пах."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "GIRDLE",
        "action": "DODGE",
        "message": "_ATTACKER_ думал не о том, и бесчувственный _DEFENDER_, увернулся от удара головой в пах."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "HEAD",
        "action": "ATTACK",
        "message": "_DEFENDER_ растерялся, как вдруг продвинутый _ATTACKER_ нехотя уколол грудью в глаз."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "HEAD",
        "action": "ATTACK",
        "message": "_DEFENDER_ высморкался, и в это время храбрый _ATTACKER_, разбежавшись, рубанул ногой по переносице."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "HEAD",
        "action": "ATTACK",
        "message": "_DEFENDER_ ковырялся в зубах, и тут мужественный _ATTACKER_ не подумав, рубанул ногой в левый глаз."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "HEAD",
        "action": "ATTACK",
        "message": "_DEFENDER_ ковырялся в зубах, и тут мужественный _ATTACKER_, улыбаясь, саданул укол ребром руки в левый глаз."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "HEAD",
        "action": "ATTACK",
        "message": "_DEFENDER_ думал не о том, и продвинутый _ATTACKER_ отчаянно проткнул кулаком в правый глаз."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "HEAD",
        "action": "ATTACK",
        "message": "_DEFENDER_ поперхнулся, но вдруг мужественный _ATTACKER_, разбежавшись, рубанул коленом в глаз."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "HEAD",
        "action": "ATTACK",
        "message": "_DEFENDER_ пытался что-то сказать но вдруг, неожиданно _ATTACKER_ сдуру вмазал ребром руки в кадык."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "HEAD",
        "action": "ATTACK",
        "message": "_DEFENDER_ думал не о том, и продвинутый _ATTACKER_ приложил удар правой ногой по затылку."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "HEAD",
        "action": "ATTACK",
        "message": "_DEFENDER_ пришел в себя, но в это время злобный _ATTACKER_, улыбаясь, саданул укол лбом в левый глаз."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "HEAD",
        "action": "ATTACK",
        "message": "_DEFENDER_ поперхнулся, но вдруг _ATTACKER_ сдуру вмазал левой ногой по затылку."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "HEAD",
        "action": "ATTACK",
        "message": "_DEFENDER_ замешкался, и за это мужественный _ATTACKER_ отчаянно проткнул ребром руки в кадык."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "HEAD",
        "action": "ATTACK",
        "message": "_DEFENDER_ засмотрелся на &lt;вырезано цензурой&gt;, а в это время _ATTACKER_ ударил коленом в глаз."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "HEAD",
        "action": "ATTACK",
        "message": "_DEFENDER_ думал не о том, и _ATTACKER_ ударил кулаком в нос."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "HEAD",
        "action": "ATTACK",
        "message": "_DEFENDER_ ковырялся в зубах, и тут жестокий _ATTACKER_, улыбаясь, саданул укол грудью в левый глаз."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "HEAD",
        "action": "ATTACK",
        "message": "_DEFENDER_ растерялся, как вдруг хитрый _ATTACKER_, разбежавшись, рубанул коленом в левый глаз."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "HEAD",
        "action": "ATTACK",
        "message": "_DEFENDER_ пришел в себя, но в это время злобный _ATTACKER_, разбежавшись, рубанул левой ногой в нос."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "HEAD",
        "action": "ATTACK",
        "message": "_DEFENDER_ растерялся, как вдруг жестокий _ATTACKER_ нехотя уколол ногой по переносице."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "HEAD",
        "action": "ATTACK",
        "message": "_DEFENDER_ высморкался, и в это время продвинутый _ATTACKER_ ударил правой ногой в правый глаз."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "HEAD",
        "action": "ATTACK",
        "message": "_DEFENDER_ растерялся, как вдруг _ATTACKER_ приложил удар левой ногой в правый глаз."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "HEAD",
        "action": "ATTACK",
        "message": "_DEFENDER_ растерялся, как вдруг обезумевший _ATTACKER_ ударил левой ногой в глаз."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "HEAD",
        "action": "ATTACK",
        "message": "_DEFENDER_ пришел в себя, но в это время мужественный _ATTACKER_, улыбаясь, саданул укол кулаком в нос."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "HEAD",
        "action": "ATTACK",
        "message": "_DEFENDER_ высморкался, и в это время храбрый _ATTACKER_ отчаянно проткнул ребром руки в левый глаз."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "HEAD",
        "action": "ATTACK",
        "message": "_DEFENDER_ пришел в себя, но в это время _ATTACKER_ приложил удар правой ногой в правый глаз."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "HEAD",
        "action": "ATTACK",
        "message": "_DEFENDER_ засмотрелся на &lt;вырезано цензурой&gt;, а в это время расстроенный _ATTACKER_ ударил ногой в нос."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "HEAD",
        "action": "ATTACK",
        "message": "_DEFENDER_ засмотрелся на &lt;вырезано цензурой&gt;, а в это время разъяренный _ATTACKER_ не подумав, рубанул ребром руки по затылку."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "HEAD",
        "action": "ATTACK",
        "message": "_DEFENDER_ ковырялся в зубах, и тут жестокий _ATTACKER_, разбежавшись, рубанул коленом в кадык."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "HEAD",
        "action": "ATTACK",
        "message": "_DEFENDER_ поперхнулся, но вдруг хитрый _ATTACKER_ нехотя уколол коленом в нос."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "HEAD",
        "action": "ATTACK",
        "message": "_DEFENDER_ пришел в себя, но в это время хитрый _ATTACKER_ сдуру вмазал лбом по переносице."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "HEAD",
        "action": "ATTACK",
        "message": "_DEFENDER_ пришел в себя, но в это время хитрый _ATTACKER_ приложил удар ногой в нос."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "HEAD",
        "action": "ATTACK",
        "message": "_DEFENDER_ думал не о том, и продвинутый _ATTACKER_, разбежавшись, рубанул коленом в глаз."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "HEAD",
        "action": "ATTACK",
        "message": "_DEFENDER_ думал не о том, и мужественный _ATTACKER_ не подумав, рубанул коленом в кадык."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "HEAD",
        "action": "ATTACK",
        "message": "_DEFENDER_ обернулся, как внезапно бесчувственный _ATTACKER_ ударил кулаком в правый глаз."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "HEAD",
        "action": "ATTACK",
        "message": "_DEFENDER_ забылся, и тут бесчувственный _ATTACKER_ ударил коленом по затылку."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "HEAD",
        "action": "ATTACK",
        "message": "_DEFENDER_ пришел в себя, но в это время бесчувственный _ATTACKER_ сдуру вмазал левой ногой по переносице."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "HEAD",
        "action": "ATTACK",
        "message": "_DEFENDER_ забылся, и тут продвинутый _ATTACKER_ ударил левой ногой в правый глаз."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "HEAD",
        "action": "ATTACK",
        "message": "_DEFENDER_ думал не о том, и храбрый _ATTACKER_ приложил удар ногой в нос."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "HEAD",
        "action": "ATTACK",
        "message": "_DEFENDER_ думал не о том, и неустрашимый _ATTACKER_, улыбаясь, саданул укол грудью в челюсть."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "HEAD",
        "action": "ATTACK",
        "message": "_DEFENDER_ обернулся, как внезапно жестокий _ATTACKER_ отчаянно проткнул коленом в кадык."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "HEAD",
        "action": "ATTACK",
        "message": "_DEFENDER_ высморкался, и в это время хитрый _ATTACKER_ сдуру вмазал левой ногой в правый глаз."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "HEAD",
        "action": "ATTACK",
        "message": "_DEFENDER_ пытался что-то сказать но вдруг, неожиданно разъяренный _ATTACKER_ сдуру вмазал кулаком по переносице."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "HEAD",
        "action": "ATTACK",
        "message": "_DEFENDER_ пришел в себя, но в это время продвинутый _ATTACKER_ приложил удар коленом по затылку."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "HEAD",
        "action": "ATTACK",
        "message": "_DEFENDER_ растерялся, как вдруг храбрый _ATTACKER_ приложил удар правой ногой в нос."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "HEAD",
        "action": "ATTACK",
        "message": "_DEFENDER_ забылся, и тут _ATTACKER_, разбежавшись, рубанул ребром руки по переносице."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "HEAD",
        "action": "ATTACK",
        "message": "_DEFENDER_ высморкался, и в это время расстроенный _ATTACKER_, разбежавшись, рубанул левой ногой в кадык."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "HEAD",
        "action": "ATTACK",
        "message": "_DEFENDER_ растерялся, как вдруг наглый _ATTACKER_ ударил ребром руки в глаз."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "HEAD",
        "action": "ATTACK",
        "message": "_DEFENDER_ замешкался, и за это жестокий _ATTACKER_ приложил удар грудью в правый глаз."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "HEAD",
        "action": "ATTACK",
        "message": "_DEFENDER_ обернулся, как внезапно неустрашимый _ATTACKER_ нехотя уколол ногой в правый глаз."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "HEAD",
        "action": "ATTACK",
        "message": "_DEFENDER_ пришел в себя, но в это время мужественный _ATTACKER_ приложил удар правой ногой в кадык."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "HEAD",
        "action": "ATTACK",
        "message": "_DEFENDER_ пришел в себя, но в это время расстроенный _ATTACKER_ не подумав, рубанул коленом в правый глаз."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "HEAD",
        "action": "ATTACK",
        "message": "_DEFENDER_ пытался что-то сказать но вдруг, неожиданно неустрашимый _ATTACKER_ отчаянно проткнул правой ногой в нос."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "HEAD",
        "action": "ATTACK",
        "message": "_DEFENDER_ растерялся, как вдруг храбрый _ATTACKER_ ударил левой ногой в левый глаз."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "HEAD",
        "action": "ATTACK",
        "message": "_DEFENDER_ высморкался, и в это время неустрашимый _ATTACKER_, разбежавшись, рубанул грудью по переносице."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "HEAD",
        "action": "BLOCK",
        "message": "_ATTACKER_ старался провести удар, но разъяренный _DEFENDER_ отбил удар коленом в кадык."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "HEAD",
        "action": "BLOCK",
        "message": "_ATTACKER_ думал о &lt;вырезано цензурой&gt;, вследствие чего жестокий _DEFENDER_ заблокировал удар правой ногой в кадык."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "HEAD",
        "action": "BLOCK",
        "message": "_ATTACKER_ закашлялся, и бесчувственный _DEFENDER_ заблокировал удар лбом по затылку."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "HEAD",
        "action": "BLOCK",
        "message": "_ATTACKER_ старался провести удар, но _DEFENDER_ заблокировал удар ногой в правый глаз."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "HEAD",
        "action": "BLOCK",
        "message": "_ATTACKER_ пытался нанести удар, но храбрый _DEFENDER_ заблокировал удар ногой в кадык."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "HEAD",
        "action": "BLOCK",
        "message": "_ATTACKER_ думал не о том, и бесчувственный _DEFENDER_ отбил удар грудью в челюсть."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "HEAD",
        "action": "BLOCK",
        "message": "_ATTACKER_ пытался провести удар, но храбрый _DEFENDER_ остановил удар правой ногой в нос."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "HEAD",
        "action": "BLOCK",
        "message": "_ATTACKER_ закашлялся, и _DEFENDER_ заблокировал удар грудью по переносице."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "HEAD",
        "action": "BLOCK",
        "message": "_ATTACKER_ старался провести удар, но мужественный _DEFENDER_ остановил удар коленом в нос."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "HEAD",
        "action": "BLOCK",
        "message": "_ATTACKER_ закашлялся, и разъяренный _DEFENDER_ остановил удар кулаком в кадык."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "HEAD",
        "action": "BLOCK",
        "message": "_ATTACKER_ старался провести удар, но злобный _DEFENDER_ отбил удар коленом в кадык."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "HEAD",
        "action": "BLOCK",
        "message": "_ATTACKER_ пытался нанести удар, но _DEFENDER_ остановил удар грудью в глаз."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "HEAD",
        "action": "BLOCK",
        "message": "_ATTACKER_ думал не о том, и злобный _DEFENDER_ остановил удар правой ногой в челюсть."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "HEAD",
        "action": "BLOCK",
        "message": "_ATTACKER_ потерял самоконтроль, вследствие чего храбрый _DEFENDER_ остановил удар ногой в кадык."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "HEAD",
        "action": "BLOCK",
        "message": "_ATTACKER_ подскользнулся, и неустрашимый _DEFENDER_ отбил удар кулаком в правый глаз."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "HEAD",
        "action": "BLOCK",
        "message": "_ATTACKER_ старался провести удар, но обезумевший _DEFENDER_ заблокировал удар коленом по переносице."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "HEAD",
        "action": "BLOCK",
        "message": "_ATTACKER_ пытался провести удар, но жестокий _DEFENDER_ заблокировал удар лбом по затылку."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "HEAD",
        "action": "BLOCK",
        "message": "_ATTACKER_ пытался нанести удар, но расстроенный _DEFENDER_ отбил удар правой ногой в кадык."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "HEAD",
        "action": "BLOCK",
        "message": "_ATTACKER_ думал о &lt;вырезано цензурой&gt;, вследствие чего _DEFENDER_ заблокировал удар лбом в правый глаз."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "HEAD",
        "action": "BLOCK",
        "message": "_ATTACKER_ подскользнулся, и _DEFENDER_ отбил удар ребром руки в нос."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "HEAD",
        "action": "BLOCK",
        "message": "_ATTACKER_ старался провести удар, но _DEFENDER_ отбил удар правой ногой в правый глаз."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "HEAD",
        "action": "BLOCK",
        "message": "_ATTACKER_ думал не о том, и неустрашимый _DEFENDER_ отбил удар коленом в нос."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "HEAD",
        "action": "BLOCK",
        "message": "_ATTACKER_ старался провести удар, но неустрашимый _DEFENDER_ заблокировал удар левой ногой по затылку."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "HEAD",
        "action": "BLOCK",
        "message": "_ATTACKER_ пытался нанести удар, но мужественный _DEFENDER_ отбил удар правой ногой по переносице."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "HEAD",
        "action": "BLOCK",
        "message": "_ATTACKER_ старался провести удар, но злобный _DEFENDER_ остановил удар левой ногой в глаз."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "HEAD",
        "action": "BLOCK",
        "message": "_ATTACKER_ потерял самоконтроль, вследствие чего _DEFENDER_ остановил удар кулаком в нос."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "HEAD",
        "action": "BLOCK",
        "message": "_ATTACKER_ думал не о том, и неустрашимый _DEFENDER_ остановил удар лбом в нос."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "HEAD",
        "action": "BLOCK",
        "message": "_ATTACKER_ пытался провести удар, но расстроенный _DEFENDER_ заблокировал удар грудью по переносице."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "HEAD",
        "action": "BLOCK",
        "message": "_ATTACKER_ старался провести удар, но бесчувственный _DEFENDER_ отбил удар кулаком по затылку."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "HEAD",
        "action": "BLOCK",
        "message": "_ATTACKER_ думал о &lt;вырезано цензурой&gt;, вследствие чего мужественный _DEFENDER_ остановил удар коленом в челюсть."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "HEAD",
        "action": "BLOCK",
        "message": "_ATTACKER_ думал о &lt;вырезано цензурой&gt;, вследствие чего мужественный _DEFENDER_ заблокировал удар левой ногой в челюсть."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "HEAD",
        "action": "BLOCK",
        "message": "_ATTACKER_ старался провести удар, но разъяренный _DEFENDER_ заблокировал удар грудью в глаз."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "HEAD",
        "action": "BLOCK",
        "message": "_ATTACKER_ думал о &lt;вырезано цензурой&gt;, вследствие чего мужественный _DEFENDER_ заблокировал удар ногой в кадык."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "HEAD",
        "action": "BLOCK",
        "message": "_ATTACKER_ думал не о том, и мужественный _DEFENDER_ заблокировал удар левой ногой в нос."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "HEAD",
        "action": "BLOCK",
        "message": "_ATTACKER_ думал о &lt;вырезано цензурой&gt;, вследствие чего _DEFENDER_ заблокировал удар левой ногой в кадык."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "HEAD",
        "action": "BLOCK",
        "message": "_ATTACKER_ потерял самоконтроль, вследствие чего мужественный _DEFENDER_ остановил удар коленом в правый глаз."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "HEAD",
        "action": "BLOCK",
        "message": "_ATTACKER_ пытался нанести удар, но расстроенный _DEFENDER_ отбил удар кулаком в нос."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "HEAD",
        "action": "BLOCK",
        "message": "_ATTACKER_ пытался нанести удар, но обезумевший _DEFENDER_ отбил удар грудью по переносице."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "HEAD",
        "action": "BLOCK",
        "message": "_ATTACKER_ пытался провести удар, но неустрашимый _DEFENDER_ отбил удар левой ногой в левый глаз."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "HEAD",
        "action": "BLOCK",
        "message": "_ATTACKER_ потерял самоконтроль, вследствие чего жестокий _DEFENDER_ отбил удар ребром руки в глаз."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "HEAD",
        "action": "BLOCK",
        "message": "_ATTACKER_ старался провести удар, но продвинутый _DEFENDER_ заблокировал удар грудью по затылку."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "HEAD",
        "action": "BLOCK",
        "message": "_ATTACKER_ закашлялся, и хитрый _DEFENDER_ заблокировал удар грудью в глаз."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "HEAD",
        "action": "BLOCK",
        "message": "_ATTACKER_ думал не о том, и обезумевший _DEFENDER_ заблокировал удар ребром руки в правый глаз."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "HEAD",
        "action": "BLOCK",
        "message": "_ATTACKER_ закашлялся, и _DEFENDER_ остановил удар ногой по затылку."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "HEAD",
        "action": "BLOCK",
        "message": "_ATTACKER_ пытался нанести удар, но продвинутый _DEFENDER_ остановил удар грудью по переносице."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "HEAD",
        "action": "BLOCK",
        "message": "_ATTACKER_ старался провести удар, но мужественный _DEFENDER_ заблокировал удар правой ногой в кадык."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "HEAD",
        "action": "BREAK_THROUGH_BLOCK",
        "message": "_DEFENDER_ думал не о том, и мужественный _ATTACKER_, напугав всех, неслышно подойдя сзади ударил, пробив блок, булыжником в правый глаз."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "HEAD",
        "action": "BREAK_THROUGH_BLOCK",
        "message": "_DEFENDER_ обернулась, как внезапно злобный _ATTACKER_, пробив блок, наступил на ногу врага, ударил в правый глаз."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "HEAD",
        "action": "BREAK_THROUGH_BLOCK",
        "message": "_DEFENDER_ растерялся, как вдруг _ATTACKER_, пробив блок, расцарапал нос соперника, ударил в челюсть."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "HEAD",
        "action": "BREAK_THROUGH_BLOCK",
        "message": "_DEFENDER_ растерялся, как вдруг хитрый _ATTACKER_, пробив блок, ласково заломил руку за спину соперника, ударил по затылку."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "HEAD",
        "action": "BREAK_THROUGH_BLOCK",
        "message": "_DEFENDER_ высморкался, и в это время продвинутый _ATTACKER_, пробив блок, наступил на ногу врага, ударил в глаз."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "HEAD",
        "action": "BREAK_THROUGH_BLOCK",
        "message": "_DEFENDER_ растерялся, как вдруг бесчувственный _ATTACKER_, пробив блок, укусил в нос противника, ударил в правый глаз."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "HEAD",
        "action": "BREAK_THROUGH_BLOCK",
        "message": "_DEFENDER_ растерялся, как вдруг _ATTACKER_, пробив блок, укусил в нос противника, ударил в левый глаз."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "HEAD",
        "action": "BREAK_THROUGH_BLOCK",
        "message": "_DEFENDER_ засмотрелся на &lt;вырезано цензурой&gt;, а в это время храбрый _ATTACKER_, пробив блок, провел ужасный бросок через пупок оппонента, ударил по переносице."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "HEAD",
        "action": "BREAK_THROUGH_BLOCK",
        "message": "_DEFENDER_ замешкался, и за это продвинутый _ATTACKER_, пробив блок, ласково заломил руку за спину соперника, ударил в левый глаз."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "HEAD",
        "action": "BREAK_THROUGH_BLOCK",
        "message": "_DEFENDER_ засмотрелся на &lt;вырезано цензурой&gt;, а в это время злобный _ATTACKER_, пробив блок, укусил в нос противника, ударил в правый глаз."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "HEAD",
        "action": "BREAK_THROUGH_BLOCK",
        "message": "_DEFENDER_ замешкался, и за это бесчувственный _ATTACKER_, пробив блок, наступил на ногу врага, ударил в кадык."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "HEAD",
        "action": "CRITICAL",
        "message": "_DEFENDER_ засмотрелся на &lt;вырезано цензурой&gt;, а в это время _ATTACKER_, напугав всех, неслышно подойдя сзади ударил булыжником оппонента в нос."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "HEAD",
        "action": "CRITICAL",
        "message": "_DEFENDER_ пришел в себя, но в это время мужественный _ATTACKER_, проклиная этот сайт, провел ужасный бросок через пупок оппонента, попал по затылку."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "HEAD",
        "action": "CRITICAL",
        "message": "_DEFENDER_ пытался что-то сказать но вдруг, неожиданно хитрый _ATTACKER_, напугав всех, неслышно подойдя сзади ударил булыжником оппонента в кадык."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "HEAD",
        "action": "CRITICAL",
        "message": "_DEFENDER_ поперхнулся, но вдруг продвинутый _ATTACKER_, напугав всех, неслышно подойдя сзади ударил булыжником оппонента в кадык."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "HEAD",
        "action": "CRITICAL",
        "message": "_DEFENDER_ ковырялся в зубах, и тут разъяренный _ATTACKER_, расслабившись, расцарапал нос соперника и ударил в правый глаз."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "HEAD",
        "action": "CRITICAL",
        "message": "_DEFENDER_ засмотрелся на &lt;вырезано цензурой&gt;, а в это время _ATTACKER_, сказав &quot;БУ!&quot;, ласково заломил руку за спину соперника, ударил в кадык."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "HEAD",
        "action": "CRITICAL",
        "message": "_DEFENDER_ пришел в себя, но в это время жестокий _ATTACKER_, показав сразу два пальца, наступил на ногу врага и ударил в глаз."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "HEAD",
        "action": "CRITICAL",
        "message": "_DEFENDER_ обернулся, как внезапно наглый _ATTACKER_, расслабившись, расцарапал нос соперника и ударил по переносице."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "HEAD",
        "action": "CRITICAL",
        "message": "_DEFENDER_ поперхнулся, но вдруг разъяренный _ATTACKER_, расслабившись, расцарапал нос соперника и ударил по переносице."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "HEAD",
        "action": "CRITICAL",
        "message": "_DEFENDER_ пришел в себя, но в это время _ATTACKER_, напугав всех, укусил в нос противника и ударил в нос."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "HEAD",
        "action": "CRITICAL",
        "message": "_DEFENDER_ забылся, и тут _ATTACKER_, показав сразу два пальца, наступил на ногу врага и ударил по затылку."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "HEAD",
        "action": "DODGE",
        "message": "_ATTACKER_ думал о &lt;вырезано цензурой&gt;, вследствие чего хитрый _DEFENDER_, отпрыгнул от удара кулаком в глаз."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "HEAD",
        "action": "DODGE",
        "message": "_ATTACKER_ пытался провести удар, но бесчувственный _DEFENDER_, отпрыгнул от удара пальцем в глаз."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "HEAD",
        "action": "DODGE",
        "message": "_ATTACKER_ потерял самоконтроль, вследствие чего неустрашимый _DEFENDER_, уклонился от удара ребром ладони по переносице."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "HEAD",
        "action": "DODGE",
        "message": "_ATTACKER_ думал не о том, и храбрый _DEFENDER_, увернулся от удара мизинцем по затылку."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "HEAD",
        "action": "DODGE",
        "message": "_ATTACKER_ думал о &lt;вырезано цензурой&gt;, вследствие чего _DEFENDER_, отпрыгнул от удара кулака в челюсть."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "HEAD",
        "action": "DODGE",
        "message": "_ATTACKER_ закашлялся, и хитрый _DEFENDER_, уклонился от удара пальцем в правый глаз."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "HEAD",
        "action": "DODGE",
        "message": "_ATTACKER_ пытался нанести удар, но жестокий _DEFENDER_, отскочил от удара кулака по затылку."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "HEAD",
        "action": "DODGE",
        "message": "_ATTACKER_ потерял самоконтроль, вследствие чего злобный _DEFENDER_, отпрыгнул от удара локтем в нос."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "HEAD",
        "action": "DODGE",
        "message": "_ATTACKER_ думал не о том, и злобный _DEFENDER_, уклонился от удара коленом по переносице."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "HEAD",
        "action": "DODGE",
        "message": "_ATTACKER_ думал о &lt;вырезано цензурой&gt;, вследствие чего расстроенный _DEFENDER_, отпрыгнул от удара ногой в нос."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "HEAD",
        "action": "DODGE",
        "message": "_ATTACKER_ поскользнулся, и разъяренный _DEFENDER_, отпрыгнул от удара ребром руки в нос."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "HEAD",
        "action": "DODGE",
        "message": "_ATTACKER_ поскользнулся, и обезумевший _DEFENDER_, отскочил от удара коленом в нос."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "HEAD",
        "action": "DODGE",
        "message": "_ATTACKER_ старался провести удар, но хитрый _DEFENDER_, увернулся от удара грудью в правый глаз."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "HEAD",
        "action": "DODGE",
        "message": "_ATTACKER_ старался провести удар, но _DEFENDER_, уклонился от удара правой ногой по затылку."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "HEAD",
        "action": "DODGE",
        "message": "_ATTACKER_ думал не о том, и _DEFENDER_, отскочил от удара левой ногой в кадык."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "HEAD",
        "action": "DODGE",
        "message": "_ATTACKER_ поскользнулся, и хитрый _DEFENDER_, увернулся от удара лбом в кадык."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "LEGS",
        "action": "ATTACK",
        "message": "_DEFENDER_ растерялся, как вдруг жестокий _ATTACKER_, разбежавшись, рубанул ногой по коленной чашечке."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "LEGS",
        "action": "ATTACK",
        "message": "_DEFENDER_ засмотрелся на &lt;вырезано цензурой&gt;, а в это время _ATTACKER_ нехотя уколол коленом по коленной чашечке."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "LEGS",
        "action": "ATTACK",
        "message": "_DEFENDER_ пришел в себя, но в это время продвинутый _ATTACKER_ ударил коленом по коленной чашечке."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "LEGS",
        "action": "ATTACK",
        "message": "_DEFENDER_ высморкался, и в это время злобный _ATTACKER_ не подумав, рубанул кулаком по икрам."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "LEGS",
        "action": "ATTACK",
        "message": "_DEFENDER_ ковырялся в зубах, и тут мужественный _ATTACKER_, разбежавшись, рубанул правой ногой в область левой пятки."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "LEGS",
        "action": "ATTACK",
        "message": "_DEFENDER_ пришел в себя, но в это время продвинутый _ATTACKER_, улыбаясь, саданул укол правой ногой в область левой пятки."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "LEGS",
        "action": "ATTACK",
        "message": "_DEFENDER_ замешкался, и за это жестокий _ATTACKER_, разбежавшись, рубанул грудью по икрам."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "LEGS",
        "action": "ATTACK",
        "message": "_DEFENDER_ замешкался, и за это продвинутый _ATTACKER_, разбежавшись, рубанул ребром руки по икрам."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "LEGS",
        "action": "ATTACK",
        "message": "_DEFENDER_ пришел в себя, но в это время _ATTACKER_ ударил коленом по икрам."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "LEGS",
        "action": "ATTACK",
        "message": "_DEFENDER_ высморкался, и в это время _ATTACKER_ приложил удар грудью по икрам."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "LEGS",
        "action": "ATTACK",
        "message": "_DEFENDER_ забылся, и тут храбрый _ATTACKER_ нехотя уколол лбом в область правой пятки."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "LEGS",
        "action": "ATTACK",
        "message": "_DEFENDER_ замешкался, и за это расстроенный _ATTACKER_, улыбаясь, саданул укол правой ногой по ногам."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "LEGS",
        "action": "ATTACK",
        "message": "_DEFENDER_ думал не о том, и неустрашимый _ATTACKER_ ударил лбом по ногам."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "LEGS",
        "action": "ATTACK",
        "message": "_DEFENDER_ пришел в себя, но в это время _ATTACKER_ сдуру вмазал коленом по ногам."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "LEGS",
        "action": "ATTACK",
        "message": "_DEFENDER_ ковырялся в зубах, и тут _ATTACKER_ нехотя уколол коленом в область левой пятки."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "LEGS",
        "action": "ATTACK",
        "message": "_DEFENDER_ растерялся, как вдруг _ATTACKER_ ударил коленом в область правой пятки."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "LEGS",
        "action": "ATTACK",
        "message": "_DEFENDER_ думал не о том, и мужественный _ATTACKER_ приложил удар ребром руки по ногам."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "LEGS",
        "action": "ATTACK",
        "message": "_DEFENDER_ замешкался, и за это разъяренный _ATTACKER_ приложил удар правой ногой в область правой пятки."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "LEGS",
        "action": "ATTACK",
        "message": "_DEFENDER_ пришел в себя, но в это время злобный _ATTACKER_ не подумав, рубанул коленом по коленной чашечке."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "LEGS",
        "action": "ATTACK",
        "message": "_DEFENDER_ растерялся, как вдруг мужественный _ATTACKER_ отчаянно проткнул лбом в область левой пятки."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "LEGS",
        "action": "ATTACK",
        "message": "_DEFENDER_ пришел в себя, но в это время _ATTACKER_ не подумав, рубанул лбом в область правой пятки."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "LEGS",
        "action": "ATTACK",
        "message": "_DEFENDER_ поперхнулся, но вдруг расстроенный _ATTACKER_ ударил левой ногой по коленной чашечке."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "LEGS",
        "action": "ATTACK",
        "message": "_DEFENDER_ высморкался, и в это время мужественный _ATTACKER_, разбежавшись, рубанул ногой по коленной чашечке."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "LEGS",
        "action": "ATTACK",
        "message": "_DEFENDER_ растерялся, как вдруг хитрый _ATTACKER_ приложил удар коленом в область левой пятки."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "LEGS",
        "action": "ATTACK",
        "message": "_DEFENDER_ пришел в себя, но в это время наглый _ATTACKER_ отчаянно проткнул грудью в область левой пятки."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "LEGS",
        "action": "ATTACK",
        "message": "_DEFENDER_ ковырялся в зубах, и тут храбрый _ATTACKER_ приложил удар грудью в область левой пятки."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "LEGS",
        "action": "ATTACK",
        "message": "_DEFENDER_ растерялся, как вдруг _ATTACKER_ приложил удар левой ногой по коленной чашечке."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "LEGS",
        "action": "ATTACK",
        "message": "_DEFENDER_ растерялся, как вдруг _ATTACKER_ приложил удар коленом по коленной чашечке."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "LEGS",
        "action": "ATTACK",
        "message": "_DEFENDER_ думал не о том, и продвинутый _ATTACKER_, разбежавшись, рубанул лбом в область правой пятки."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "LEGS",
        "action": "ATTACK",
        "message": "_DEFENDER_ обернулся, как внезапно мужественный _ATTACKER_ сдуру вмазал ребром руки в область правой пятки."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "LEGS",
        "action": "ATTACK",
        "message": "_DEFENDER_ думал не о том, и наглый _ATTACKER_ ударил коленом в область правой пятки."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "LEGS",
        "action": "ATTACK",
        "message": "_DEFENDER_ высморкался, и в это время наглый _ATTACKER_ приложил удар кулаком по ногам."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "LEGS",
        "action": "ATTACK",
        "message": "_DEFENDER_ забылся, и тут злобный _ATTACKER_ не подумав, рубанул коленом по ногам."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "LEGS",
        "action": "ATTACK",
        "message": "_DEFENDER_ растерялся, как вдруг _ATTACKER_ отчаянно проткнул ногой в область правой пятки."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "LEGS",
        "action": "ATTACK",
        "message": "_DEFENDER_ пришел в себя, но в это время _ATTACKER_, улыбаясь, саданул укол правой ногой по ногам."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "LEGS",
        "action": "ATTACK",
        "message": "_DEFENDER_ высморкался, и в это время продвинутый _ATTACKER_, улыбаясь, саданул укол грудью по икрам."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "LEGS",
        "action": "ATTACK",
        "message": "_DEFENDER_ замешкался, и за это разъяренный _ATTACKER_, улыбаясь, саданул укол лбом по коленной чашечке."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "LEGS",
        "action": "ATTACK",
        "message": "_DEFENDER_ растерялся, как вдруг расстроенный _ATTACKER_, улыбаясь, саданул укол коленом по икрам."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "LEGS",
        "action": "ATTACK",
        "message": "_DEFENDER_ забылся, и тут мужественный _ATTACKER_ не подумав, рубанул грудью в область правой пятки."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "LEGS",
        "action": "ATTACK",
        "message": "_DEFENDER_ ковырялся в зубах, и тут хитрый _ATTACKER_ сдуру вмазал ребром руки по ногам."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "LEGS",
        "action": "ATTACK",
        "message": "_DEFENDER_ пытался что-то сказать но вдруг, неожиданно неустрашимый _ATTACKER_, улыбаясь, саданул укол грудью по ногам."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "LEGS",
        "action": "ATTACK",
        "message": "_DEFENDER_ забылся, и тут разъяренный _ATTACKER_ не подумав, рубанул грудью в область правой пятки."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "LEGS",
        "action": "ATTACK",
        "message": "_DEFENDER_ забылся, и тут расстроенный _ATTACKER_ не подумав, рубанул грудью по коленной чашечке."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "LEGS",
        "action": "ATTACK",
        "message": "_DEFENDER_ поперхнулся, но вдруг _ATTACKER_ сдуру вмазал левой ногой по коленной чашечке."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "LEGS",
        "action": "ATTACK",
        "message": "_DEFENDER_ засмотрелся на &lt;вырезано цензурой&gt;, а в это время продвинутый _ATTACKER_, разбежавшись, рубанул левой ногой в область левой пятки."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "LEGS",
        "action": "ATTACK",
        "message": "_DEFENDER_ ковырялся в зубах, и тут злобный _ATTACKER_ нехотя уколол коленом по коленной чашечке."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "LEGS",
        "action": "ATTACK",
        "message": "_DEFENDER_ поперхнулся, но вдруг мужественный _ATTACKER_ сдуру вмазал ногой по коленной чашечке."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "LEGS",
        "action": "ATTACK",
        "message": "_DEFENDER_ растерялся, как вдруг продвинутый _ATTACKER_ приложил удар ребром руки по ногам."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "LEGS",
        "action": "ATTACK",
        "message": "_DEFENDER_ засмотрелся на &lt;вырезано цензурой&gt;, а в это время бесчувственный _ATTACKER_ не подумав, рубанул ребром руки в область правой пятки."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "LEGS",
        "action": "ATTACK",
        "message": "_DEFENDER_ замешкался, и за это мужественный _ATTACKER_ нехотя уколол левой ногой в область правой пятки."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "LEGS",
        "action": "ATTACK",
        "message": "_DEFENDER_ пытался что-то сказать но вдруг, неожиданно жестокий _ATTACKER_ ударил левой ногой в область левой пятки."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "LEGS",
        "action": "ATTACK",
        "message": "_DEFENDER_ пришел в себя, но в это время обезумевший _ATTACKER_, разбежавшись, рубанул ребром руки в область левой пятки."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "LEGS",
        "action": "ATTACK",
        "message": "_DEFENDER_ растерялся, как вдруг продвинутый _ATTACKER_ ударил ребром руки по ногам."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "LEGS",
        "action": "ATTACK",
        "message": "_DEFENDER_ пытался что-то сказать но вдруг, неожиданно _ATTACKER_, разбежавшись, рубанул ногой в область левой пятки."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "LEGS",
        "action": "ATTACK",
        "message": "_DEFENDER_ пришел в себя, но в это время мужественный _ATTACKER_, улыбаясь, саданул укол лбом в область правой пятки."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "LEGS",
        "action": "ATTACK",
        "message": "_DEFENDER_ думал не о том, и неустрашимый _ATTACKER_, улыбаясь, саданул укол грудью в область правой пятки."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "LEGS",
        "action": "ATTACK",
        "message": "_DEFENDER_ обернулся, как внезапно храбрый _ATTACKER_ нехотя уколол кулаком по икрам."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "LEGS",
        "action": "ATTACK",
        "message": "_DEFENDER_ засмотрелся на &lt;вырезано цензурой&gt;, а в это время мужественный _ATTACKER_, разбежавшись, рубанул левой ногой по ногам."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "LEGS",
        "action": "ATTACK",
        "message": "_DEFENDER_ думал не о том, и мужественный _ATTACKER_, улыбаясь, саданул укол левой ногой по ногам."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "LEGS",
        "action": "ATTACK",
        "message": "_DEFENDER_ думал не о том, и мужественный _ATTACKER_ ударил левой ногой по коленной чашечке."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "LEGS",
        "action": "ATTACK",
        "message": "_DEFENDER_ пытался что-то сказать но вдруг, неожиданно наглый _ATTACKER_, разбежавшись, рубанул лбом по ногам."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "LEGS",
        "action": "ATTACK",
        "message": "_DEFENDER_ замешкался, и за это разъяренный _ATTACKER_ не подумав, рубанул левой ногой по икрам."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "LEGS",
        "action": "ATTACK",
        "message": "_DEFENDER_ пришел в себя, но в это время мужественный _ATTACKER_, улыбаясь, саданул укол левой ногой по икрам."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "LEGS",
        "action": "ATTACK",
        "message": "_DEFENDER_ обернулся, как внезапно бесчувственный _ATTACKER_ приложил удар коленом в область правой пятки."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "LEGS",
        "action": "ATTACK",
        "message": "_DEFENDER_ пытался что-то сказать но вдруг, неожиданно _ATTACKER_, разбежавшись, рубанул левой ногой по коленной чашечке."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "LEGS",
        "action": "ATTACK",
        "message": "_DEFENDER_ забылся, и тут храбрый _ATTACKER_, разбежавшись, рубанул ногой в область правой пятки."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "LEGS",
        "action": "ATTACK",
        "message": "_DEFENDER_ пытался что-то сказать но вдруг, неожиданно храбрый _ATTACKER_ ударил коленом по коленной чашечке."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "LEGS",
        "action": "ATTACK",
        "message": "_DEFENDER_ высморкался, и в это время неустрашимый _ATTACKER_ отчаянно проткнул кулаком в область левой пятки."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "LEGS",
        "action": "ATTACK",
        "message": "_DEFENDER_ замешкался, и за это злобный _ATTACKER_, разбежавшись, рубанул кулаком по ногам."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "LEGS",
        "action": "ATTACK",
        "message": "_DEFENDER_ растерялся, как вдруг разъяренный _ATTACKER_ сдуру вмазал лбом по коленной чашечке."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "LEGS",
        "action": "ATTACK",
        "message": "_DEFENDER_ думал не о том, и расстроенный _ATTACKER_ приложил удар левой ногой по коленной чашечке."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "LEGS",
        "action": "ATTACK",
        "message": "_DEFENDER_ ковырялся в зубах, и тут хитрый _ATTACKER_ приложил удар лбом по икрам."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "LEGS",
        "action": "ATTACK",
        "message": "_DEFENDER_ замешкался, и за это разъяренный _ATTACKER_, разбежавшись, рубанул ногой в область левой пятки."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "LEGS",
        "action": "ATTACK",
        "message": "_DEFENDER_ засмотрелся на &lt;вырезано цензурой&gt;, а в это время неустрашимый _ATTACKER_ ударил коленом по икрам."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "LEGS",
        "action": "ATTACK",
        "message": "_DEFENDER_ пришел в себя, но в это время злобный _ATTACKER_ ударил грудью по ногам."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "LEGS",
        "action": "ATTACK",
        "message": "_DEFENDER_ ковырялся в зубах, и тут _ATTACKER_, разбежавшись, рубанул левой ногой по икрам."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "LEGS",
        "action": "ATTACK",
        "message": "_DEFENDER_ забылся, и тут злобный _ATTACKER_ не подумав, рубанул ребром руки по ногам."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "LEGS",
        "action": "ATTACK",
        "message": "_DEFENDER_ засмотрелся на &lt;вырезано цензурой&gt;, а в это время бесчувственный _ATTACKER_, улыбаясь, саданул укол коленом по икрам."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "LEGS",
        "action": "ATTACK",
        "message": "_DEFENDER_ замешкался, и за это разъяренный _ATTACKER_, улыбаясь, саданул укол левой ногой в область правой пятки."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "LEGS",
        "action": "BLOCK",
        "message": "_ATTACKER_ думал не о том, и хитрый _DEFENDER_ отбил удар правой ногой по ногам."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "LEGS",
        "action": "BLOCK",
        "message": "_ATTACKER_ старался провести удар, но злобный _DEFENDER_ отбил удар лбом в область правой пятки."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "LEGS",
        "action": "BLOCK",
        "message": "_ATTACKER_ закашлялся, и жестокий _DEFENDER_ отбил удар коленом в область левой пятки."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "LEGS",
        "action": "BLOCK",
        "message": "_ATTACKER_ закашлялся, и _DEFENDER_ заблокировал удар правой ногой по коленной чашечке."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "LEGS",
        "action": "BLOCK",
        "message": "_ATTACKER_ старался провести удар, но расстроенный _DEFENDER_ остановил удар ногой в область правой пятки."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "LEGS",
        "action": "BLOCK",
        "message": "_ATTACKER_ пытался нанести удар, но злобный _DEFENDER_ заблокировал удар ребром руки по ногам."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "LEGS",
        "action": "BLOCK",
        "message": "_ATTACKER_ пытался нанести удар, но неустрашимый _DEFENDER_ отбил удар кулаком по ногам."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "LEGS",
        "action": "BLOCK",
        "message": "_ATTACKER_ пытался нанести удар, но _DEFENDER_ заблокировал удар коленом по коленной чашечке."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "LEGS",
        "action": "BLOCK",
        "message": "_ATTACKER_ пытался провести удар, но жестокий _DEFENDER_ отбил удар лбом в область правой пятки."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "LEGS",
        "action": "BLOCK",
        "message": "_ATTACKER_ потерял самоконтроль, вследствие чего продвинутый _DEFENDER_ остановил удар ногой по ногам."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "LEGS",
        "action": "BLOCK",
        "message": "_ATTACKER_ закашлялся, и наглый _DEFENDER_ остановил удар ногой по икрам."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "LEGS",
        "action": "BLOCK",
        "message": "_ATTACKER_ закашлялся, и неустрашимый _DEFENDER_ заблокировал удар левой ногой по икрам."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "LEGS",
        "action": "BLOCK",
        "message": "_ATTACKER_ закашлялся, и бесчувственный _DEFENDER_ остановил удар правой ногой по икрам."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "LEGS",
        "action": "BLOCK",
        "message": "_ATTACKER_ закашлялся, и злобный _DEFENDER_ отбил удар коленом по икрам."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "LEGS",
        "action": "BLOCK",
        "message": "_ATTACKER_ пытался нанести удар, но наглый _DEFENDER_ отбил удар левой ногой по ногам."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "LEGS",
        "action": "BLOCK",
        "message": "_ATTACKER_ потерял самоконтроль, вследствие чего храбрый _DEFENDER_ отбил удар левой ногой по коленной чашечке."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "LEGS",
        "action": "BLOCK",
        "message": "_ATTACKER_ закашлялся, и _DEFENDER_ заблокировал удар кулаком по ногам."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "LEGS",
        "action": "BLOCK",
        "message": "_ATTACKER_ подскользнулся, и мужественный _DEFENDER_ отбил удар ногой по ногам."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "LEGS",
        "action": "BLOCK",
        "message": "_ATTACKER_ старался провести удар, но мужественный _DEFENDER_ заблокировал удар лбом по икрам."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "LEGS",
        "action": "BLOCK",
        "message": "_ATTACKER_ подскользнулся, и хитрый _DEFENDER_ заблокировал удар ребром руки по коленной чашечке."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "LEGS",
        "action": "BLOCK",
        "message": "_ATTACKER_ закашлялся, и разъяренный _DEFENDER_ заблокировал удар ребром руки в область левой пятки."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "LEGS",
        "action": "BLOCK",
        "message": "_ATTACKER_ пытался провести удар, но продвинутый _DEFENDER_ заблокировал удар правой ногой по икрам."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "LEGS",
        "action": "BLOCK",
        "message": "_ATTACKER_ думал о &lt;вырезано цензурой&gt;, вследствие чего храбрый _DEFENDER_ остановил удар грудью по икрам."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "LEGS",
        "action": "BLOCK",
        "message": "_ATTACKER_ думал не о том, и храбрый _DEFENDER_ заблокировал удар грудью в область левой пятки."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "LEGS",
        "action": "BLOCK",
        "message": "_ATTACKER_ думал не о том, и обезумевший _DEFENDER_ остановил удар лбом по коленной чашечке."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "LEGS",
        "action": "BLOCK",
        "message": "_ATTACKER_ думал не о том, и храбрый _DEFENDER_ отбил удар ребром руки в область левой пятки."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "LEGS",
        "action": "BLOCK",
        "message": "_ATTACKER_ думал не о том, и _DEFENDER_ отбил удар лбом в область левой пятки."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "LEGS",
        "action": "BLOCK",
        "message": "_ATTACKER_ подскользнулся, и неустрашимый _DEFENDER_ остановил удар ногой в область правой пятки."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "LEGS",
        "action": "BLOCK",
        "message": "_ATTACKER_ старался провести удар, но неустрашимый _DEFENDER_ заблокировал удар грудью по ногам."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "LEGS",
        "action": "BLOCK",
        "message": "_ATTACKER_ старался провести удар, но продвинутый _DEFENDER_ отбил удар правой ногой по ногам."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "LEGS",
        "action": "BLOCK",
        "message": "_ATTACKER_ подскользнулся, и наглый _DEFENDER_ отбил удар левой ногой по коленной чашечке."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "LEGS",
        "action": "BLOCK",
        "message": "_ATTACKER_ потерял самоконтроль, вследствие чего расстроенный _DEFENDER_ отбил удар лбом по ногам."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "LEGS",
        "action": "BLOCK",
        "message": "_ATTACKER_ думал не о том, и жестокий _DEFENDER_ заблокировал удар кулаком по коленной чашечке."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "LEGS",
        "action": "BLOCK",
        "message": "_ATTACKER_ пытался нанести удар, но _DEFENDER_ заблокировал удар кулаком в область левой пятки."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "LEGS",
        "action": "BLOCK",
        "message": "_ATTACKER_ закашлялся, и неустрашимый _DEFENDER_ заблокировал удар грудью в область правой пятки."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "LEGS",
        "action": "BLOCK",
        "message": "_ATTACKER_ думал не о том, и мужественный _DEFENDER_ отбил удар коленом в область правой пятки."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "LEGS",
        "action": "BLOCK",
        "message": "_ATTACKER_ подскользнулся, и мужественный _DEFENDER_ остановил удар ребром руки по ногам."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "LEGS",
        "action": "BLOCK",
        "message": "_ATTACKER_ думал о &lt;вырезано цензурой&gt;, вследствие чего продвинутый _DEFENDER_ остановил удар ребром руки в область левой пятки."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "LEGS",
        "action": "BLOCK",
        "message": "_ATTACKER_ пытался нанести удар, но продвинутый _DEFENDER_ отбил удар ребром руки в область левой пятки."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "LEGS",
        "action": "BLOCK",
        "message": "_ATTACKER_ думал не о том, и храбрый _DEFENDER_ остановил удар кулаком по коленной чашечке."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "LEGS",
        "action": "BLOCK",
        "message": "_ATTACKER_ подскользнулся, и мужественный _DEFENDER_ заблокировал удар левой ногой по икрам."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "LEGS",
        "action": "BLOCK",
        "message": "_ATTACKER_ думал о &lt;вырезано цензурой&gt;, вследствие чего неустрашимый _DEFENDER_ заблокировал удар грудью в область левой пятки."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "LEGS",
        "action": "BLOCK",
        "message": "_ATTACKER_ потерял самоконтроль, вследствие чего храбрый _DEFENDER_ заблокировал удар левой ногой по коленной чашечке."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "LEGS",
        "action": "BLOCK",
        "message": "_ATTACKER_ пытался провести удар, но храбрый _DEFENDER_ заблокировал удар ногой в область левой пятки."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "LEGS",
        "action": "BLOCK",
        "message": "_ATTACKER_ думал о &lt;вырезано цензурой&gt;, вследствие чего расстроенный _DEFENDER_ остановил удар правой ногой по ногам."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "LEGS",
        "action": "BLOCK",
        "message": "_ATTACKER_ потерял самоконтроль, вследствие чего жестокий _DEFENDER_ отбил удар ребром руки в область правой пятки."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "LEGS",
        "action": "BLOCK",
        "message": "_ATTACKER_ думал о &lt;вырезано цензурой&gt;, вследствие чего храбрый _DEFENDER_ остановил удар левой ногой по коленной чашечке."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "LEGS",
        "action": "BLOCK",
        "message": "_ATTACKER_ потерял самоконтроль, вследствие чего бесчувственный _DEFENDER_ остановил удар левой ногой по икрам."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "LEGS",
        "action": "BLOCK",
        "message": "_ATTACKER_ потерял самоконтроль, вследствие чего _DEFENDER_ отбил удар кулаком по коленной чашечке."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "LEGS",
        "action": "BLOCK",
        "message": "_ATTACKER_ подскользнулся, и мужественный _DEFENDER_ остановил удар ногой по коленной чашечке."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "LEGS",
        "action": "BLOCK",
        "message": "_ATTACKER_ пытался провести удар, но разъяренный _DEFENDER_ остановил удар ногой по коленной чашечке."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "LEGS",
        "action": "BLOCK",
        "message": "_ATTACKER_ подскользнулся, и злобный _DEFENDER_ заблокировал удар лбом в область левой пятки."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "LEGS",
        "action": "BLOCK",
        "message": "_ATTACKER_ думал не о том, и разъяренный _DEFENDER_ остановил удар грудью в область правой пятки."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "LEGS",
        "action": "BLOCK",
        "message": "_ATTACKER_ думал о &lt;вырезано цензурой&gt;, вследствие чего обезумевший _DEFENDER_ остановил удар правой ногой в область левой пятки."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "LEGS",
        "action": "BLOCK",
        "message": "_ATTACKER_ подскользнулся, и _DEFENDER_ заблокировал удар грудью по икрам."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "LEGS",
        "action": "BLOCK",
        "message": "_ATTACKER_ старался провести удар, но храбрый _DEFENDER_ отбил удар правой ногой в область правой пятки."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "LEGS",
        "action": "BLOCK",
        "message": "_ATTACKER_ пытался провести удар, но _DEFENDER_ остановил удар коленом по икрам."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "LEGS",
        "action": "BLOCK",
        "message": "_ATTACKER_ пытался провести удар, но бесчувственный _DEFENDER_ отбил удар ногой в область правой пятки."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "LEGS",
        "action": "BLOCK",
        "message": "_ATTACKER_ думал не о том, и неустрашимый _DEFENDER_ остановил удар кулаком в область левой пятки."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "LEGS",
        "action": "BLOCK",
        "message": "_ATTACKER_ пытался провести удар, но наглый _DEFENDER_ остановил удар коленом по ногам."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "LEGS",
        "action": "BLOCK",
        "message": "_ATTACKER_ закашлялся, и бесчувственный _DEFENDER_ заблокировал удар грудью по ногам."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "LEGS",
        "action": "BLOCK",
        "message": "_ATTACKER_ подскользнулся, и злобный _DEFENDER_ отбил удар кулаком по икрам."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "LEGS",
        "action": "BLOCK",
        "message": "_ATTACKER_ закашлялся, и мужественный _DEFENDER_ заблокировал удар ногой по икрам."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "LEGS",
        "action": "BLOCK",
        "message": "_ATTACKER_ думал о &lt;вырезано цензурой&gt;, вследствие чего продвинутый _DEFENDER_ заблокировал удар правой ногой по ногам."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "LEGS",
        "action": "BLOCK",
        "message": "_ATTACKER_ старался провести удар, но разъяренный _DEFENDER_ отбил удар ногой в область правой пятки."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "LEGS",
        "action": "BLOCK",
        "message": "_ATTACKER_ думал о &lt;вырезано цензурой&gt;, вследствие чего _DEFENDER_ заблокировал удар коленом по ногам."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "LEGS",
        "action": "BLOCK",
        "message": "_ATTACKER_ подскользнулся, и злобный _DEFENDER_ отбил удар правой ногой по коленной чашечке."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "LEGS",
        "action": "BLOCK",
        "message": "_ATTACKER_ пытался провести удар, но расстроенный _DEFENDER_ заблокировал удар правой ногой по икрам."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "LEGS",
        "action": "BLOCK",
        "message": "_ATTACKER_ подскользнулся, и хитрый _DEFENDER_ остановил удар ногой по коленной чашечке."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "LEGS",
        "action": "BLOCK",
        "message": "_ATTACKER_ потерял самоконтроль, вследствие чего жестокий _DEFENDER_ остановил удар правой ногой в область левой пятки."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "LEGS",
        "action": "BLOCK",
        "message": "_ATTACKER_ старался провести удар, но мужественный _DEFENDER_ отбил удар грудью в область левой пятки."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "LEGS",
        "action": "BLOCK",
        "message": "_ATTACKER_ старался провести удар, но неустрашимый _DEFENDER_ заблокировал удар ногой по икрам."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "LEGS",
        "action": "BREAK_THROUGH_BLOCK",
        "message": "_DEFENDER_ обернулся, как внезапно хитрый _ATTACKER_, пробив блок, ласково заломил руку за спину соперника, ударил в область правой пятки."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "LEGS",
        "action": "BREAK_THROUGH_BLOCK",
        "message": "_DEFENDER_ засмотрелся на &lt;вырезано цензурой&gt;, а в это время храбрый _ATTACKER_, пробив блок, провел ужасный бросок через пупок оппонента, ударил по левой ноге."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "LEGS",
        "action": "BREAK_THROUGH_BLOCK",
        "message": "_DEFENDER_ растерялся, как вдруг _ATTACKER_, пробив блок, наступил на ногу врага, ударил по коленной чашечке."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "LEGS",
        "action": "BREAK_THROUGH_BLOCK",
        "message": "_DEFENDER_ ковырялся в зубах, и тут злобный _ATTACKER_, пробив блок, укусил в нос противника, ударил в область левой пятки."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "LEGS",
        "action": "BREAK_THROUGH_BLOCK",
        "message": "_DEFENDER_ пытался что-то сказать но вдруг, неожиданно разъяренный _ATTACKER_, напугав всех, неслышно подойдя сзади ударил, пробив блок, булыжником по икрам."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "LEGS",
        "action": "BREAK_THROUGH_BLOCK",
        "message": "_DEFENDER_ думал не о том, и расстроенный _ATTACKER_, пробив блок, ласково заломил руку за спину соперника, ударил по коленной чашечке."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "LEGS",
        "action": "BREAK_THROUGH_BLOCK",
        "message": "_DEFENDER_ ковырялся в зубах, и тут злобный _ATTACKER_, напугав всех, неслышно подойдя сзади ударил, пробив блок, булыжником по ногам."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "LEGS",
        "action": "BREAK_THROUGH_BLOCK",
        "message": "_DEFENDER_ поперхнулся, но вдруг наглый _ATTACKER_, пробив блок, расцарапал нос соперника, ударил по икрам."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "LEGS",
        "action": "BREAK_THROUGH_BLOCK",
        "message": "_DEFENDER_ забылся, и тут _ATTACKER_, сказав &quot;БУ!&quot;, ласково заломил руку за спину соперника, пробив блок, ударил в область правой пятки."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "LEGS",
        "action": "BREAK_THROUGH_BLOCK",
        "message": "_DEFENDER_ пришел в себя, но в это время _ATTACKER_, проклиная этот сайт, провел ужасный бросок через пупок оппонента, пробив блок, попал по ногам."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "LEGS",
        "action": "BREAK_THROUGH_BLOCK",
        "message": "_DEFENDER_ пытался что-то сказать но вдруг, неожиданно обезумевший _ATTACKER_, показав сразу два пальца, пробив блок, наступил на ногу врага и ударил в область правой пятки."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "LEGS",
        "action": "CRITICAL",
        "message": "_DEFENDER_ пытался что-то сказать но вдруг, неожиданно обезумевший _ATTACKER_, показав сразу два пальца, наступил на ногу врага и ударил в область правой пятки."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "LEGS",
        "action": "CRITICAL",
        "message": "_DEFENDER_ забылся, и тут хитрый _ATTACKER_, напугав всех, неслышно подойдя сзади ударил булыжником оппонента в область правой пятки."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "LEGS",
        "action": "CRITICAL",
        "message": "_DEFENDER_ пытался что-то сказать но вдруг, неожиданно наглый _ATTACKER_, напугав всех, неслышно подойдя сзади ударил булыжником оппонента по коленной чашечке."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "LEGS",
        "action": "CRITICAL",
        "message": "_DEFENDER_ замешкался, и за это _ATTACKER_, расслабившись, расцарапал нос соперника и ударил в область левой пятки."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "LEGS",
        "action": "CRITICAL",
        "message": "_DEFENDER_ забылся, и тут _ATTACKER_, сказав &quot;БУ!&quot;, ласково заломил руку за спину соперника, ударил в область правой пятки."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "LEGS",
        "action": "CRITICAL",
        "message": "_DEFENDER_ пришел в себя, но в это время _ATTACKER_, проклиная этот сайт, провел ужасный бросок через пупок оппонента, попал по ногам."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "LEGS",
        "action": "CRITICAL",
        "message": "_DEFENDER_ замешкался, и за это жестокий _ATTACKER_, сказав &quot;БУ!&quot;, ласково заломил руку за спину соперника, ударил в область левой пятки."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "LEGS",
        "action": "CRITICAL",
        "message": "_DEFENDER_ обернулся, как внезапно храбрый _ATTACKER_, напугав всех, неслышно подойдя сзади ударил булыжником оппонента по ногам."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "LEGS",
        "action": "CRITICAL",
        "message": "_DEFENDER_ ковырялся в зубах, и тут наглый _ATTACKER_, показав сразу два пальца, наступил на ногу врага и ударил в область левой пятки."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "LEGS",
        "action": "CRITICAL",
        "message": "_DEFENDER_ засмотрелся на &lt;вырезано цензурой&gt;, а в это время хитрый _ATTACKER_, напугав всех, неслышно подойдя сзади ударил булыжником оппонента по ногам."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "LEGS",
        "action": "CRITICAL",
        "message": "_DEFENDER_ замешкался, и за это разъяренный _ATTACKER_, расслабившись, расцарапал нос соперника и ударил в область левой пятки."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "LEGS",
        "action": "CRITICAL",
        "message": "_DEFENDER_ пытался что-то сказать но вдруг, неожиданно продвинутый _ATTACKER_, показав сразу два пальца, наступил на ногу врага и ударил по ногам."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "LEGS",
        "action": "DODGE",
        "message": "_ATTACKER_ пытался провести удар, но бесчувственный _DEFENDER_, отпрыгнул от удара правой ногой по икрам."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "LEGS",
        "action": "DODGE",
        "message": "_ATTACKER_ думал не о том, и расстроенный _DEFENDER_, уклонился от удара кулаком по коленной чашечке."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "LEGS",
        "action": "DODGE",
        "message": "_ATTACKER_ пытался провести удар, но продвинутый _DEFENDER_, уклонился от удара левой ногой в область левой пятки."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "LEGS",
        "action": "DODGE",
        "message": "_ATTACKER_ потерял самоконтроль, вследствие чего мужественный _DEFENDER_, увернулся от удара лбом в область левой пятки."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "LEGS",
        "action": "DODGE",
        "message": "_ATTACKER_ пытался нанести удар, но мужественный _DEFENDER_, отпрыгнул от удара лбом по икрам."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "LEGS",
        "action": "DODGE",
        "message": "_ATTACKER_ пытался провести удар, но продвинутый _DEFENDER_, отскочил от удара грудью по коленной чашечке."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "LEGS",
        "action": "DODGE",
        "message": "_ATTACKER_ закашлялся, и разъяренный _DEFENDER_, уклонился от удара кулаком в область правой пятки."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "LEGS",
        "action": "DODGE",
        "message": "_ATTACKER_ потерял самоконтроль, вследствие чего неустрашимый _DEFENDER_, отпрыгнул от удара коленом по икрам."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "LEGS",
        "action": "DODGE",
        "message": "_ATTACKER_ пытался нанести удар, но хитрый _DEFENDER_, отпрыгнул от удара ребром руки в область правой пятки."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "LEGS",
        "action": "DODGE",
        "message": "_ATTACKER_ закашлялся, и _DEFENDER_, отскочил от удара головой по икрам."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "LEGS",
        "action": "DODGE",
        "message": "_ATTACKER_ закашлялся, и продвинутый _DEFENDER_, уклонился от удара правой ногой по икрам."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "LEGS",
        "action": "DODGE",
        "message": "_ATTACKER_ пытался провести удар, но _DEFENDER_, уклонился от левой пяткой по икрам."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "LEGS",
        "action": "DODGE",
        "message": "_ATTACKER_ старался провести удар, но мужественный _DEFENDER_, увернулся от удара копчиком по икрам."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "LEGS",
        "action": "DODGE",
        "message": "_ATTACKER_ закашлялся, и _DEFENDER_, уклонился от удара грудью по ногам."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "LEGS",
        "action": "DODGE",
        "message": "_ATTACKER_ пытался нанести удар, но обезумевший _DEFENDER_, увернулся от удара ребром руки по икрам."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "LEGS",
        "action": "DODGE",
        "message": "_ATTACKER_ старался провести удар, но неустрашимый _DEFENDER_, отпрыгнул от удара кулаком в область правой пятки."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "LEGS",
        "action": "DODGE",
        "message": "_ATTACKER_ поскользнулся, и бесчувственный _DEFENDER_, отскочил от удара локтем в область правой пятки."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "LEGS",
        "action": "DODGE",
        "message": "_ATTACKER_ старался провести удар, но бесчувственный _DEFENDER_, уклонился от удара коленом по икрам."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "LEGS",
        "action": "DODGE",
        "message": "_ATTACKER_ старался провести удар, но бесчувственный _DEFENDER_, уклонился от удара головой по икрам."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "LEGS",
        "action": "DODGE",
        "message": "_ATTACKER_ закашлялся, и неустрашимый _DEFENDER_, увернулся от удара коленом в область левой пятки."
    },
    {
        "battle_type": "FISTFUL",
        "body_part": "LEGS",
        "action": "DODGE",
        "message": "_ATTACKER_ поскользнулся, и разъяренный _DEFENDER_, увернулся от удара ребром руки в область левой пятки."
    },
    {
        "action": "DEAD",
        "message": "_LOGIN_ пал в бою!"
    },
    {
        "action": "DEAD",
        "message": "_LOGIN_ отошел в мир иной!"
    },
    {
        "action": "DEAD",
        "message": "_LOGIN_ умер!"
    },
    {
        "action": "DEAD",
        "message": "_LOGIN_ погиб!"
    },
];
