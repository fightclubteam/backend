'use strict';

module.exports = [
    {
        slug: 'STUDENT',
        position: 0,
        title: 'Ученик',
    },
    {
        slug: 'WARRIOR',
        position: 20,
        title: 'Воин',
    },
    {
        slug: 'ELITE_WARRIOR',
        position: 40,
        title: 'Элитный воин',
    },
    {
        slug: 'MINISTER',
        position: 60,
        title: 'Служитель',
    },
    {
        slug: 'WATCHMAN',
        position: 80,
        title: 'Хранитель',
    },
];
