'use strict';

const _ = require('lodash');
const locations = require('./LocationSeed');
const userRanks = require('./UserRankSeed');
const location = _.find(locations, { name: 'CENTRAL_SQUARE' });

module.exports = [
    {
        email: 'admin@bk.ru',
        login: 'Администратор',
        name: 'Ivan Zubok',
        role: 'ADMIN',
        sex: 'MALE',
        register_ip: '::1',
        birthday: new Date(1990, 2, 2),
        password: '123123',
        rank: userRanks[3].slug,
        dinars: 100000,
        location,
    },
    {
        email: 'username@bk.ru',
        login: 'Username',
        name: 'User Name',
        role: 'USER',
        sex: 'MALE',
        register_ip: '::1',
        birthday: new Date(1990, 2, 2),
        password: '123123',
        rank: userRanks[4].slug,
        dinars: 100000,
        location,
    },
    {
        email: 'tankist@bk.ru',
        login: 'tankist',
        name: 'Viktor Gryshko',
        role: 'USER',
        sex: 'MALE',
        register_ip: '::1',
        birthday: new Date(1990, 2, 2),
        password: '123123',
        rank: userRanks[1].slug,
        dinars: 100000,
        location,
    },
    {
        email: 'leron0000@gmail.com',
        login: 'Leron',
        name: 'Max Leron',
        role: 'USER',
        sex: 'MALE',
        register_ip: '::1',
        birthday: new Date(1990, 2, 2),
        password: 'leron7272',
        rank: userRanks[1].slug,
        dinars: 100000,
        location,
    },
];
