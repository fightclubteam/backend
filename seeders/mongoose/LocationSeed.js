'use strict';

const _ = require('lodash');
const faker = require('faker');

function getLoremObject() {
    return {
        name: faker.lorem.slug(2),
        label: faker.lorem.words(2),
        image: faker.image.imageUrl(),
        coordinates: _.times(3, faker.random.number).join(','),
    };
}

const locationObjects = require('./LocationObjectSeed');

const locations = [
    {
        name: 'CENTRAL_SQUARE',
        label: 'Центральная площадь',
        image: 'http://img.likebk.com/loca/cpnews/day/cplikebk.jpg',
        can_attack: true,
        can_create_fight: true,
        is_default: true,
        objects: locationObjects,
        nearby_locations: [],
    },
    {
        name: 'JUSTICE_STREET',
        label: 'Улица Правосудия',
        image: 'http://img.likebk.com/loca/cpnews/forest/cp_park5.jpg',
        can_attack: false,
        can_create_fight: false,
        is_default: false,
        objects: [
            getLoremObject(),
        ],
        nearby_locations: [{
            name: 'CENTRAL_SQUARE',
            label: 'Центральная площадь',
            image: 'http://img.likebk.com/loca/cpnews/day/cplikebk.jpg',
            can_attack: true,
            can_create_fight: true,
            is_default: true,
        }],
    },
    {
        name: 'TOURNAMENT_LOCATION',
        label: 'Турнирная локация',
        image: 'http://img.likebk.com/loca/cpnews/cp_str/cpstrlikebk.jpg',
        can_attack: false,
        can_create_fight: false,
        is_default: false,
        objects: [
            getLoremObject(),
        ],
        nearby_locations: [],
    },
    {
        name: 'CASTLE_AREA',
        label: 'Замковая Площадь',
        image: 'http://oldbk.ru/street/img/d_59.png',
        can_attack: false,
        can_create_fight: false,
        is_default: false,
        objects: [
            getLoremObject(),
        ],
        nearby_locations: [],
    },
    {
        name: 'SCARECROW_STREET',
        label: 'Страшилкина Улица',
        image: 'http://img.likebk.com/loca/cpnews/cp_str/cpstrlikebk.jpg',
        can_attack: false,
        can_create_fight: false,
        is_default: false,
        objects: [
            getLoremObject(),
        ],
        nearby_locations: [],
    },
    {
        name: 'MAIN_PORTAL',
        label: 'Главный портал',
        image: 'http://img.combats.ru/i/images/300x225/dream_chat.jpg',
        can_attack: false,
        can_create_fight: false,
        is_default: false,
        objects: [
            getLoremObject(),
        ],
        nearby_locations: [],
    },

    {
        name: 'PARK_STREET',
        label: 'Парковая Улица',
        image: 'http://oldbk.ru/street/img/d_19.png',
        can_attack: false,
        can_create_fight: false,
        is_default: false,
        objects: [
            getLoremObject(),
        ],
        nearby_locations: [],
    },
    {
        name: 'RESERVE',
        label: 'Заповедник',
        image: 'http://img.likebk.com/loca/cpnews/forest/cp_park6.jpg',
        can_attack: false,
        can_create_fight: false,
        is_default: false,
        objects: [
            getLoremObject(),
        ],
        nearby_locations: [],
    },
    {
        name: 'FORGOTTEN_CAVES_STREET',
        label: 'Улица Забытых Пещер',
        image: 'http://img.combats.ru/i/images/300x225/dream_guild.jpg',
        can_attack: false,
        can_create_fight: false,
        is_default: false,
        objects: [
            getLoremObject(),
        ],
        nearby_locations: [],
    },
];

module.exports = locations;