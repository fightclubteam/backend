'use strict';

module.exports = [
    {
        name: 'FIGHT_CLUB',
        label: 'Бойцовский Клуб',
        image: 'http://img.likebk.com/loca/cpnews/day/bk.png',
        coordinates: '163,20',
    },
    {
        name: 'STATE_STORE',
        label: 'Государственный магазин',
        image: 'http://img.likebk.com/loca/cpnews/day/shop.png',
        coordinates: '95,155',
    },
    {
        name: 'POST',
        label: 'Почта',
        image: 'http://img.likebk.com/loca/cpnews/day/post.png',
        coordinates: '455,105',
    },
    {
        name: 'REPAIR_SHOP',
        label: 'Ремонтная мастерская',
        image: 'http://img.likebk.com/loca/cpnews/day/remont.png',
        coordinates: '180,130,5',
    },
    {
        name: 'COMMISSION_SHOP',
        label: 'Комиссионный магазин',
        image: 'http://img.likebk.com/loca/cpnews/day/commision.png',
        coordinates: '35,130',
    },
];