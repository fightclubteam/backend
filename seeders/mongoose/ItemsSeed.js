'use strict';

const _ = require('lodash');
const faker = require('faker');

const types = [
    'HELMET',
    'BRACELET',
    'AXE',
    'SWORD',
    'KNIFE',
    'MACE',
    'SHIRT',
    'ARMOR',
    'CLOAK',
    'BELT',
    'EARRINGS',
    'AMULET',
    'RING',
    'GLOVES',
    'KNIFE_LEFT',
    'SHIELD',
    'LEGGINGS',
    'BOOTS',
];

faker.locale = 'ru';

const abilities = [
    'STRENGTH',
    'AGILITY',
    'INTUITION',
    'CRIT',
    'ANTICRITICAL',
    'DODGING',
    'ANTIDODGE',
    'ARMOR',
    'HP',
    'DAMAGE',
    'POWER',
];

let items = [];

for (let i = 0; i < 100; i++) {
    items.push({
        title: faker.helpers.mustache('{{adjective}} {{abbreviation}} {{noun}}', {
            abbreviation: faker.hacker.abbreviation,
            adjective: faker.hacker.adjective,
            ingverb: faker.hacker.ingverb,
            noun: faker.hacker.noun,
            verb: faker.hacker.verb
        }),
        weight: faker.finance.amount(),
        created_at: new Date(),
        updated_at: new Date(),
        abilities: _.times(_.random(1, 5), function () {
            return {
                type: faker.random.arrayElement(abilities),
                value: faker.random.number(),
            };
        }),
        price: faker.finance.amount(),
        restrictions: {
            minUserRank: faker.random.number(10),
            minUserLevel: faker.random.number(10),
        },
        durability: {
            current: faker.random.number(100),
            max: 100,
        },
        item_type: faker.random.arrayElement(types),
        image: '/static/store/' + faker.random.number(115) + '.jpg',
        armor: {
            head: faker.random.number(100),
            chest: faker.random.number(100),
            abdomen: faker.random.number(100),
            girdle: faker.random.number(100),
            legs: faker.random.number(100),
        },
    });
}

module.exports = items;
