'use strict';

const _ = require('lodash');
const faker = require('faker');
const slug = require('slug');

const items = require('./ItemsSeed');

function getRandomItems(count) {
    return _.map(_.shuffle(items).slice(0, count), function (item) {
        return {
            item,
            quantity: faker.random.number(100),
        };
    })
}

module.exports = [
    {
        title: 'Main Store',
        slug: slug('main'),
        tax: 0.5,
        // items: getRandomItems(faker.random.number(100)),
    },
    {
        title: 'Test Store',
        slug: slug('test'),
        tax: 0,
        // items: getRandomItems(faker.random.number(100)),
    },
];
