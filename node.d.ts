/// <reference types="express" />
import { IUser } from './modules/user/interfaces/user';

declare module 'express' {
    interface IAclRole {
        getRoles(): string[];
    }

    interface IAclResource {
        getResourceId(): string;
    }

    // tslint:disable-next-line
    interface Acl {
        // readonly TYPE_ALLOW: 'TYPE_ALLOW';
        // readonly TYPE_DENY: 'TYPE_DENY';
        // readonly OP_ADD: 'OP_ADD';
        // readonly OP_REMOVE: 'OP_REMOVE';
        // readonly USER_IDENTITY_ROLE: 'acl.role';

        setUserIdentity(identity?: IAclRole): void;
        getUserIdentity(): null | IAclRole;
        isAllowed(role: string, resource?: IAclResource, privilege?: string): boolean;
        can(resource?: IAclResource, privilege?: string): boolean;
    }

    interface IJWTUser {
        sub: string;
        aud: string;
        exp: number;
        jti: string;
        iat: number;
    }

    // tslint:disable-next-line
    export interface Request {
        acl?: Acl;
        user?: IUser;
        jwt?: IJWTUser;
        clientIp?: string;
    }
}
