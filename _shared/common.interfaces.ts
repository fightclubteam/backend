'use strict';

export interface IAclResource {
    /**
     * Returns the string identifier of the Resource
     */
    getResourceId(): string;
}

export interface IAclRole {
    /**
     * Returns array of the Role string identifiers
     */
    getRoles: string[];
}

export interface IFindableByIdOrFail<TInstance> {
    findByIdOrFail(id: string): Promise<TInstance | null>;
}

export interface IPagination {
    totalItemsCount: number;
    totalPages: number;
    page: number;
    itemsPerPage: number;
}

export interface IPaginationCriteria {
    populate: object | any | null;
}
