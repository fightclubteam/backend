'use strict';

import * as _ from 'lodash';

/**
 * Returns array of integers ...
 *
 * @param {number} x
 * @param {number} y
 *
 * @returns {number[]}
 */
export function uniformDistributionIntegers(x: number, y: number = 100) {
    const a = Array(y);
    if (x <= 0) {
        return _.fill(a, 0);
    }

    const f = Math.floor(x / y);
    const r = x % y;
    const d = r / y;
    let c = 0;
    for (let i = 0; i < y; i++) {
        a[i] = Math.floor(f + d + c);
        c = f + d + c - a[i];
    }

    return _.shuffle(a);
}

export function escapeRegExp(regExpStr: string) {
    return regExpStr.replace(/[.*+?^${}()|[\]\\]/g, '\\$&');
}
