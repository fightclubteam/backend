'use strict';

import * as config from 'config';
import * as glob from 'glob';
import * as mongoose from 'mongoose';
import * as path from 'path';

import { IMessageDocument } from '../modules/chat/interfaces/message';
import { IFightDocument } from '../modules/fight/interfaces/fight';
import { IFightActionMessageDocument } from '../modules/fight/interfaces/fight.action.message';
import { IFightLogDocument } from '../modules/fight/interfaces/fight.log';
import { IFightRoundDocument } from '../modules/fight/interfaces/fight.round';
import { IFightRoundScaleDocument } from '../modules/fight/interfaces/fight.round.scale';
import { IItemDocument } from '../modules/item/interfaces/item';
import { ILocationDocument } from '../modules/location/interfaces/location';
import { ILocationObjectDocument } from '../modules/location/interfaces/location.object';
import { IStoreDocument } from '../modules/store/interfaces/store';
import { IStoreItemDocument } from '../modules/store/interfaces/store_item';
import { IRevokedAuthTokenDocument } from '../modules/user/interfaces/revoked-auth-token';
import { IUserDocument, IUserRankDocument, IUserRoomDocument } from '../modules/user/interfaces/user';
import { IUserItemDocument } from '../modules/user/interfaces/user_item';

export interface IMongooseDocuments {
    Message: IMessageDocument;
    RevokedAuthToken: IRevokedAuthTokenDocument;
    Fight: IFightDocument;
    FightLog: IFightLogDocument;
    FightRound: IFightRoundDocument;
    FightRoundScale: IFightRoundScaleDocument;
    FightActionMessage: IFightActionMessageDocument;
    User: IUserDocument;
    UserItem: IUserItemDocument;
    UserRank: IUserRankDocument;
    UserRoom: IUserRoomDocument;
    Location: ILocationDocument;
    LocationObject: ILocationObjectDocument;
    Item: IItemDocument;
    Store: IStoreDocument;
    StoreItem: IStoreItemDocument;
}

class Database {
    private documents: IMongooseDocuments = {} as any;
    private mongooseConnection: mongoose.MongooseThenable;

    constructor() {
        this.mongooseConnection = mongoose.connect(
            config.get('mongoose.uri') as string,
            config.get('mongoose.options'),
        );

        if (!config.get('is_prod')) {
            mongoose.set('debug', true);
        }

        glob
            .sync(path.resolve(__dirname, '..', 'modules') + '/**/documents/*.js')
            .filter((filepath: string) => {
                const basename = path.basename(filepath);
                return basename.slice(-3) === '.js' && basename.indexOf('.') !== 0;
            })
            .forEach((filepath: string) => {
                const imported = require(filepath).default || require(filepath);
                if (typeof imported.modelName !== 'undefined') {
                    this.documents[imported.modelName] = imported;
                }
            });
    }

    /**
     * Returns documents
     *
     * @returns {IMongooseDocuments}
     */
    public getDocuments(): IMongooseDocuments {
        return this.documents;
    }

    /**
     * Returns mongoose instance
     *
     * @returns {mongooseConnection.MongooseThenable}
     */
    public getMongoose(): mongoose.MongooseThenable {
        return this.mongooseConnection;
    }
}

const database = new Database();
export const documents = database.getDocuments();
export const mongooseConnection = database.getMongoose();
