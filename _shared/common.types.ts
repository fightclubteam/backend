'use strict';

export default `
    interface Node {
        _id: ID!
    }
    
    type PaginationType {
        totalItemsCount: Int!
        totalPages: Int!
        page: Int!
        itemsPerPage: Int!
    }
`;
