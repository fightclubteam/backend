'use strict';

import * as mongoose from 'mongoose';

import { IPaginationCriteria } from '../../common.interfaces';

import { IncorrectItemsPerPagePaginationError } from './errors/IncorrectItemsPerPagePaginationError';
import { IncorrectPagePaginationError } from './errors/IncorrectPagePaginationError';

export async function paginateModel(
    model: mongoose.Model<any>,
    page: number = 1,
    itemsPerPage: number = 10,
    criteria: IPaginationCriteria = { populate: '' },
) {
    if (page <= 0) {
        throw new IncorrectPagePaginationError('page argument should be greater then 0');
    }

    if (itemsPerPage <= 0) {
        throw new IncorrectItemsPerPagePaginationError('itemsPerPage argument should be greater then 0');
    }

    const populationCriteria = criteria.populate;
    delete criteria.populate;

    const [items, totalItemsCount] = await Promise.all([
        model
            .find(criteria)
            .populate(populationCriteria)
            .limit(itemsPerPage)
            .skip((page - 1) * itemsPerPage),
        model.count(criteria),
    ]);

    return paginateArray(items, totalItemsCount, page, itemsPerPage);
}

export function paginateArray(items: any[], totalItemsCount, page: number = 1, itemsPerPage: number = 10) {
    return {
        data: items,
        pagination: {
            totalPages: Math.ceil(totalItemsCount / itemsPerPage),
            totalItemsCount,
            page,
            itemsPerPage,
        },
    };
}
