'use strict';

import { PaginationError } from './PaginationError';

export class IncorrectPagePaginationError extends PaginationError {
    constructor(public message: string, public code?: number | string, public parent?: Error) {
        super(message, code, parent);
    }
}
