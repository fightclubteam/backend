'use strict';

import { S3 } from 'aws-sdk';
import * as config from 'config';
import * as fs from 'fs';
import * as mime from 'mime';

export async function uploadSharedFile(s3Path: string, localPath: string) {
    return new S3()
        .upload({
            Bucket: config.get('user_files_bucket_name') as string,
            Key: s3Path,
            Body: fs.createReadStream(localPath),
            ContentType: mime.getType(localPath),
        })
        .promise();
}
