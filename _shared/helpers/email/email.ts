'use strict';

import { SES } from 'aws-sdk';
import * as config from 'config';
import { EmailTemplate } from 'email-templates';
import { join } from 'path';

import { IUser } from '../../../modules/user/interfaces/user';

export async function sendEmailTemplateToUser(user: IUser, template: string) {
    const data = await new EmailTemplate(join(config.get('email.path') as string, template)).render({
        login: user.login,
        email_static_path: config.get('email.static_path'),
    });

    try {
        await new SES()
            .sendEmail({
                Destination: {
                    ToAddresses: [user.email],
                },
                Message: {
                    Body: {
                        Html: {
                            Charset: 'UTF-8',
                            Data: (data as any).html,
                        },
                    },
                    Subject: {
                        Charset: 'UTF-8',
                        Data: (data as any).subject,
                    },
                },
                Source: `Sacralium administration <${config.get('email.no_reply')}>`,
            })
            .promise();
    } catch (e) {
        console.error('Cannot send email', e.message);
    }
}
