'use strict';

import * as config from 'config';
import { RedisPubSub } from 'graphql-redis-subscriptions';

const pubSub = new RedisPubSub({ ...config.get('redis') });

export { pubSub };
