'use strict';

// about resolve function arguments read following link
// @link http://pcarion.com/2015/09/26/graphql-resolve/

import { makeExecutableSchema } from 'graphql-tools';
import { mergeResolvers, mergeTypes } from 'merge-graphql-schemas';

import chatResolvers from '../modules/chat/resolvers';
import fightResolvers from '../modules/fight/resolvers';
import itemResolvers from '../modules/item/resolvers';
import locationResolvers from '../modules/location/resolvers';
import storeResolvers from '../modules/store/resolvers';
import userResolvers from '../modules/user/resolvers';

import commonTypes from './common.types';

import chatTypes from '../modules/chat/types';
import fightTypes from '../modules/fight/types';
import itemTypes from '../modules/item/types';
import locationTypes from '../modules/location/types';
import storeTypes from '../modules/store/types';
import userTypes from '../modules/user/types';

const resolvers = mergeResolvers([
    chatResolvers,
    fightResolvers,
    itemResolvers,
    locationResolvers,
    storeResolvers,
    userResolvers,
]);

const typeDefs = mergeTypes(
    [].concat.apply([], [commonTypes, userTypes, fightTypes, locationTypes, chatTypes, itemTypes, storeTypes]),
);

export default makeExecutableSchema({
    typeDefs,
    resolvers,
});
