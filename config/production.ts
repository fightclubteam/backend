'use strict';

import * as process from 'process';

export = {
    mongoose: {
        uri: process.env.PRODUCTION_AWS_MONGO_URI,
    },
    security: {
        token: process.env.PRODUCTION_SECURITY_TOKEN,
    },
    aws: {
        accessKeyId: process.env.ACCESS_KEY_ID,
        secretAccessKey: process.env.SECRET_ACCESS_KEY,
    },
    redis: {
        connection: {
            host: process.env.PRODUCTION_REDIS_HOST,
        },
    },
};
