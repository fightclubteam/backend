'use strict';

import * as os from 'os';
import { join } from 'path';
import * as process from 'process';

const ENV: string = process.env.NODE_ENV;

export = {
    env: ENV,
    is_prod: ENV === 'production',
    port: process.env.PORT || 8081,
    security: {
        expires_in: 18000, // 5 hours
        token: 'DCE2s?w8u4SM?X+vM7Mr6xqdugNhgJmS?4!su+9VpKLYT$rq_n_DkFEC5wB5KfUxKth^+ALb-*8GKhtJ63KYsB7CU2zYmr2%AuCU',
    },
    mongoose: {
        uri: 'mongodb://localhost:27017/bk',
        options: {
            native_parser: true,
            promiseLibrary: global.Promise,
            useMongoClient: true,
        },
    },
    redis: {
        connection: {
            host: '127.0.0.1',
            port: '6379',
        },
    },
    aws: {
        accessKeyId: '',
        secretAccessKey: '',
        region: 'us-east-1',
        apiVersions: {
            ses: '2010-12-01',
        },
        sqs: {
            consumer: {
                waitTimeSeconds: 10,
                batchSize: 10,
            },
            fight: {
                url: 'https://sqs.us-east-1.amazonaws.com/019498050267/fights-dev',
            },
        },
    },
    email: {
        path: join(__dirname, '..', '..', 'views', 'emails'),
        no_reply: 'noreply@bkgame.click',
        static_path: 'https://static.bkgame.click/static/img/emails/',
    },
    max_event_listener: 1024,
    subscription_path: '/s',
    graphql_path: '/q',
    frontend_host: process.env.FRONTEND_HOSTNAME,
    user_files_bucket_name: process.env.USER_FILES_BUCKET_NAME,
    // Note! temp_upload_path should be absolute path without trailing slash
    temp_upload_path: os.tmpdir(),
    cdn_hostname: process.env.CDN_HOSTNAME,
};
