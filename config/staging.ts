'use strict';

import * as process from 'process';

export = {
    mongoose: {
        uri: process.env.STAGING_AWS_MONGO_URI,
    },
    security: {
        token: process.env.STAGING_SECURITY_TOKEN,
    },
    aws: {
        accessKeyId: process.env.ACCESS_KEY_ID,
        secretAccessKey: process.env.SECRET_ACCESS_KEY,
    },
    redis: {
        connection: {
            host: process.env.STAGING_REDIS_HOST,
        },
    },
};
