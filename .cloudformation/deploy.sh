#!/usr/bin/env bash

BKT=sacralium-cfn

while [[ $# -gt 1 ]]
do
key="$1"

case ${key} in
    -b|--bucket)
    BKT="$2"
    shift # past argument
    ;;
    -t|--template)
    TMP="$2"
    shift # past argument
    ;;
    -s|--stack)
    STK="$2"
    shift # past argument
    ;;
    *)
            # unknown option
    ;;
esac
shift # past argument or value
done

if [ -z ${STK} ]; then
    echo "Stack is required"
    exit 1
fi

if [ -z ${TMP} ]; then
    echo "Template is required"
    exit 1
fi

STACK_FOUND=$(aws cloudformation describe-stacks | jq '.Stacks | .[] | .StackName' | grep "${STK}" | wc -l)

aws --profile default s3 sync ./ s3://${BKT}/
aws --profile default cloudformation validate-template --template-url "https://$BKT.s3.amazonaws.com/$TMP"
if [ ${STACK_FOUND} != "0" ] ; then
    aws --profile default cloudformation update-stack --stack-name ${STK} --template-url "https://$BKT.s3.amazonaws.com/$TMP"
else
    aws --profile default cloudformation create-stack --stack-name ${STK} --template-url "https://$BKT.s3.amazonaws.com/$TMP"
fi
