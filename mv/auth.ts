'use strict';

import * as _ from 'lodash';
import * as jwt from 'express-jwt';
import * as express from 'express';
import { documents } from '../_shared/documents';
import { IUser } from '../modules/user/interfaces/user';
import { USER_ROLE, GUEST_ROLE } from '../modules/user/enums/user';

const Acl = new (require('js-acl'))();

Acl
    // roles
    .addRole(GUEST_ROLE)
    .addRole(USER_ROLE.USER, GUEST_ROLE)
    .addRole(USER_ROLE.DEALER, USER_ROLE.USER)
    .addRole(USER_ROLE.CLAN_HEAD, USER_ROLE.USER)
    .addRole(USER_ROLE.MODERATOR, USER_ROLE.CLAN_HEAD)
    .addRole(USER_ROLE.ADMIN, USER_ROLE.MODERATOR)
    // resources
    .addResource(documents.User.getResourceId())
    // allow resources
    .allow(USER_ROLE.ADMIN, [documents.User.getResourceId()])
    .allow(GUEST_ROLE, documents.User.getResourceId(), 'view')
    .allow(
        USER_ROLE.USER,
        documents.User.getResourceId(),
        'edit',
        (currentUser: IUser, user: IUser, privilege) => user.id === currentUser.id,
    );

export const authMv = jwt({ getToken: getAuthToken, secret, isRevoked, credentialsRequired: false });
export const authMvRequired = jwt({ getToken: getAuthToken, secret, isRevoked });

/**
 * Returns `true` if token is revoked, otherwise `false`
 */
function isRevoked(req: express.Request, payload, done) {
    if (!payload || !payload.jti) {
        done(null, false);
    } else {
        documents.RevokedAuthToken.findOne({ jti: payload.jti }, (err, revokedToken) => {
            if (err) {
                return done(err);
            }
            return done(null, !!revokedToken);
        });
    }
}

/**
 * Returns secret key
 */
export function secret(req: express.Request, payload, done) {
    if (!payload || !payload.sub) {
        done(null, null);
    } else {
        documents.User.findById(payload.sub, (err, user) => {
            if (err) {
                return done(err);
            }
            done(null, (user && user.getAuthToken()) || null);
        });
    }
}

/**
 *
 * @param {e.Request} req
 * @returns {string|null}
 */
export function getAuthToken(req: express.Request): string | null {
    const authorization = req.get('authorization');
    if (typeof authorization === 'string') {
        const authParts = authorization.split(' ');
        if (authParts[0] === 'Bearer') {
            return authParts[1];
        }
    }
    return null;
}

/**
 *
 * @param {e.Request} req
 * @param {e.Response} res
 * @param {e.NextFunction} next
 * @returns {Promise<any>}
 */
export async function userMv(req: express.Request, res: express.Response, next: express.NextFunction) {
    let user: any = req.user;
    req.jwt = user;
    if (_.isEmpty(user)) {
        user = {
            _id: '-1',
            role: GUEST_ROLE,
            getResourceId() {
                return documents.User.getResourceId();
            },
            getRoles() {
                return [].concat(this.role || GUEST_ROLE);
            },
        };
    } else {
        // https://stackoverflow.com/questions/24096546/mongoose-populate-vs-object-nesting/24096822#24096822
        // https://stackoverflow.com/questions/28504046/how-does-mongoose-populate-work-under-the-hood
        user = await documents.User.findById((user as any).sub).populate('location');
    }

    req.user = user;
    next();
}

/**
 *
 * @param {e.Request} req
 * @param {e.Response} res
 * @param {e.NextFunction} next
 * @returns {Promise<any>}
 */
export function aclMv(req: express.Request, res: express.Response, next: express.NextFunction) {
    req.acl = Acl;
    req.acl.setUserIdentity(req.user as any);

    next();
}
