'use strict';

import * as schedule from 'node-schedule';
import * as _ from 'lodash';
import * as moment from 'moment';
import { documents } from '../_shared/documents';
import { pubSub } from '../_shared/pub-sub';
import { IFightRound } from '../modules/fight/interfaces/fight.round';

async function getAttacksToRejecting(periodSec: number) {
    const now = moment();
    const nextCronTime = now
        .clone()
        .add(periodSec, 's')
        .toDate();
    const fightsRounds = await documents.FightRound.findInProgressBlows(nextCronTime);

    const promises = [];
    _.each(fightsRounds, fightRound => {
        if (fightRound.expires_at < now.toDate()) {
            promises.push(runExpireBlows(fightRound));
        } else {
            schedule.scheduleJob(fightRound.expires_at, runExpireBlowsById.bind(null, fightRound._id));
        }
    });

    if (promises.length > 0) {
        await Promise.all(promises);
    }
}

/**
 * Runs expire blows
 *
 * @param {string} id
 * @returns {Promise<any>}
 */
async function runExpireBlowsById(id) {
    const fightRound = await documents.FightRound.findById(id);
    if (!fightRound) {
        return console.warn('Cannot find fight round by ID:', id);
    }
    return runExpireBlows(fightRound);
}

async function runExpireBlows(fightRound: IFightRound) {
    if (!fightRound.inProgress()) {
        return console.info('Fight round is not in progress anymore:', fightRound.id);
    }
    pubSub.publish('blowsExpired', {}); // todo:
}

export default function run() {
    const periodSec = 20;
    const scheduleJob = schedule.scheduleJob(`*/${periodSec} * * * * *`, getAttacksToRejecting.bind(null, periodSec));
    process.on('SIGINT', () => {
        scheduleJob.cancel();
    });
}
