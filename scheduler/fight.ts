'use strict';

import * as _ from 'lodash';
import * as moment from 'moment';
import * as schedule from 'node-schedule';

import { documents } from '../_shared/documents';
import { pubSub } from '../_shared/pub-sub';

import { FIGHT_REJECTED_REASON } from '../modules/fight/enums/fight';
import { IFight } from '../modules/fight/interfaces/fight';

async function getFightsToRunning(periodSec: number) {
    const now = moment();
    const nextCronTime = now
        .clone()
        .add(periodSec, 's')
        .toDate();
    const fights = await documents.Fight.findInProgressFights(nextCronTime);

    const promises = [];
    _.each(fights, fight => {
        if (fight.begins_in >= now.toDate()) {
            schedule.scheduleJob(fight.begins_in, runFightById.bind(null, fight._id));
        } else {
            promises.push(runFight(fight));
        }
    });
    if (promises.length > 0) {
        await Promise.all(promises);
    }
}

/**
 * Runs fight
 *
 * @param id
 * @returns {Promise<void>}
 */
async function runFightById(id) {
    const fight = await documents.Fight.findById(id);
    if (!fight) {
        return console.error('Cannot find the fight:', id);
    }

    return runFight(fight);
}

async function runFight(fight: IFight) {
    if (!fight.inProgress()) {
        return console.info('Fight is not in progress anymore:', fight.id);
    }

    if (!fight.canStartFight()) {
        await fight.remove();
        pubSub.publish('fightRejected', {
            fight_id: fight.id,
            fight_type: fight.fight_type,
            location: fight.location,
            reason_type: FIGHT_REJECTED_REASON.NOT_ENOUGH_PARTICIPANTS,
            members: fight.getAllTeamLoginList(), // just for subscription.ts
        });
    } else {
        await fight.makeFightInBattle();
        pubSub.publish('battleStarted', { fight_id: fight.id, members: fight.getAllTeamLoginList() });
    }
}

export default function run() {
    const periodSec = 20;
    const scheduleJob = schedule.scheduleJob(`*/${periodSec} * * * * *`, getFightsToRunning.bind(null, periodSec));
    process.on('SIGINT', () => {
        scheduleJob.cancel();
    });
}
