'use strict';

module.exports = {
    up: function (queryInterface, DataTypes) {
        return queryInterface.createTable('locations', {
            id: {
                primaryKey: true,
                type: DataTypes.UUID,
                defaultValue: DataTypes.UUIDV4
            },
            name: {
                type: DataTypes.STRING,
                allowNull: false
            },
            label: {
                type: DataTypes.STRING,
                allowNull: false
            },
            image: {
                type: DataTypes.STRING,
                allowNull: false
            },
            can_attack: {
                type: DataTypes.BOOLEAN,
            },
            can_create_fight: {
                type: DataTypes.BOOLEAN,
            },
        }).then(() => queryInterface.addIndex(
            'locations',
            ['name'],
            {
                indexName: 'idx-locations-name',
                indicesType: 'UNIQUE',
            }
        ));
    },

    down: function (queryInterface) {
        return queryInterface.dropTable('locations');
    }
};
