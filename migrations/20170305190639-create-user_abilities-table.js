'use strict';

module.exports = {
    up: function (queryInterface, DataTypes) {
        return queryInterface.createTable('user_abilities', {
            user_id: {
                primaryKey: true,
                type: DataTypes.UUID,
                references: {
                    model: 'users',
                    key: 'id'
                },
                onUpdate: 'NO ACTION',
                onDelete: 'CASCADE',
                allowNull: false
            },
            strength: {
                type: DataTypes.INTEGER(4).UNSIGNED,
                allowNull: false,
                defaultValue: 3
            },
            agility: {
                type: DataTypes.INTEGER(4).UNSIGNED,
                allowNull: false,
                defaultValue: 3
            },
            intuition: {
                type: DataTypes.INTEGER(4).UNSIGNED,
                allowNull: false,
                defaultValue: 3
            },
            endurance: {
                type: DataTypes.INTEGER(4).UNSIGNED,
                allowNull: false,
                defaultValue: 3
            }
        });
    },

    down: function (queryInterface) {
        return queryInterface.dropTable('user_abilities');
    }
};
