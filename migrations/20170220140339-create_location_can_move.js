'use strict';

module.exports = {
    up: function (queryInterface, DataTypes) {
        return queryInterface.createTable('location_can_move', {
            location_id: {
                type: DataTypes.UUID,
                references: {
                    model: 'locations',
                    key: 'id'
                },
                onUpdate: 'NO ACTION',
                onDelete: 'CASCADE',
                allowNull: false
            },
            nearby_location_id: {
                type: DataTypes.UUID,
                references: {
                    model: 'locations',
                    key: 'id'
                },
                onUpdate: 'NO ACTION',
                onDelete: 'CASCADE',
                allowNull: false
            },
            order: {
                type: DataTypes.INTEGER(4).UNSIGNED,
                allowNull: false
            }
        })
    },

    down: function (queryInterface) {
        return queryInterface.dropTable('location_can_move');
    }
};