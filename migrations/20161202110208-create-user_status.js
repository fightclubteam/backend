'use strict';

module.exports = {
    up: function (queryInterface, DataTypes) {
        return queryInterface.createTable('user_status', {
            user_id: {
                primaryKey: true,
                type: DataTypes.UUID,
                references: {
                    model: 'users',
                    key: 'id'
                },
                onUpdate: 'NO ACTION',
                onDelete: 'CASCADE',
                allowNull: false
            },
            silent_till: {
                type: DataTypes.DATE
            },
            ban_till: {
                type: DataTypes.DATE
            },
        });
    },

    down: function (queryInterface) {
        return queryInterface.dropTable('user_status');
    }
};
