'use strict';

module.exports = {
    /**
     *
     * @param queryInterface
     * @param DataTypes
     *
     * @returns {*}
     */
    up: function (queryInterface, DataTypes) {
        return queryInterface.createTable('users', {
            id: {
                primaryKey: true,
                type: DataTypes.UUID,
                defaultValue: DataTypes.UUIDV4
            },
            email: {
                type: DataTypes.STRING,
                allowNull: false
            },
            login: {
                type: DataTypes.STRING(20),
                allowNull: false
            },
            password: {
                type: DataTypes.STRING(100),
                allowNull: false
            },
            register_ip: {
                type: DataTypes.STRING(45),
                allowNull: false
            },
            name: {
                type: DataTypes.STRING,
                allowNull: false
            },
            birthday: {
                type: DataTypes.DATEONLY
            },
            location_id: {
                type: DataTypes.UUID,
                references: {
                    model: 'locations',
                    key: 'id'
                },
            },
            sex: {
                type: DataTypes.ENUM,
                values: ['MALE', 'FEMALE'],
                allowNull: false
            },
            role: {
                type: DataTypes.ENUM,
                values: ['ADMIN', 'DEALER', 'CLAN_HEAD', 'STORE_OWNER', 'GENERAL_MANAGER', 'MANAGER', 'MODERATOR', 'USER'],
                allowNull: false
            },
            free_ability: {
                type: DataTypes.INTEGER(4).UNSIGNED,
                allowNull: false,
                defaultValue: 200
            },
            exp: {
                type: DataTypes.INTEGER.UNSIGNED,
                allowNull: false,
                defaultValue: 0
            },
            level: {
                type: DataTypes.INTEGER(4).UNSIGNED,
                allowNull: false,
                defaultValue: 0
            },
            wins: {
                type: DataTypes.INTEGER(8).UNSIGNED,
                allowNull: false,
                defaultValue: 0
            },
            defeats: {
                type: DataTypes.INTEGER(8).UNSIGNED,
                allowNull: false,
                defaultValue: 0
            },
            draws: {
                type: DataTypes.INTEGER(8).UNSIGNED,
                allowNull: false,
                defaultValue: 0
            },
            credits: {
                type: DataTypes.INTEGER.UNSIGNED,
                allowNull: false,
                defaultValue: 0
            },
            euro_credits: {
                type: DataTypes.INTEGER.UNSIGNED,
                allowNull: false,
                defaultValue: 0
            },
            timezone: {
                type: DataTypes.STRING(100)
            },
            login_attempts: {
                type: DataTypes.INTEGER(4).UNSIGNED,
                defaultValue: 0,
                allowNull: false
            },
            reset_password_hash: {
                type: DataTypes.UUID,
                defaultValue: DataTypes.UUIDV4
            },
            sent_reset_password_at: {
                type: DataTypes.DATE
            },
            created_at: {
                type: DataTypes.DATE,
                allowNull: false
            },
            updated_at: {
                type: DataTypes.DATE,
                allowNull: false
            },
        }).then(() => queryInterface.addIndex(
            'users',
            ['email'],
            {
                indexName: 'idx-users-email',
                indicesType: 'UNIQUE'
            }
        )).then(() => queryInterface.addIndex(
            'users',
            ['login'],
            {
                indexName: 'idx-users-login',
                indicesType: 'UNIQUE'
            }
        ));
    },

    down: function (queryInterface) {
        return queryInterface.dropTable('users');
    }
};
