'use strict';

module.exports = {
    up: function (queryInterface, DataTypes) {
        return queryInterface.createTable('location_objects', {
            id: {
                primaryKey: true,
                type: DataTypes.UUID,
                defaultValue: DataTypes.UUIDV4
            },
            name: {
                type: DataTypes.STRING,
                allowNull: false
            },
            label: {
                type: DataTypes.STRING,
                allowNull: false
            },
            image: {
                type: DataTypes.STRING,
                allowNull: false
            },
            coordinates: {
                type: DataTypes.STRING,
            },
            location_id: {
                type: DataTypes.UUID,
                references: {
                    model: 'locations',
                    key: 'id'
                },
                onUpdate: 'NO ACTION',
                onDelete: 'CASCADE',
            },
        })
    },

    down: function (queryInterface) {
        return queryInterface.dropTable('location_objects');
    }
};
