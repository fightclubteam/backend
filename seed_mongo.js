'use strict';

const { mongoose, mongooseConnection } = require('./dist/_shared/documents');
const seedMongoose = require('util').promisify(require('seed-mongoose'));

(async function () {
    const connection = await mongooseConnection;
    try {
        await connection.db.dropDatabase();
        await seedMongoose({
            path: 'seeders/mongoose',
            logger: console,
            mongoose: connection,
            environment: '',
        });
        await connection.close();
        process.exit(0);
    } catch (e) {
        await connection.close();
        console.error('\x1b[31mError while migrating seeders for mongoose\x1b[0m:', e);
        process.exit(1);
    }
})();
